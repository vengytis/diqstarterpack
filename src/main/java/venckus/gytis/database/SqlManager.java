package venckus.gytis.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SqlManager {

   protected Logger logger = LoggerFactory.getLogger(SqlManager.class);

   protected String OracleDriver;
   protected String URL;
   protected String Username;
   protected String Password;
   protected String Hostname;
   protected String Port;
   protected String SID;


   private Connection connection;
   private Properties properties;

   private String errorMessage;

   public SqlManager() {
      OracleDriver = "oracle.jdbc.driver.OracleDriver";
   }

   public SqlManager(String hostname, String port, String sid, String dbusername, String dbpassword) {
      OracleDriver = "oracle.jdbc.driver.OracleDriver";
      Hostname = hostname;
      Port = port;
      SID = sid;
      URL = URLBuilder(hostname, port, sid);
      Username = dbusername;
      Password = dbpassword;
   }

   public SqlManager(String username, String password, String URL) {
      OracleDriver = "oracle.jdbc.driver.OracleDriver";
      Password = password;
      Username = username;
      this.URL = URL;
   }

   /**
    * Builds URL for oracle jdbc driver
    * @param hostname hostname used for DB connection
    * @param port port used for DB connection
    * @param SID system identifier for oracle DB connection
    * @return
    */
   protected String URLBuilder(String hostname, String port, String SID) {
      return String.format("jdbc:oracle:thin:@//%s:%s/%s", hostname, port, SID);
   }

   /**
    * Registers connection properties by provided credentials
    * @return connection properties
    */
   protected Properties getProperties() {
      if (properties == null) {
         properties = new Properties();
         properties.setProperty("user", Username);
         properties.setProperty("password", Password);
      }
      return properties;
   }

   /**
    * Attempts to establish connection with oracle database via jdbc driver with provided parameters
    * @return connection or null (in case connection was not established)
    */
   public Connection connect() {
      if (connection == null) {
         try {
            Class.forName(OracleDriver);
            URL = URLBuilder(Hostname, Port, SID);
            connection = DriverManager.getConnection(URL, getProperties());
            logger.debug("Connection established with database - {}", connection.toString());
         } catch (ClassNotFoundException | SQLException e) {
            logger.error(e.getMessage(), e);
            connection = null;
            errorMessage = e.getMessage();
         }
      }
      return connection;
   }

   /**
    * Attempts to disconnect from database
    */
   public void disconnect() {
      if (connection != null) {
         try {
            connection.close();
            connection = null;
            logger.debug("Successfully disconnected from database");
         } catch (SQLException e) {
            logger.error("SQL error - {}, message - {}", e.getSQLState(), e.getMessage(), e);
            e.printStackTrace();
         }
      }
   }

   @Override
   public String toString() {
      return "SqlManager{" +
              "Username='" + Username + '\'' +
              ", Password='" + Password + '\'' +
              ", Hostname='" + Hostname + '\'' +
              ", Port='" + Port + '\'' +
              ", SID='" + SID + '\'' +
              '}';
   }

   public String getErrorMessage() {
      return errorMessage;
   }

   public String getUsername() {
      return Username;
   }

   public String getPassword() {
      return Password;
   }

   public String getURL() {
      return URL;
   }

   public String getHostname() {
      return Hostname;
   }

   public String getPort() {
      return Port;
   }

   public String getSID() {
      return SID;
   }

   public void setUsername(String username) {
      Username = username;
   }

   public void setPassword(String password) {
      Password = password;
   }

   public void setHostname(String hostname) {
      Hostname = hostname;
   }

   public void setPort(String port) {
      Port = port;
   }

   public void setSID(String SID) {
      this.SID = SID;
   }
}
