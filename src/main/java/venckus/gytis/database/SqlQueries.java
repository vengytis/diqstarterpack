package venckus.gytis.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlQueries extends SqlManager {

   private String GET_LOAD_UNITS_QUERY = "SELECT * FROM COR_LUM_LOAD_UNIT WHERE PK IS NOT NULL";

   public SqlQueries() {
      super();
   }

   public SqlQueries(String hostname, String port, String SID, String dbusername, String dbpassword) {
      super(hostname, port, SID, dbusername, dbpassword);
      URL = URLBuilder(hostname, port, SID);
   }

   public SqlQueries(String username, String password, String URL) {
      super(username, password, URL);
   }

   public Connection connect() {
      return super.connect();
   }

   public ResultSet executeCustomQuery(StringBuilder query) throws SQLException {
      PreparedStatement preparedStatement = connect().prepareStatement(query.toString());
      return preparedStatement.executeQuery();
   }

   public SqlQueries(SqlManager databaseConnector) {
      this.Username = databaseConnector.getUsername();
      this.Password = databaseConnector.getPassword();
      this.Hostname = databaseConnector.getHostname();
      this.Port = databaseConnector.getPort();
      this.SID = databaseConnector.getSID();
   }

   /**
    * Calculates what is the most optimal fetch size when retrieving data from DB. The idea is to make 5 trips to DB and back.
    *
    * @return size of rows to be retrieved in one trip from DB
    * @throws SQLException
    */
   public int getOptimalFetchSize(StringBuilder query) throws SQLException {
      StringBuilder countQuery = new StringBuilder();
      countQuery.append("SELECT COUNT(*) as rowcount FROM (");
      countQuery.append(query);
      countQuery.append(")");
      PreparedStatement countRows = connect().prepareStatement(countQuery.toString());
      logger.debug("Executing query - {}", countQuery.toString());
      ResultSet countRs = countRows.executeQuery();
      countRs.next();
      int count = countRs.getInt("rowcount");
      logger.trace("Total amount of rows to be retrieved - {}", count);
      countRs.close();
      int fetchSize = (int) ((count / 5) + Math.round(count * 0.01));
      logger.debug("Calculated optimized fetch size - {}", fetchSize);
      return fetchSize;
   }

   /**
    * Fetches all load units from DB
    *
    * @return load units in the system
    * @throws SQLException
    */
   private String getAllLoadUnits() throws SQLException {
      StringBuilder query = new StringBuilder();
      query.append(GET_LOAD_UNITS_QUERY);
      query.append(" AND CURRENT_LOC IS NOT NULL");
      return query.toString();
   }

   /**
    * Fetches single load unit from DB
    *
    * @param loadUnitId barcode of load unit
    * @return load unit as a ResultSet or null if LU does not exist
    * @throws SQLException not handled here
    */
   private ResultSet getLoadUnit(String loadUnitId) throws SQLException {
      PreparedStatement preparedStatement = connect().prepareStatement(
              "SELECT * " +
                      "FROM COR_LUM_LOAD_UNIT " +
                      "WHERE PK = '" + loadUnitId + "'");
      return preparedStatement.executeQuery();
   }

   /**
    * Returns detailed location of specified load unit
    *
    * @param loadUnitId load unit of interest
    * @return detailed location of load unit, for example WCS-02.MS.08.23.MS08112423 or WCS-02.PI.0.0.CCPI02PL02
    * @throws SQLException
    */
   public String getLoadUnitDetailedLocation(String loadUnitId) throws SQLException {
      return getLoadUnit(loadUnitId).getString("CURRENT_LOC");
   }

   /**
    * Returns simplified location version of specified load unit
    *
    * @param loadUnitId load unit of interest
    * @return location of load unit, for example MS08112423 or CCPI02PL02
    * @throws SQLException
    */
   public String getLoadUnitLocation(String loadUnitId) throws SQLException {
      String detailedLocation = getLoadUnitDetailedLocation(loadUnitId);
      String[] locationParts = detailedLocation.split("\\.");
      return locationParts[4];
   }

   /**
    * @param loadUnitId load unit of interest
    * @return client of load unit (INTERNAL, 1, PEP etc.)
    * @throws SQLException
    */
   public String getLoadUnitClient(String loadUnitId) throws SQLException {
      return getLoadUnit(loadUnitId).getString("CLIENT");
   }

   /**
    * @param loadUnitId load unit of interest
    * @return status of load unit (STORED, SCHEDULED, TRANSPORT etc.)
    * @throws SQLException
    */
   public String getLoadUnitStatus(String loadUnitId) throws SQLException {
      return getLoadUnit(loadUnitId).getString("STATUS");
   }

   /**
    * @return all load units in the system with status 'STORED'
    * @throws SQLException
    */
   private ResultSet getStoredLoadUnits() throws SQLException {
      PreparedStatement preparedStatement = connect().prepareStatement(
              "SELECT * " +
                      "FROM COR_LUM_LOAD_UNIT " +
                      "WHERE STATUS = 'STORED'");
      return preparedStatement.executeQuery();
   }

   /**
    * @return String representation of all load units in the system with status 'STORED'
    * @throws SQLException
    */
   public ArrayList<String> getStoredLoadUnitsAsStrings() throws SQLException {
      ArrayList<String> loadUnits = new ArrayList<>();
      ResultSet storedLoadUnitsRS = getStoredLoadUnits();
      while (storedLoadUnitsRS.next()) {
         loadUnits.add(storedLoadUnitsRS.getString("PK"));
      }
      return loadUnits;
   }

   /**
    * @param numberOfLoadUnits amount of load units to be fetched
    * @return String representation of selected number of load units in the system with status 'STORED'
    * @throws SQLException
    */
   public ArrayList<String> getNumberOfStoredLoadUnitsAsStrings(int numberOfLoadUnits) throws SQLException {
      int currentCount = 0;
      ArrayList<String> loadUnits = new ArrayList<>();
      ResultSet storedLoadUnitsRS = getStoredLoadUnits();
      while (storedLoadUnitsRS.next() || currentCount < numberOfLoadUnits) {
         loadUnits.add(storedLoadUnitsRS.getString("PK"));
      }
      return loadUnits;
   }

   /**
    * @param aisle aisle of interest (1, 51, 7 etc.)
    * @return all load units in provided aisle with status 'STORED'
    * @throws SQLException
    */
   public ArrayList<String> getLoadUnitsStoredInSpecificAisle(String aisle) throws SQLException {
      //if aisle is passed as a single digit parameter, add 0 to the beginning
      aisle = (aisle.length() == 1) ? "0" + aisle : aisle;
      ArrayList<String> loadUnits = new ArrayList<>();
      ResultSet storedLoadUnitsRS = getStoredLoadUnits();
      //we only care about stored load units
      while (storedLoadUnitsRS.next()) {
         String loadUnit = storedLoadUnitsRS.getString("PK");
         String loadUnitLocation = storedLoadUnitsRS.getString("CURRENT_LOC");
         String[] locationParts = loadUnitLocation.split("\\.");
         //aisle is extracted from current location, for example if current location is WCS-14.MS.02.12.* - only take the part
         //between second and third dots
         if (locationParts[2].equals(aisle)) {
            loadUnits.add(loadUnit);
         }
      }
      return loadUnits;
   }

   /**
    * @param location provided parameter should be of simplified type (CCPI02PL02, CCDE00CS01 etc.)
    * @return all load units in provided location
    * @throws SQLException
    */
   public ArrayList<String> getLoadUnitsInLocationAsArrayList(String location) throws SQLException {
      ArrayList<String> loadUnits = new ArrayList<>();
      StringBuilder loadUnitsQuery = new StringBuilder(getAllLoadUnits());
      loadUnitsQuery.append(" AND CURRENT_LOC LIKE '%").append(location).append("%'");
      ResultSet loadUnitsInLocationRS = executeCustomQuery(loadUnitsQuery);
      while (loadUnitsInLocationRS.next()) {
         String loadUnit = loadUnitsInLocationRS.getString("PK");
         loadUnits.add(loadUnit);
      }
      return loadUnits;
   }

   /**
    * @return true if load unit is stored, false otherwise (LU does not exist, is not stored etc.)
    * @throws SQLException
    */
   public boolean loadUnitStored(String loadUnitId) throws SQLException {
      StringBuilder query = new StringBuilder();
      query.append("SELECT * FROM COR_LUM_LOAD_UNIT ");
      query.append("WHERE PK = '").append(loadUnitId).append("'");
      ResultSet loadUnit = executeCustomQuery(query);
      return loadUnit.next() && loadUnit.getString("STATUS").equals("STORED");
   }

   /**
    * @return load unit Id or null if none exists
    * @throws SQLException
    */
   public String getFirstLoadUnitInLocation(String location) throws SQLException {
      StringBuilder query = new StringBuilder();
      query.append("SELECT * FROM COR_LUM_LOAD_UNIT ");
      query.append("WHERE STATUS = 'NO_TO' ");
      query.append("AND CURRENT_LOC LIKE '%").append(location).append("%'");
      ResultSet loadUnit = executeCustomQuery(query);
      return loadUnit.next() ? loadUnit.getString("PK") : null;
   }

   public String getDetailedLocation(String location) throws SQLException {
      StringBuilder query = new StringBuilder();
      query.append("SELECT * FROM COR_STO_LOCATION ");
      query.append("WHERE COORDINATE = '").append(location).append("'");
      ResultSet detailedLocation = executeCustomQuery(query);
      detailedLocation.next();
      return detailedLocation.getString("PK");
   }

   public ArrayList<String> getLoadUnitsInLocation(String location) {
      return null;
   }

   /**
    * Checks whether location is empty or not
    *
    * @param location provided parameter should be of simplified type (CCPI02PL02, CCDE00CS01 etc.)
    * @return true if location is empty, false otherwise
    * @throws SQLException
    */
   public boolean locationEmpty(String location) throws SQLException {
      return getLoadUnitsInLocationAsArrayList(location).isEmpty();
   }

   public void clarifyLoadUnit(String loadUnitId) throws SQLException {
      PreparedStatement preparedStatement = connect().prepareStatement(
              "UPDATE COR_LUM_LOAD_UNIT SET" +
                      " LOGISTIC_ERROR_CODE = NULL," +
                      " LOGISTIC_ERROR_REASON = NULL," +
                      " ERROR_CODE = 'OK'," +
                      " ERROR_REASON = NULL," +
                      " QAREQUIRED = '0'," +
                      " QA_REASON = NULL," +
                      " CONTOUR_STATE = '..............'," +
                      " LOCK_STATUS = 'FREE' " +
                      " WHERE PK = '" + loadUnitId + "'");
      preparedStatement.executeQuery();
   }
}
