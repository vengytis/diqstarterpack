package venckus.gytis.boundary.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.database.SqlQueries;

import java.util.ArrayList;
import java.util.List;

public class Storage {

   private Logger logger = LoggerFactory.getLogger(Storage.class);

   protected int NumberOfAisles;
   protected int NumberOfLevels;
   protected int NumberOfXCoordinates;
   protected int StartingAisleNumber;
   protected String Facility;
   protected String Area;
   protected ArrayList<String> Aisles;
   protected String Client;

   public Storage() {

   }

   public Storage(int numberOfAisles, int numberOfLevels, int numberOfXCoordinates, int startingAisleNumber, String facility,
                  ArrayList<String> aisles, String client) {
      NumberOfAisles = numberOfAisles;
      NumberOfLevels = numberOfLevels;
      NumberOfXCoordinates = numberOfXCoordinates;
      StartingAisleNumber = startingAisleNumber;
      Facility = facility;
      Aisles = aisles;
      Client = client;
   }

   /**
    * Generates virtual locations of storage for all aisles
    * The format of generated locations are FACILITY.AREA.AISLE.LEVEL.0
    * For example WCS-02.MS.0.0.0 or BURGOS.HBA1.0.0.0
    * @return list of generated virtual locations
    */
   protected ArrayList<String> generateVirtualLocations() {
      ArrayList<String> virtualLocations = new ArrayList<>();
      for (int i = StartingAisleNumber; i <= StartingAisleNumber + NumberOfAisles - 1; i++) {
         for (int j = 1; j <= NumberOfLevels; j++) {
            String aisleNumber = ((i < 10) ? "0" + i : String.valueOf(i));
            String levelNumber = ((j < 10) ? "0" + j : String.valueOf(j));
            String virtualLocation = Facility + "." + Area + "." + aisleNumber + "." + levelNumber + ".0";
            virtualLocations.add(virtualLocation);
         }
      }
      logger.debug("Generated {} virtual locations", virtualLocations);
      return virtualLocations;
   }

   /**
    * Generates virtual locations of storage for specific aisles
    * The format of generated locations are FACILITY.AREA.AISLE.LEVEL.0
    * For example WCS-02.MS.0.0.0 or BURGOS.HBA1.0.0.0
    * @return list of generated virtual locations
    */
   protected ArrayList<String> generateVirtualLocationsByAisles() {
      ArrayList<String> virtualLocations = new ArrayList<>();
      for (String aisle : Aisles) {
         for (int j = 1; j <= NumberOfLevels; j++) {
            String aisleNumber = ((Integer.parseInt(aisle) < 10) ? "0" + Integer.parseInt(aisle) : aisle);
            String levelNumber = ((j < 10) ? "0" + j : String.valueOf(j));
            String virtualLocation = Facility + "." + Area + "." + aisleNumber + "." + levelNumber + ".0";
            virtualLocations.add(virtualLocation);
         }
      }
      logger.debug("Generated {} virtual locations", virtualLocations);
      return virtualLocations;
   }

}
