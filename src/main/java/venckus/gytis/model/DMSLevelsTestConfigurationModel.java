package venckus.gytis.model;

public class DMSLevelsTestConfigurationModel {

   private String restInput;
   private String dbusername;
   private String dbpassword;
   private String dbhostname;
   private String dbport;
   private String dbsid;
   private String numberOfAisles;
   private String numberOfLevels;
   private String workstation;
   private String facility;
   private String firstAisle;

   public String getRestInput() {
      return restInput;
   }

   public void setRestInput(String restInput) {
      this.restInput = restInput;
   }

   public String getDbusername() {
      return dbusername;
   }

   public void setDbusername(String dbusername) {
      this.dbusername = dbusername;
   }

   public String getDbpassword() {
      return dbpassword;
   }

   public void setDbpassword(String dbpassword) {
      this.dbpassword = dbpassword;
   }

   public String getDbhostname() {
      return dbhostname;
   }

   public void setDbhostname(String dbhostname) {
      this.dbhostname = dbhostname;
   }

   public String getDbport() {
      return dbport;
   }

   public void setDbport(String dbport) {
      this.dbport = dbport;
   }

   public String getDbsid() {
      return dbsid;
   }

   public void setDbsid(String dbsid) {
      this.dbsid = dbsid;
   }

   public String getNumberOfAisles() {
      return numberOfAisles;
   }

   public void setNumberOfAisles(String numberOfAisles) {
      this.numberOfAisles = numberOfAisles;
   }

   public String getNumberOfLevels() {
      return numberOfLevels;
   }

   public void setNumberOfLevels(String numberOfLevels) {
      this.numberOfLevels = numberOfLevels;
   }

   public String getWorkstation() {
      return workstation;
   }

   public void setWorkstation(String workstation) {
      this.workstation = workstation;
   }

   public String getFacility() {
      return facility;
   }

   public void setFacility(String facility) {
      this.facility = facility;
   }

   public String getFirstAisle() {
      return firstAisle;
   }

   public void setFirstAisle(String firstAisle) {
      this.firstAisle = firstAisle;
   }

   @Override
   public String toString() {
      return "DMSLevelsTestModel{" +
              "restInput='" + restInput + '\'' +
              ", dbusername='" + dbusername + '\'' +
              ", dbpassword='" + dbpassword + '\'' +
              ", dbhostname='" + dbhostname + '\'' +
              ", dbport='" + dbport + '\'' +
              ", dbsid='" + dbsid + '\'' +
              ", numberOfAisles='" + numberOfAisles + '\'' +
              ", numberOfLevels='" + numberOfLevels + '\'' +
              ", workstation='" + workstation + '\'' +
              ", facility='" + facility + '\'' +
              ", firstAisle='" + firstAisle + '\'' +
              '}';
   }
}
