package venckus.gytis.model;

public class LoadUnitRetrieverConfigurationModel {

   private String restInput;
   private String dbusername;
   private String dbpassword;
   private String dbhostname;
   private String dbport;
   private String dbsid;
   private String retrievalDestination;
   private String loopDelay;
   private boolean loop;

   public String getRestInput() {
      return restInput;
   }

   public void setRestInput(String restInput) {
      this.restInput = restInput;
   }

   public String getDbusername() {
      return dbusername;
   }

   public void setDbusername(String dbusername) {
      this.dbusername = dbusername;
   }

   public String getDbpassword() {
      return dbpassword;
   }

   public void setDbpassword(String dbpassword) {
      this.dbpassword = dbpassword;
   }

   public String getDbhostname() {
      return dbhostname;
   }

   public void setDbhostname(String dbhostname) {
      this.dbhostname = dbhostname;
   }

   public String getDbport() {
      return dbport;
   }

   public void setDbport(String dbport) {
      this.dbport = dbport;
   }

   public String getDbsid() {
      return dbsid;
   }

   public void setDbsid(String dbsid) {
      this.dbsid = dbsid;
   }

   public String getRetrievalDestination() {
      return retrievalDestination;
   }

   public void setRetrievalDestination(String retrievalDestination) {
      this.retrievalDestination = retrievalDestination;
   }

   public String getLoopDelay() {
      return loopDelay;
   }

   public void setLoopDelay(String loopDelay) {
      this.loopDelay = loopDelay;
   }

   public boolean isLoop() {
      return loop;
   }

   public void setLoop(String loop) {
      this.loop = loop.equals("true");
   }

   @Override
   public String toString() {
      return "LoadUnitRetrieverConfigurationModel{" +
              "restInput='" + restInput + '\'' +
              ", dbusername='" + dbusername + '\'' +
              ", dbpassword='" + dbpassword + '\'' +
              ", dbhostname='" + dbhostname + '\'' +
              ", dbport='" + dbport + '\'' +
              ", dbsid='" + dbsid + '\'' +
              ", retrievalDestination='" + retrievalDestination + '\'' +
              ", loopDelay='" + loopDelay + '\'' +
              ", loop=" + loop +
              '}';
   }
}
