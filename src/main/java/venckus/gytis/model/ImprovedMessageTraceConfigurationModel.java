package venckus.gytis.model;

public class ImprovedMessageTraceConfigurationModel {

   private String dbusername;
   private String dbpassword;
   private String dbhostname;
   private String dbport;
   private String dbsid;
   private String pathToExcel;
   private String timestamp;
   private String maxNumberOfAisles;
   private String maxXLocations;
   private String maxYLocations;
   private String maxDepth;
   private String firstAisle;

   public String getDbusername() {
      return dbusername;
   }

   public void setDbusername(String dbusername) {
      this.dbusername = dbusername;
   }

   public String getDbpassword() {
      return dbpassword;
   }

   public void setDbpassword(String dbpassword) {
      this.dbpassword = dbpassword;
   }

   public String getDbhostname() {
      return dbhostname;
   }

   public void setDbhostname(String dbhostname) {
      this.dbhostname = dbhostname;
   }

   public String getDbport() {
      return dbport;
   }

   public void setDbport(String dbport) {
      this.dbport = dbport;
   }

   public String getDbsid() {
      return dbsid;
   }

   public void setDbsid(String dbsid) {
      this.dbsid = dbsid;
   }

   public String getPathToExcel() {
      return pathToExcel;
   }

   public void setPathToExcel(String pathToExcel) {
      this.pathToExcel = pathToExcel;
   }

   public String getTimestamp() {
      return timestamp;
   }

   public void setTimestamp(String timestamp) {
      this.timestamp = timestamp;
   }

   public String getMaxNumberOfAisles() {
      return maxNumberOfAisles;
   }

   public void setMaxNumberOfAisles(String maxNumberOfAisles) {
      this.maxNumberOfAisles = maxNumberOfAisles;
   }

   public String getMaxXLocations() {
      return maxXLocations;
   }

   public void setMaxXLocations(String maxXLocations) {
      this.maxXLocations = maxXLocations;
   }

   public String getMaxYLocations() {
      return maxYLocations;
   }

   public void setMaxYLocations(String maxYLocations) {
      this.maxYLocations = maxYLocations;
   }

   public String getMaxDepth() {
      return maxDepth;
   }

   public void setMaxDepth(String maxDepth) {
      this.maxDepth = maxDepth;
   }

   public String getFirstAisle() {
      return firstAisle;
   }

   public void setFirstAisle(String firstAisle) {
      this.firstAisle = firstAisle;
   }

   @Override
   public String toString() {
      return "ImprovedMessageTraceModel{" +
              "dbusername='" + dbusername + '\'' +
              ", dbpassword='" + dbpassword + '\'' +
              ", dbhostname='" + dbhostname + '\'' +
              ", dbport='" + dbport + '\'' +
              ", dbsid='" + dbsid + '\'' +
              ", pathToExcel='" + pathToExcel + '\'' +
              ", timestamp='" + timestamp + '\'' +
              ", maxNumberOfAisles='" + maxNumberOfAisles + '\'' +
              ", maxXLocations='" + maxXLocations + '\'' +
              ", maxYLocations='" + maxYLocations + '\'' +
              ", maxDepth='" + maxDepth + '\'' +
              ", firstAisle='" + firstAisle + '\'' +
              '}';
   }
}
