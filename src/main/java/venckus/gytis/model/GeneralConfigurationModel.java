package venckus.gytis.model;

import venckus.gytis.database.SqlManager;

public class GeneralConfigurationModel {

   private String ConfigurationName;

   //general parameters
   private String CreateTransportRestCall;

   //DB parameters
   private String UsernameDB;
   private String PasswordDB;
   private String HostnameDB;
   private String PortDB;
   private String SidDB;

   //Load Unit Extractor parameters
   private String LoadUnitExtractor_Prefix;
   private String LoadUnitExtractor_PrefixLength;

   //Load Unit Sender parameters
   private String LoadUnitSender_SourceLocation;
   private String LoadUnitSender_DestinationLocation;
   private String LoadUnitSender_Delay;
   private boolean LoadUnitSender_Clarify;
   private boolean LoadUnitSender_DisregardClient;

   //Load Unit Retriever
   private String LoadUnitRetriever_RetrievalDestination;
   private String LoadUnitRetriever_LoopDelay;
   private boolean LoadUnitRetriever_LoopEnabled;
   private boolean LoadUnitRetriever_StoredCheckEnabled;

   public String getConfigurationName() {
      return ConfigurationName;
   }

   public void setConfigurationName(String configurationName) {
      ConfigurationName = configurationName;
   }

   public String getCreateTransportRestCall() {
      return CreateTransportRestCall;
   }

   public void setCreateTransportRestCall(String createTransportRestCall) {
      CreateTransportRestCall = createTransportRestCall;
   }

   public String getUsernameDB() {
      return UsernameDB;
   }

   public void setUsernameDB(String usernameDB) {
      UsernameDB = usernameDB;
   }

   public String getPasswordDB() {
      return PasswordDB;
   }

   public void setPasswordDB(String passwordDB) {
      PasswordDB = passwordDB;
   }

   public String getHostnameDB() {
      return HostnameDB;
   }

   public void setHostnameDB(String hostnameDB) {
      HostnameDB = hostnameDB;
   }

   public String getPortDB() {
      return PortDB;
   }

   public void setPortDB(String portDB) {
      PortDB = portDB;
   }

   public String getSidDB() {
      return SidDB;
   }

   public void setSidDB(String sidDB) {
      SidDB = sidDB;
   }

   public String getLoadUnitExtractor_PrefixLength() {
      return LoadUnitExtractor_PrefixLength;
   }

   public void setLoadUnitExtractor_PrefixLength(String loadUnitExtractor_PrefixLength) {
      LoadUnitExtractor_PrefixLength = loadUnitExtractor_PrefixLength;
   }

   public String getLoadUnitExtractor_Prefix() {
      return LoadUnitExtractor_Prefix;
   }

   public void setLoadUnitExtractor_Prefix(String loadUnitExtractor_Prefix) {
      LoadUnitExtractor_Prefix = loadUnitExtractor_Prefix;
   }

   public String getLoadUnitSender_SourceLocation() {
      return LoadUnitSender_SourceLocation;
   }

   public void setLoadUnitSender_SourceLocation(String loadUnitSender_SourceLocation) {
      LoadUnitSender_SourceLocation = loadUnitSender_SourceLocation;
   }

   public String getLoadUnitSender_DestinationLocation() {
      return LoadUnitSender_DestinationLocation;
   }

   public void setLoadUnitSender_DestinationLocation(String loadUnitSender_DestinationLocation) {
      LoadUnitSender_DestinationLocation = loadUnitSender_DestinationLocation;
   }

   public String getLoadUnitSender_Delay() {
      return LoadUnitSender_Delay;
   }

   public void setLoadUnitSender_Delay(String loadUnitSender_Delay) {
      LoadUnitSender_Delay = loadUnitSender_Delay;
   }

   public boolean isLoadUnitSender_Clarify() {
      return LoadUnitSender_Clarify;
   }

   public void setLoadUnitSender_Clarify(String loadUnitSender_Clarify) {
      LoadUnitSender_Clarify = loadUnitSender_Clarify.equals("true");
   }

   public boolean isLoadUnitSender_DisregardClient() {
      return LoadUnitSender_DisregardClient;
   }

   public void setLoadUnitSender_DisregardClient(String loadUnitSender_DisregardClient) {
      LoadUnitSender_DisregardClient = loadUnitSender_DisregardClient.equals("true");
   }

   public String getLoadUnitRetriever_RetrievalDestination() {
      return LoadUnitRetriever_RetrievalDestination;
   }

   public void setLoadUnitRetriever_RetrievalDestination(String loadUnitRetriever_RetrievalDestination) {
      LoadUnitRetriever_RetrievalDestination = loadUnitRetriever_RetrievalDestination;
   }

   public String getLoadUnitRetriever_LoopDelay() {
      return LoadUnitRetriever_LoopDelay;
   }

   public void setLoadUnitRetriever_LoopDelay(String loadUnitRetriever_LoopDelay) {
      LoadUnitRetriever_LoopDelay = loadUnitRetriever_LoopDelay;
   }

   public boolean isLoadUnitRetriever_LoopEnabled() {
      return LoadUnitRetriever_LoopEnabled;
   }

   public void setLoadUnitRetriever_LoopEnabled(String loadUnitRetriever_LoopEnabled) {
      LoadUnitRetriever_LoopEnabled = loadUnitRetriever_LoopEnabled.equals("true");
   }

   public boolean isLoadUnitRetriever_StoredCheckEnabled() {
      return LoadUnitRetriever_StoredCheckEnabled;
   }

   public void setLoadUnitRetriever_StoredCheckEnabled(String loadUnitRetriever_StoredCheckEnabled) {
      LoadUnitRetriever_StoredCheckEnabled = loadUnitRetriever_StoredCheckEnabled.equals("true");
   }

   public SqlManager getDatabaseParameters() {
      SqlManager databaseConnection = new SqlManager();
      databaseConnection.setUsername(this.UsernameDB);
      databaseConnection.setPassword(this.PasswordDB);
      databaseConnection.setHostname(this.HostnameDB);
      databaseConnection.setPort(this.PortDB);
      databaseConnection.setSID(this.SidDB);
      return databaseConnection;
   }

   @Override
   public String toString() {
      return "GeneralConfigurationModel{" +
              "ConfigurationName='" + ConfigurationName + '\'' +
              ", CreateTransportRestCall='" + CreateTransportRestCall + '\'' +
              ", UsernameDB='" + UsernameDB + '\'' +
              ", PasswordDB='" + PasswordDB + '\'' +
              ", HostnameDB='" + HostnameDB + '\'' +
              ", PortDB='" + PortDB + '\'' +
              ", SidDB='" + SidDB + '\'' +
              ", LoadUnitExtractor_Prefix='" + LoadUnitExtractor_Prefix + '\'' +
              ", LoadUnitExtractor_PrefixLength='" + LoadUnitExtractor_PrefixLength + '\'' +
              ", LoadUnitSender_SourceLocation='" + LoadUnitSender_SourceLocation + '\'' +
              ", LoadUnitSender_DestinationLocation='" + LoadUnitSender_DestinationLocation + '\'' +
              ", LoadUnitSender_Delay='" + LoadUnitSender_Delay + '\'' +
              ", LoadUnitSender_Clarify=" + LoadUnitSender_Clarify +
              ", LoadUnitSender_DisregardClient=" + LoadUnitSender_DisregardClient +
              ", LoadUnitRetriever_RetrievalDestination='" + LoadUnitRetriever_RetrievalDestination + '\'' +
              ", LoadUnitRetriever_LoopDelay='" + LoadUnitRetriever_LoopDelay + '\'' +
              ", LoadUnitRetriever_LoopEnabled=" + LoadUnitRetriever_LoopEnabled +
              ", LoadUnitRetriever_StoredCheckEnabled=" + LoadUnitRetriever_StoredCheckEnabled +
              '}';
   }
}
