package venckus.gytis.model;

public class LoadUnitSenderConfigurationModel {

   private String restInput;
   private String dbusername;
   private String dbpassword;
   private String dbhostname;
   private String dbport;
   private String dbsid;
   private String sourcelocation;
   private String destinationlocation;
   private String delay;
   private boolean clarify;
   private boolean disregardClient;

   public LoadUnitSenderConfigurationModel() {
      this.restInput = "";
      this.dbusername = "";
      this.dbpassword = "";
      this.dbhostname = "";
      this.dbport = "";
      this.dbsid = "";
      this.sourcelocation = "";
      this.destinationlocation = "";
      this.delay = "";
      this.clarify = false;
      this.disregardClient = false;
   }

   public String getRestInput() {
      return restInput;
   }

   public void setRestInput(String restInput) {
      this.restInput = restInput;
   }

   public String getDbusername() {
      return dbusername;
   }

   public void setDbusername(String dbusername) {
      this.dbusername = dbusername;
   }

   public String getDbpassword() {
      return dbpassword;
   }

   public void setDbpassword(String dbpassword) {
      this.dbpassword = dbpassword;
   }

   public String getDbhostname() {
      return dbhostname;
   }

   public void setDbhostname(String dbhostname) {
      this.dbhostname = dbhostname;
   }

   public String getDbport() {
      return dbport;
   }

   public void setDbport(String dbport) {
      this.dbport = dbport;
   }

   public String getDbsid() {
      return dbsid;
   }

   public void setDbsid(String dbsid) {
      this.dbsid = dbsid;
   }

   public String getSourcelocation() {
      return sourcelocation;
   }

   public void setSourcelocation(String sourcelocation) {
      this.sourcelocation = sourcelocation;
   }

   public String getDestinationlocation() {
      return destinationlocation;
   }

   public void setDestinationlocation(String destinationlocation) {
      this.destinationlocation = destinationlocation;
   }

   public String getDelay() {
      return delay;
   }

   public void setDelay(String delay) {
      this.delay = delay;
   }

   public boolean isClarify() {
      return clarify;
   }

   public void setClarify(String clarify) {
      this.clarify = clarify.equals("true");
   }

   public boolean isDisregardClient() {
      return disregardClient;
   }

   public void setDisregardClient(String disregardClient) {
      this.disregardClient = disregardClient.equals("true");
   }

   @Override
   public String toString() {
      return "LoadUnitSenderConfigurationModel{" +
              "restInput='" + restInput + '\'' +
              ", dbusername='" + dbusername + '\'' +
              ", dbpassword='" + dbpassword + '\'' +
              ", dbhostname='" + dbhostname + '\'' +
              ", dbport='" + dbport + '\'' +
              ", dbsid='" + dbsid + '\'' +
              ", sourcelocation='" + sourcelocation + '\'' +
              ", destinationlocation='" + destinationlocation + '\'' +
              ", delay='" + delay + '\'' +
              ", clarify=" + clarify +
              ", disregardClient=" + disregardClient +
              '}';
   }
}
