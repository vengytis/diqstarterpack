package venckus.gytis.model;

public class LoadUnitExtractorConfigurationModel {

   private String prefixLength;
   private String prefix;

   public String getPrefixLength() {
      return prefixLength;
   }

   public void setPrefixLength(String prefixLength) {
      this.prefixLength = prefixLength;
   }

   public String getPrefix() {
      return prefix;
   }

   public void setPrefix(String prefix) {
      this.prefix = prefix;
   }

   @Override
   public String toString() {
      return "LoadUnitExtractorModel{" +
              "prefixLength='" + prefixLength + '\'' +
              ", prefix='" + prefix + '\'' +
              '}';
   }
}
