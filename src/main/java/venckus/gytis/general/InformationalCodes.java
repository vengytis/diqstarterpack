package venckus.gytis.general;

public class InformationalCodes {
   public static final String PROGRAM_NAME_LOAD_UNIT_EXTRACTOR = "LoadUnitExtractor";
   public static final String PROGRAM_NAME_LOAD_UNIT_SENDER = "LoadUnitSender";
   public static final String PROGRAM_NAME_LOAD_UNIT_RETRIEVER = "LoadUnitRetriever";
   public static final String PROGRAM_NAME_STORAGE_TESTS = "StorageTests";
   public static final String PROGRAM_NAME_IMPROVED_MESSAGE_TRACE = "ImprovedMessageTrace";
   public static final String PROGRAM_NAME_HOST_SIMULATOR = "HostSimulator";
   public static final String PROGRAM_NAME_PERFORMANCE_ANALYZER = "PerformanceAnalyzer";

   public static final String IMT_EXCEPTION_MESSAGE = "This message is exception, so it is always correct";
   public static final String IMT_DUPLICATE_MESSAGE = "Message is dupplicate";
   public static final String IMT_INVALID_MAPING = "Message is not valid or mapping is incorrect";
   public static final String IMT_MAPPING_MESSAGES = "Mapping retrieved messages";
   public static final String IMT_TULL_MESSAGE_NOTE = "TULL cannot be sent from location, because previous message is from different location";
}
