package venckus.gytis.general;

import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.database.SqlManager;
import venckus.gytis.database.SqlQueries;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Validations {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private FilesManager FilesManager;

   /**
    * Constructor with provided FilesManager
    * @param filesManager for logging/output operations
    */
   public Validations(FilesManager filesManager) {
      FilesManager = filesManager;
   }

   /**
    * Constructor
    */
   public Validations() {
      FilesManager = new FilesManager();
      //in case class does not use FilesManager object and we need to create new one
   }

   /**
    * Checks if it is possible to establish connection to database
    * @param outputTextArea text area for error output
    * @return true if connection was established, false otherwise
    */
   public boolean validateDatabaseConnection(TextArea outputTextArea, TextField username, TextField password, TextField hostname, TextField port, TextField sid) {
      //TODO connection validation should be done on different thread, since it is not instant and may cause the program to be unresponsive
      Connection connection;
      if (!validateDatabaseParameters(outputTextArea, username, password, hostname, port, sid)) {
         return false;
      } else {
         SqlManager sqlManager = new SqlManager(hostname.getText(), port.getText(), sid.getText(), username.getText(), password.getText());
         connection = sqlManager.connect();
         if (connection == null) {
            FilesManager.printToOutputTextArea(sqlManager.getErrorMessage(), outputTextArea);
            return false;
         } else {
            try {
               connection.close();
               return true;
            } catch (SQLException e) {
               FilesManager.printToOutputTextArea(e.getMessage(), outputTextArea);
               return false;
            }
         }
      }
   }
   //TODO resolve dupplicate methods
   public boolean validateDatabaseConnection(TextArea outputTextArea, String username, String password, String hostname, String port, String sid) {
      //TODO connection validation should be done on different thread, since it is not instant and may cause the program to be unresponsive
      Connection connection;
      if (!validateDatabaseParameters(outputTextArea, username, password, hostname, port, sid)) {
         return false;
      } else {
         SqlQueries sqlQueries = new SqlQueries(hostname, port, sid, username, password);
         connection = sqlQueries.connect();
         if (connection == null) {
            return false;
         } else {
            try {
               connection.close();
               return true;
            } catch (SQLException e) {
               return false;
            }
         }
      }
   }

   /**
    * Fields validation for parameters used to establish DB connection
    * @param outputTextArea text area for error output
    * @return true if all fields are valid
    */
   private boolean validateDatabaseParameters(TextArea outputTextArea, TextField username, TextField password, TextField hostname, TextField port, TextField sid) {
      if (username.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_USERNAME_FIELD, outputTextArea);
         return false;
      }
      if (password.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_PASSWORD_FIELD, outputTextArea);
         return false;
      }
      if (hostname.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_HOSTNAME_FIELD, outputTextArea);
         return false;
      }
      if (port.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_PORT_FIELD, outputTextArea);
         return false;
      }
      if (sid.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_SID_FIELD, outputTextArea);
         return false;
      }
      return true;
   }
   //TODO resolve dupplicate methods
   private boolean validateDatabaseParameters(TextArea outputTextArea, String username, String password, String hostname, String port, String sid) {
      if (username.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_USERNAME_FIELD, outputTextArea);
         return false;
      }
      if (password.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_PASSWORD_FIELD, outputTextArea);
         return false;
      }
      if (hostname.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_HOSTNAME_FIELD, outputTextArea);
         return false;
      }
      if (port.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_PORT_FIELD, outputTextArea);
         return false;
      }
      if (sid.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DB_SID_FIELD, outputTextArea);
         return false;
      }
      return true;
   }

   /**
    * Fields validation for LoadUnitExtractor parameters
    * @param outputTextArea text area for error output
    * @return true if all fields are valid
    */
   public boolean loadUnitExtractorValidateParameters(TextArea outputTextArea, String inputText, String loadUnitPrefix, String loadUnitIdLength) {
      if (inputText.isEmpty()) {
         logger.trace(ErrorCodes.EMPTY_INPUT_AREA);
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_INPUT_AREA, outputTextArea);
         return false;
      }
      if (loadUnitPrefix.isEmpty()) {
         logger.trace(ErrorCodes.EMPTY_LU_PREFIX_FIELD);
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LU_PREFIX_FIELD, outputTextArea);
         return false;
      }
      if (loadUnitIdLength.isEmpty()) {
         logger.trace(ErrorCodes.EMPTY_LU_LENGTH_FIELD);
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LU_LENGTH_FIELD, outputTextArea);
         return false;
      }
      if (Integer.parseInt(loadUnitIdLength) <= 0) {
         logger.trace(ErrorCodes.LU_PREFIX_FIELD_TOO_SHORT_OR_INCORRECT);
         FilesManager.printToOutputTextArea(ErrorCodes.LU_PREFIX_FIELD_TOO_SHORT_OR_INCORRECT, outputTextArea);
         return false;
      }
      if (loadUnitPrefix.length() > Integer.parseInt(loadUnitIdLength)) {
         logger.trace(ErrorCodes.PREFIX_LONGER_THAN_LENGTH);
         FilesManager.printToOutputTextArea(ErrorCodes.PREFIX_LONGER_THAN_LENGTH, outputTextArea);
         return false;
      }
      return true;
   }

   /**
    * Fields validation for LoadUnitSender parameters
    * @param outputTextArea text area for error output
    * @return true if all fields are valid
    */
   public boolean loadUnitSenderValidateParameters(TextArea outputTextArea, String createTransportByCoordEndpoint, String sourceLocation, String destinationLocation, String delay) {
      if (createTransportByCoordEndpoint.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_REST_FIELD, outputTextArea);
         return false;
      }
      if (sourceLocation.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_SOURCE_LOCATION_FIELD, outputTextArea);
         return false;
      }
      if (destinationLocation.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DESTINATION_LOCATION_FIELD, outputTextArea);
         return false;
      }
      if (delay.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DELAY_FIELD, outputTextArea);
         return false;
      }
      if (Double.parseDouble(delay) <= 0) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_DELAY_FIELD, outputTextArea);
         return false;
      }
      return true;
   }

   /**
    * Fields validation for LoadUnitRetriever parameters
    * @param outputTextArea text area for error output
    * @return true if all fields are valid
    */
   public boolean loadUnitRetrieverValidateParameters(TextArea outputTextArea, boolean loopEnabled, String createTransportByCoordEndpoint, String inputTextarea, String destinationLocation, String loopDelay) {
      if (createTransportByCoordEndpoint.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_REST_FIELD, outputTextArea);
         return false;
      }
      if (inputTextarea.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LOAD_UNITS_AREA, outputTextArea);
         return false;
      }
      if (destinationLocation.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DESTINATION_LOCATION_FIELD, outputTextArea);
         return false;
      }
      if (loopEnabled) {
         //if loop option is selected, we have to make additional validations
         if (loopDelay.isEmpty()) {
            FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LOOP_DELAY_FIELD, outputTextArea);
            return false;
         }
         if (Double.parseDouble(loopDelay) <= 0) {
            FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_DELAY_FIELD, outputTextArea);
            return false;
         }
      }
      return true;
   }

   /**
    * Fields validation for LoadUnitExtractor parameters
    * @param outputTextArea text area for error output
    * @return true if all fields are valid
    */
   public boolean throughputMonitorValidateParameters(TextArea outputTextArea, LocalDate date, String startTime, String endTime, ArrayList<String> locations) {
      String timePattern = "([01]?[0-9]|2[0-3]):[0-5][0-9]"; // represents 24 hour pattern
      DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");

      if (date == null) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DATE_FIELD, outputTextArea);
         return false;
      }
      if (startTime.isEmpty() || !startTime.matches(timePattern)) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_OR_INCORRECT_START_TIME_FIELD, outputTextArea);
         return false;
      }
      if (endTime.isEmpty() || !endTime.matches(timePattern)) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_OR_INCORRECT_END_TIME_FIELD, outputTextArea);
         return false;
      }

      //date and time fields are in correct format, now need to check if they make sense
      DateTime startDateTime = formatter.parseDateTime(String.format("%s %s", date, startTime));
      DateTime endDateTime = formatter.parseDateTime(String.format("%s %s", date, endTime));

      if (endDateTime.isBefore(startDateTime) || endDateTime.isEqual(startDateTime)) {
         FilesManager.printToOutputTextArea(ErrorCodes.END_TIME_BEFORE_START_TIME, outputTextArea);
         return false;
      }

      if (locations.isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LOCATIONS_LIST, outputTextArea);
         return false;
      }
      return true;
   }

   /**
    * Fields validation for DMSLevelsTest parameters
    * @param outputTextArea text area for error output
    * @return true if all fields are valid
    */
   public boolean dmsLevelsTestValidateParameters(TextArea outputTextArea, TextField createTransportByCoordEndpoint, TextField numberOfAisles,
                                                  TextField numberOfLevels, TextField loadUnitsPerLevel, TextField firstAisleNumber, TextField workstation, TextField facility, TextField delay, GridPane aislesGrid) {
      if (createTransportByCoordEndpoint.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_REST_FIELD, outputTextArea);
         return false;
      }
      if (numberOfAisles.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_NUMBER_OF_AISLES_FIELD, outputTextArea);
         return false;
      }
      try {
         Integer.parseInt(numberOfAisles.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_NUMBER_OF_AISLES_FIELD, outputTextArea);
         return false;
      }
      if (numberOfLevels.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_NUMBER_OF_LEVELS_FIELD, outputTextArea);
         return false;
      }
      try {
         Integer.parseInt(numberOfLevels.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_NUMBER_OF_LEVELS_FIELD, outputTextArea);
         return false;
      }
      if (loadUnitsPerLevel.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LOAD_UNITS_PER_LEVEL_FIELD, outputTextArea);
         return false;
      }
      try {
         Integer.parseInt(loadUnitsPerLevel.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_LOAD_UNITS_PER_LEVEL_FIELD, outputTextArea);
         return false;
      }
      try {
         Integer.parseInt(firstAisleNumber.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_FIRST_AISLE_NUMBER_FIELD, outputTextArea);
         return false;
      }
      if (workstation.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_WORKSTATION_FIELD, outputTextArea);
         return false;
      }
      if (facility.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_FACILITY_FIELD, outputTextArea);
         return false;
      }
      if (delay.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_DELAY_FIELD, outputTextArea);
         return false;
      }
      try {
         Double.parseDouble(delay.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_DELAY_FIELD, outputTextArea);
         return false;
      }
      if (!dmsLevelsTestValidateAislesGrid(aislesGrid)) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_AISLE_SELECTION_GRID, outputTextArea);
         return false;
      }
      return true;
   }

   /**
    * Checks if any of aisles in the grid are selected
    * @param aislesGrid grid for aisle selection
    * @return true if at least one is selected, false if none
    */
   private boolean dmsLevelsTestValidateAislesGrid(GridPane aislesGrid) {
      for (Node node : aislesGrid.getChildren()) {
         if (node instanceof CheckBox) {
            if (((CheckBox) node).isSelected()) {
               return true;
            }
         }
      }
      return false;
   }

   /**
    * Fields validation for ShuffleDMS parameters
    * @param outputArea text area for error output
    * @return true if all fields are valid
    */
   public boolean shuffleDMS_ValidateParameters(TextArea outputArea, TextField loadUnitsCount, TextField createTransportByCoordEndpoint, TextField facility, TextField numberOfAisles,
                                                TextField numberOfLevels, TextField firstAisleNumber) {
      if (loadUnitsCount.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LOAD_UNITS_COUNT_FIELD, outputArea);
         return false;
      }
      try {
         Integer.parseInt(loadUnitsCount.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_LOAD_UNITS_COUNT_FIELD, outputArea);
         return false;
      }
      if (createTransportByCoordEndpoint.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_REST_FIELD, outputArea);
         return false;
      }
      if (facility.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_FACILITY_FIELD, outputArea);
         return false;
      }
      if (numberOfAisles.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_NUMBER_OF_AISLES_FIELD, outputArea);
         return false;
      }
      try {
         Integer.parseInt(numberOfAisles.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_NUMBER_OF_AISLES_FIELD, outputArea);
         return false;
      }
      if (numberOfLevels.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_NUMBER_OF_LEVELS_FIELD, outputArea);
         return false;
      }
      try {
         Integer.parseInt(numberOfLevels.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_NUMBER_OF_LEVELS_FIELD, outputArea);
         return false;
      }
      try {
         Integer.parseInt(firstAisleNumber.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_FIRST_AISLE_NUMBER_FIELD, outputArea);
         return false;
      }
      return true;
   }

   /**
    * Fields validation for ImprovedMessageTrace parameters
    * @param outputTextArea text area for error output
    * @return true if all fields are valid
    */
   public boolean improvedTrace_ValidateParameters(TextArea outputTextArea, TextField pathToInterface, Label statusLabel, TextField numberOfAisles, TextField firstAisle,
                                                   TextField numberOfLevels, TextField maximumX, TextField improvedTrace_binDepth, TextField timestamp) {
      if (pathToInterface.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_PATH_TO_DIQ_PLC_INTERFACE, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      try {
         new XSSFWorkbook(pathToInterface.getText());
      } catch (Throwable e) {
         FilesManager.printToOutputTextArea(e.getLocalizedMessage(), outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      if (numberOfAisles.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_NUMBER_OF_AISLES_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      try {
         Integer.parseInt(numberOfAisles.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_NUMBER_OF_AISLES_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      if (firstAisle.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_FIRST_AISLE_NUMBER_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      try {
         Integer.parseInt(firstAisle.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_FIRST_AISLE_NUMBER_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      if (numberOfLevels.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_NUMBER_OF_LEVELS_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      try {
         Integer.parseInt(numberOfLevels.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_NUMBER_OF_LEVELS_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      if (maximumX.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_MAXIMUM_X_LOCATION_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      try {
         Integer.parseInt(maximumX.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_MAXIMUM_X_LOCATION_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      if (improvedTrace_binDepth.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_BIN_DEPTH_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      try {
         Integer.parseInt(improvedTrace_binDepth.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_BIN_DEPTH_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      if (timestamp.getText().isEmpty()) {
         FilesManager.printToOutputTextArea(ErrorCodes.EMPTY_LU_RETRIEVAL_TIMESTAMP_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      try {
         Integer.parseInt(timestamp.getText());
      } catch (NumberFormatException e) {
         FilesManager.printToOutputTextArea(ErrorCodes.INCORRECT_LU_RETRIEVAL_TIMESTAMP_FIELD, outputTextArea);
         statusLabel.setText("Error found - check Console/Log tab");
         return false;
      }
      return true;
   }
}
