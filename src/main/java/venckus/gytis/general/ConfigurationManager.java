package venckus.gytis.general;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.javafx.UI.ResourceLoader;
import venckus.gytis.model.GeneralConfigurationModel;

import java.io.*;
import java.util.Map;

public class ConfigurationManager {

   private static Logger logger = LoggerFactory.getLogger(ConfigurationManager.class);

   private static GeneralConfigurationModel CurrentConfiguration;

   public static GeneralConfigurationModel getCurrentConfiguration() {
      return CurrentConfiguration;
   }

   public static void setCurrentConfiguration(GeneralConfigurationModel currentConfiguration) {
      CurrentConfiguration = currentConfiguration;
   }

   public static void updateCurrentConfiguration(String propertiesFileName, Map<String, String> updatedConfigurationMap) {
      try {
         Configurations configs = new Configurations();
         FileBasedConfigurationBuilder builder = configs.propertiesBuilder(new File(ResourceLoader.CONFIGURATIONS_PATH + propertiesFileName));
         PropertiesConfiguration config = (PropertiesConfiguration) builder.getConfiguration();
         for (Map.Entry<String, String> entry : updatedConfigurationMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            config.setProperty(key, value);
            builder.save();
         }
      } catch (Exception e) {
         logger.warn(e.getMessage(), e);
      }
   }
}
