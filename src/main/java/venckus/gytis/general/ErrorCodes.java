package venckus.gytis.general;

@SuppressWarnings("WeakerAccess")
public class ErrorCodes {
   //----------------------------------------------------------Database----------------------------------------------------------
   protected static final String EMPTY_DB_USERNAME_FIELD = "Empty database username field";
   protected static final String EMPTY_DB_PASSWORD_FIELD = "Empty database password field";
   protected static final String EMPTY_DB_HOSTNAME_FIELD = "Empty database hostname field";
   protected static final String EMPTY_DB_PORT_FIELD = "Empty database port field";
   protected static final String EMPTY_DB_SID_FIELD = "Empty database sid field";
   //--------------------------------------------------Load Unit Extractor-------------------------------------------------------
   protected static final String EMPTY_INPUT_AREA = "Empty input area field";
   protected static final String EMPTY_LU_PREFIX_FIELD = "Empty Load Unit id prefix field";
   protected static final String EMPTY_LU_LENGTH_FIELD = "Empty Load Unit id length field";
   protected static final String PREFIX_LONGER_THAN_LENGTH = "Prefix should not be longer than load unit length";
   protected static final String LU_PREFIX_FIELD_TOO_SHORT_OR_INCORRECT = "Load Unit id length is too short or incorrect";
   //----------------------------------------------------------------------------------------------------------------------------
   protected static final String EMPTY_REST_FIELD = "Empty rest call field";
   protected static final String EMPTY_SOURCE_LOCATION_FIELD = "Empty source location field";
   protected static final String EMPTY_DESTINATION_LOCATION_FIELD = "Empty destination location field";
   protected static final String EMPTY_DELAY_FIELD = "Empty delay field";
   protected static final String EMPTY_NUMBER_OF_AISLES_FIELD = "Empty number of aisles field";
   protected static final String EMPTY_NUMBER_OF_LEVELS_FIELD = "Empty number of levels field";
   protected static final String EMPTY_LOAD_UNITS_PER_LEVEL_FIELD = "Empty load units per level field";
   protected static final String EMPTY_WORKSTATION_FIELD = "Empty workstation field";
   protected static final String EMPTY_FACILITY_FIELD = "Empty facility field";
   protected static final String EMPTY_AISLE_SELECTION_GRID = "No aisles selected";
   protected static final String EMPTY_FIRST_AISLE_NUMBER_FIELD = "Empty first aisle number field";
   protected static final String EMPTY_MAXIMUM_X_LOCATION_FIELD = "Empty maximum x location of bins field";
   protected static final String EMPTY_BIN_DEPTH_FIELD = "Empty bin depth field";
   protected static final String EMPTY_LU_RETRIEVAL_TIMESTAMP_FIELD = "Empty LU retrieval timestamp field";
   protected static final String EMPTY_LOAD_UNITS_AREA = "Empty load units area";
   protected static final String EMPTY_RETRIEVAL_DESTINATION_FIELD = "Empty retrieval destination field";
   protected static final String EMPTY_LOOP_DELAY_FIELD = "Loop delay field cannot be empty if looping option is selected";
   protected static final String EMPTY_LOAD_UNITS_COUNT_FIELD = "Empty load units count field";
   protected static final String EMPTY_PATH_TO_DIQ_PLC_INTERFACE = "Empty path to diq/plc interface field";

   protected static final String EMPTY_DATE_FIELD = "No date selected";
   protected static final String EMPTY_OR_INCORRECT_START_TIME_FIELD = "Start time field is empty or does not match HH:mm pattern";
   protected static final String EMPTY_OR_INCORRECT_END_TIME_FIELD = "End time field is empty or does not match HH:mm pattern";
   protected static final String END_TIME_BEFORE_START_TIME = "Start time must be before end time";
   protected static final String EMPTY_LOCATIONS_LIST = "No locations added";

   protected static final String INCORRECT_LU_RETRIEVAL_TIMESTAMP_FIELD = "Incorrect LU retrieval timestamp field format, integer is expected";
   protected static final String INCORRECT_BIN_DEPTH_FIELD = "Incorrect bin depth field format, integer is expected";
   protected static final String INCORRECT_MAXIMUM_X_LOCATION_FIELD = "Incorrect maximum X location of bins field format, integer is expected";
   protected static final String INCORRECT_FIRST_AISLE_NUMBER_FIELD = "Incorrect first aisle number field format, integer is expected";
   protected static final String INCORRECT_LOAD_UNITS_PER_LEVEL_FIELD = "Incorrect load units per level field format, integer is expected";
   protected static final String INCORRECT_NUMBER_OF_AISLES_FIELD = "Incorrect number of aisles field format, integer is expected";
   protected static final String INCORRECT_NUMBER_OF_LEVELS_FIELD = "Incorrect number of levels field format, integer is expected";
   protected static final String INCORRECT_DELAY_FIELD = "Incorrect delay field format, number with value > 0 is expected";
   protected static final String INCORRECT_LOAD_UNITS_COUNT_FIELD = "Incorrect load units count field format, integer is expected";
}
