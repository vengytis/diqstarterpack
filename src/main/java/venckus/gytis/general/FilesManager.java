package venckus.gytis.general;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextArea;
import org.joda.time.LocalTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import venckus.gytis.javafx.UI.ResourceLoader;
import venckus.gytis.model.*;
import venckus.gytis.programs.improvedmessagetrace.entity.LocationException;
import venckus.gytis.programs.improvedmessagetrace.entity.Message;


import java.io.*;
import java.util.*;

public class FilesManager {

   /**
    * Constructor
    */
   public FilesManager() {

   }

   /**
    * Prints output to specified file and creates directories if they do not exist yet
    * @param text output to print
    * @param directoryName directory where log file should be created
    * @param fileName name of log file
    */
   public void writeToFile(String text, String directoryName, String fileName) {
      createDirectoryIfNotExist("logs");
      createDirectoryIfNotExist("logs/" + directoryName);
      try (FileWriter fw = new FileWriter("logs/" + directoryName + "/" + fileName, true);
           BufferedWriter bw = new BufferedWriter(fw);
           PrintWriter out = new PrintWriter(bw)) {
         out.println("[" + LocalTime.now() + "] " + text);
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   /**
    * Checks if directory with specified {pathName} exists and creates if it does not exist
    * @param pathName relative path to directory, for example "logs"
    */
   private void createDirectoryIfNotExist(String pathName) {
      File directory = new File(pathName);
      if (!directory.exists()) {
         boolean directoryCreated = directory.mkdir();
         if (!directoryCreated) {
            //Currently no handling implemented, maybe retry attempt would be a smart choice here
         }
      }
   }

   /**
    * Generates unique text ".txt" file
    * @param filename information included in generated name
    * @return generated file name of format "{currentSystemTimeInMills}_{information}.txt
    */
   public String generateFileName(String filename) {
      return System.currentTimeMillis() + "_" + filename + ".txt";
   }

   /**
    * Prints output to text area on FX thread
    * @param text ouput to be printed
    * @param outputTextArea area where output should be placed
    */
   public void printToOutputTextArea(String text, TextArea outputTextArea) {
      System.out.println(text);
      String outputText = (text.endsWith("\n")) ? "[" + LocalTime.now() + "] " + text : "[" + LocalTime.now() + "] " + text + '\n';
      Platform.runLater(() -> outputTextArea.appendText(outputText));
   }

   /**
    * Prints output to text area and file
    * @param text output to be printed
    * @param outputTextArea area where output should be placed
    * @param logDirectoryName directory of log file (usually corresponds to name of program, for example "LoadUnitSender")
    * @param logFileName log file name
    */
   public void printToOutputTextAreaAndFile(String text, TextArea outputTextArea, String logDirectoryName, String logFileName) {
      printToOutputTextArea(text, outputTextArea);
      writeToFile(text, logDirectoryName, logFileName);
   }

   /**
    * Retrieves available configurations for program (if there are any)
    * @return available configurations
    */
   @SuppressWarnings("ConstantConditions")
   public ObservableList<String> getAvailableConfigurations() {
      ObservableList<String> configurations = FXCollections.observableArrayList();
      try {
         File inputFile = new File(ResourceLoader.CONFIGURATIONS_PATH);
         for (final File fileEntry : inputFile.listFiles()) {
            String fileName = fileEntry.getName();
            if (fileName.endsWith(".properties")) {
               //if it's not properties file - ignore it
               configurations.add(fileEntry.getName());
            }
         }
      } catch (Exception e) {
         //in case of any exception - just leave the list as is (empty)
      }
      return configurations;
   }

   public LinkedHashMap getPropertiesMapFromPropertiesFile(String propertiesFileName) {
      LinkedHashMap<String, String> propertiesMap = new LinkedHashMap<>();
      try {
         propertiesFileName = propertiesFileName.endsWith(".properties") ? propertiesFileName : propertiesFileName + ".properties";
         propertiesMap = getOrderedProperties(new FileInputStream(new File(ResourceLoader.CONFIGURATIONS_PATH + propertiesFileName)));
      } catch (NullPointerException nptr) {
         //if some field fails - assign default values
         //  loadUnitSenderConfigurationModel = new LoadUnitSenderConfigurationModel();
      } catch (IOException e) {
         e.printStackTrace();
      }
      return propertiesMap;
   }

   private LinkedHashMap<String, String> getOrderedProperties(InputStream in) throws IOException {
      LinkedHashMap<String, String> mp = new LinkedHashMap<>();
      (new Properties(){
         public synchronized Object put(Object key, Object value) {
            return mp.put((String) key, (String) value);
         }
      }).load(in);
      return mp;
   }

   public GeneralConfigurationModel getGeneralConfigurationModel(String pathToConfigs, String propertiesFileName) {
      GeneralConfigurationModel generalConfigurationModel = new GeneralConfigurationModel();
      try {
         Properties properties = new Properties();
         InputStream configInputStream = null;
         if (pathToConfigs.equals(ResourceLoader.CONFIGURATIONS_PATH)) {
            propertiesFileName = propertiesFileName.endsWith(".properties") ? propertiesFileName : propertiesFileName + ".properties";
            configInputStream = new FileInputStream(new File(pathToConfigs + propertiesFileName));
         } else if (pathToConfigs.equals(ResourceLoader.DEFAULT_CONFIGURATION)) {
            configInputStream = (getClass().getClassLoader().getResourceAsStream(ResourceLoader.DEFAULT_CONFIGURATION));
         }
         assert configInputStream != null;
         properties.load(configInputStream);
         generalConfigurationModel.setConfigurationName(propertiesFileName);
         generalConfigurationModel.setCreateTransportRestCall(properties.getProperty("CreateTransportRestCall"));
         generalConfigurationModel.setUsernameDB(properties.getProperty("UsernameDB"));
         generalConfigurationModel.setPasswordDB(properties.getProperty("PasswordDB"));
         generalConfigurationModel.setHostnameDB(properties.getProperty("HostnameDB"));
         generalConfigurationModel.setPortDB(properties.getProperty("PortDB"));
         generalConfigurationModel.setSidDB(properties.getProperty("SidDB"));
         generalConfigurationModel.setLoadUnitExtractor_Prefix(properties.getProperty("LoadUnitExtractor_Prefix"));
         generalConfigurationModel.setLoadUnitExtractor_PrefixLength(properties.getProperty("LoadUnitExtractor_PrefixLength"));
         generalConfigurationModel.setLoadUnitSender_SourceLocation(properties.getProperty("LoadUnitSender_SourceLocation"));
         generalConfigurationModel.setLoadUnitSender_DestinationLocation(properties.getProperty("LoadUnitSender_DestinationLocation"));
         generalConfigurationModel.setLoadUnitSender_Delay(properties.getProperty("LoadUnitSender_Delay"));
         generalConfigurationModel.setLoadUnitSender_Clarify(properties.getProperty("LoadUnitSender_Clarify"));
         generalConfigurationModel.setLoadUnitSender_DisregardClient(properties.getProperty("LoadUnitSender_DisregardClient"));
         generalConfigurationModel.setLoadUnitRetriever_RetrievalDestination(properties.getProperty("LoadUnitRetriever_RetrievalDestination"));
         generalConfigurationModel.setLoadUnitRetriever_LoopDelay(properties.getProperty("LoadUnitRetriever_LoopDelay"));
         generalConfigurationModel.setLoadUnitRetriever_LoopEnabled(properties.getProperty("LoadUnitRetriever_LoopEnabled"));
         generalConfigurationModel.setLoadUnitRetriever_StoredCheckEnabled(properties.getProperty("LoadUnitRetriever_StoredCheckEnabled"));
      } catch (NullPointerException nptr) {
         //if some field fails - assign default values
         //  loadUnitSenderConfigurationModel = new LoadUnitSenderConfigurationModel();
      } catch (IOException e) {
         e.printStackTrace();
      }
      return generalConfigurationModel;
   }

   /**
    * Retrieves description of program from "descriptions/" directory
    * @param descriptionFileName name of description file
    * @return String representation of description
    */
   public String getDescription(String descriptionFileName) {
      StringBuilder description = new StringBuilder();
      try {
         InputStream inputStream = getClass().getClassLoader().getResourceAsStream(String.format("%s%s%s","descriptions/", descriptionFileName, ".txt"));
         BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
         String resource;
         while ((resource = br.readLine()) != null) {
            description.append(resource).append("\n");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
      return description.toString();
   }

   /**
    * Maps location exceptions for ImprovedMessageTrace from json file to ArrayList
    * @param configName name of configuration, for example "OrtofruttaConfig"
    * @return mapped location exceptions as ArrayList
    */
   @SuppressWarnings("unchecked")
   public ArrayList<LocationException> mapLocationsExceptionsFromJSON(String configName) {
      ArrayList<LocationException> exceptions = new ArrayList<>();
      JSONParser parser = new JSONParser();
      try {
         BufferedReader br = new BufferedReader(new FileReader("exceptions.json"));
         Object obj = parser.parse(br);
         JSONObject exceptionsObject = (JSONObject) obj;
         JSONArray configurations = (JSONArray) exceptionsObject.get("Configurations");
         int configIndex = JSONConfigurationExists(configurations, configName);
         if (configIndex != -1) {
            JSONObject config = (JSONObject) configurations.get(configIndex);
            JSONArray exceptionsArray = (JSONArray) config.get(configName);
            Iterator<JSONObject> exceptionsIterator = exceptionsArray.iterator();
            while (exceptionsIterator.hasNext()) {
               JSONObject locationExceptionJSON = exceptionsIterator.next();
               LocationException locationException = new LocationException(
                       (String) locationExceptionJSON.get("operation"),
                       (String) locationExceptionJSON.get("location"),
                       (String) locationExceptionJSON.get("destination")
               );
               exceptions.add(locationException);
            }
         }
      } catch (Exception e) {
         //we don't care about badly formatted or missing exceptions.json file
         //because in both of these cases method will return empty list
         //and calling method to add new location as exception will create/overwrite file
      }
      return exceptions;
   }

   /**
    * Adds location as exception for provided configuration after parsing the message
    * @param configName name of configuration, for example "OrtofruttaConfig"
    * @param message message to be parsed and included as exception
    */
   @SuppressWarnings("unchecked")
   public void addLocationException(String configName, Message message) {
      String modifiedJSON = "";
      try {
         modifiedJSON = createLocationExceptionObject(configName, message);
      } catch (Exception e) {
         //When exceptions.json file does not exist we have to create one with json "Configurations" object
         JSONArray configurationsArray = new JSONArray();
         JSONObject exceptionsObject = new JSONObject();
         exceptionsObject.put("Configurations", configurationsArray);
         try {
            writeToExceptionsFile(exceptionsObject.toJSONString());
            modifiedJSON = createLocationExceptionObject(configName, message);
         } catch (Exception el) {
            e.printStackTrace();
         }
      } finally {
         //file is always created at this point and it should be overwritten by latest data
         try {
            writeToExceptionsFile(modifiedJSON);
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
   }

   /**
    * Overwrites existing json file with new from provided output
    * @param jsonOutput latest output
    * @throws Exception in case it's not possible to write to file, exception is handled elsewhere
    */
   private void writeToExceptionsFile(String jsonOutput) throws Exception {
      FileWriter fileWriter;
      fileWriter = new FileWriter("exceptions.json");
      fileWriter.write(jsonOutput);
      fileWriter.flush();
      fileWriter.close();
   }

   /**
    * Checks if configuration with provided {configName} exists
    * @param configurations list of available configurations
    * @param configName configuration to check against available ones
    * @return index of {configName} configuration in list, -1 if does not exist
    */
   @SuppressWarnings("unchecked")
   private int JSONConfigurationExists(JSONArray configurations, String configName) {
      int index = 0;
      Iterator<JSONObject> configsIterator = configurations.iterator();
      while (configsIterator.hasNext()) {
         JSONObject config = configsIterator.next();
         if (config.get(configName) != null) {
            return index;
         }
         index++;
      }
      return -1;
   }

   /**
    * Creates json object for new exception location
    * @param configName name of configuration, for example "OrtofruttaConfig"
    * @param message message to be parsed
    * @return String representation of created json object
    * @throws Exception handled in other places
    */
   @SuppressWarnings("unchecked")
   private String createLocationExceptionObject(String configName, Message message) throws Exception {
      JSONParser parser = new JSONParser();
      BufferedReader br = new BufferedReader(new FileReader("exceptions.json"));
      Object obj = parser.parse(br);
      JSONObject exceptionsObject = (JSONObject) obj;
      JSONArray configurations = (JSONArray) exceptionsObject.get("Configurations");
      int configIndex = JSONConfigurationExists(configurations, configName);
      JSONObject newLocationException = new JSONObject();
      newLocationException.put("operation", message.getOperationId());
      newLocationException.put("location", message.getCurrentLocation());
      newLocationException.put("destination", message.getDestinationLocation());
      if (configIndex == -1) {
         //when configuration does not exist we have to create one
         JSONArray newLocationExceptionArray = new JSONArray();
         newLocationExceptionArray.add(newLocationException);
         JSONObject newConfiguration = new JSONObject();
         newConfiguration.put(configName, newLocationExceptionArray);
         configurations.add(newConfiguration);
      } else {
         //when configuration exists we have to update it
         JSONObject config = (JSONObject) configurations.get(configIndex);
         JSONArray exceptionsArray = (JSONArray) config.get(configName);
         exceptionsArray.add(newLocationException);
      }
      return ((JSONObject) obj).toJSONString();
   }
}
