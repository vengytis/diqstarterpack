package venckus.gytis.javafx.GUI.mainGUI;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;

import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.database.SqlManager;
import venckus.gytis.database.SqlQueries;
import venckus.gytis.general.ConfigurationManager;
import venckus.gytis.general.FilesManager;
import venckus.gytis.general.InformationalCodes;
import venckus.gytis.general.Validations;
import venckus.gytis.javafx.UI.ResourceLoader;
import venckus.gytis.programs.dmslevelstest.DMSLevelsTest;
import venckus.gytis.programs.dmslevelstest.ShuffleDMS;
import venckus.gytis.programs.improvedmessagetrace.control.MessageChecker;
import venckus.gytis.programs.improvedmessagetrace.control.database.MessagesManagerSql;
import venckus.gytis.programs.improvedmessagetrace.control.subsystem.DMSManager;
import venckus.gytis.programs.improvedmessagetrace.entity.LoadUnit;
import venckus.gytis.programs.loadunitretriever.LoadUnitRetriever;
import venckus.gytis.programs.loadunitsender.LoadUnitSender;
import venckus.gytis.programs.loadunitextractor.LoadUnitExtractor;
import venckus.gytis.programs.performanceanalyzer.throughputmonitor.Telegram;
import venckus.gytis.programs.performanceanalyzer.throughputmonitor.ThroughputMonitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class Controller {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private LoadUnitExtractor LoadUnitExtractor;
   private SqlManager SqlManager;
   private SqlQueries SqlQueries;
   private LoadUnitSender LoadUnitSender;
   private LoadUnitRetriever LoadUnitRetriever;
   private DMSLevelsTest DMSLevelsTest;
   private MessageChecker MessageChecker;
   private ShuffleDMS ShuffleDMS;
   private ThroughputMonitor ThroughputMonitor;
   private FilesManager FilesManager;
   private Validations Validations;

   private Map<Button, Pane> ButtonPaneMap;

   private ObservableMap<String, String> LoadUnitSenderTransportsMap;
   private Map<String, Thread> LoadUnitSenderThreadsMap;
   private static Map<String, AtomicBoolean> LoadUnitSenderRunningThreadsMap;
   private static Map<String, AtomicBoolean> LoadUnitSenderStoppedThreadsMap;

   private ObservableMap<String, String> DMSLevelsTestTransportsMap;
   private Map<String, Thread> DMSLevelsTestThreadsMap;
   private static Map<String, AtomicBoolean> DMSLevelsTestRunningThreadsMap;
   private static Map<String, AtomicBoolean> DMSLevelsTestStoppedThreadsMap;

   private Thread shuffleDMSThread = new Thread();
   private AtomicBoolean shuffleDMSRunning = new AtomicBoolean();

   private ObservableMap<String, String> LoadUnitRetrieverTransportsMap;
   private Map<String, Thread> LoadUnitRetrieverThreadsMap;
   private static Map<String, AtomicBoolean> LoadUnitRetrieverRunningThreadsMap;
   private static Map<String, AtomicBoolean> LoadUnitRetrieverStoppedThreadsMap;

   private Thread improvedTraceThread = new Thread();
   private AtomicBoolean improvedTraceThreadRunning = new AtomicBoolean();
   public static MessagesManagerSql improvedTraceMessagesManagerSql;
   public static String improvedTracePathToExcel;
   public static String improvedTracelogFileName;
   private String improvedTraceConfigName;
   private ArrayList<LoadUnit> totalLoadUnits;

   private ObservableList<String> performanceAnalyzerLocations;

   @FXML
   private ImageView maximizedImageView;
   private Image maximizedIcon = new Image(String.valueOf(getClass().getClassLoader().getResource(ResourceLoader.SYSTEM_UI_MAXIMIZE_ICON)));
   private Image notMaximizedIcon = new Image(String.valueOf(getClass().getClassLoader().getResource(ResourceLoader.SYSTEM_UI_NOT_MAXIMIZE_ICON)));

   @FXML
   private Menu configurationsMenu;
   @FXML
   public Label currentConfigLabel;

   //FXML Menu buttons
   @FXML
   private Button loadUnitExtractorButton;
   @FXML
   private Button storageOperationsButton;
   @FXML
   private Button loadUnitSenderButton;
   @FXML
   private Button hostSimulatorButton;
   @FXML
   private Button loadUnitRetrieverButton;
   @FXML
   private Button improvedTraceButton;
   @FXML
   private Button performanceAnalyzerButton;
   @FXML
   private Button descriptionsButton;

   //FXML Panes
   @FXML
   private Pane loadUnitExtractorPane;
   @FXML
   private Pane storageOperationsPane;
   @FXML
   private Pane loadUnitSenderPane;
   @FXML
   private Pane hostSimulatorPane;
   @FXML
   private Pane loadUnitRetrieverPane;
   @FXML
   private Pane improvedTracePane;
   @FXML
   private Pane performanceAnalyzerPane;
   @FXML
   private Pane descriptionsPane;

   //FXML Description items
   @FXML
   private TextArea description_generalInfoDesc;
   @FXML
   private TextArea description_loadUnitExtractorDesc;
   @FXML
   private TextArea description_loadUnitSenderDesc;
   @FXML
   private TextArea description_loadUnitRetrieverDesc;
   @FXML
   private TextArea description_hostSimulatorDesc;
   @FXML
   private TextArea description_storageTestsDesc;
   @FXML
   private TextArea description_improvedTraceDesc;
   @FXML
   private TextArea description_performanceAnalyzerDesc;

   //Load Unit Extractor FXML items
   @FXML
   private TextArea loadUnitExtractor_inputField;
   @FXML
   private TextArea loadUnitExtractor_outputField;
   @FXML
   private Label loadUnitExtractor_copiedLabel;
   @FXML
   private TextField loadUnitExtractor_LUprefix;
   @FXML
   private TextField loadUnitExtractor_LUbarcodeLength;

   //Load Unit Sender FXML items
   @FXML
   private TextField loadUnitSender_sourcelocation;
   @FXML
   private TextField loadUnitSender_destinationlocation;
   @FXML
   private TextField loadUnitSender_delay;
   @FXML
   private TextArea loadUnitSender_logoutput;
   @FXML
   private TextField loadUnitSender_dbsid;
   @FXML
   private TextField loadUnitSender_dbport;
   @FXML
   private TextField loadUnitSender_dbhostname;
   @FXML
   private PasswordField loadUnitSender_dbpassword;
   @FXML
   private TextField loadUnitSender_dbusername;
   @FXML
   private TextField loadUnitSender_restInput;
   @FXML
   private ListView<String> loadUnitSender_threadsList;
   @FXML
   private ComboBox<String> loadUnitSender_clientList;
   @FXML
   private CheckBox loadUnitSender_clarifyCheckbox;
   @FXML
   private CheckBox loadUnitSender_disregardClientCheckbox;

   //LoadUnitRetriever items
   @FXML
   private TextField loadUnitRetriever_dbsid;
   @FXML
   private TextField loadUnitRetriever_dbport;
   @FXML
   private TextField loadUnitRetriever_dbhostname;
   @FXML
   private PasswordField loadUnitRetriever_dbpassword;
   @FXML
   private TextField loadUnitRetriever_dbusername;
   @FXML
   private TextArea loadUnitRetriever_loadUnits;
   @FXML
   private TextArea loadUnitRetriever_loopOutput;
   @FXML
   private TextField loadUnitRetriever_loopDelay;
   @FXML
   private CheckBox loadUnitRetriever_loopCheckbox;
   @FXML
   private CheckBox loadUnitRetriever_storedCheckbox;
   @FXML
   private TextField loadUnitRetriever_restInput;
   @FXML
   private TextField loadUnitRetriever_retrievalDestination;

   //DMS Levels Test items
   @FXML
   public TextField dmsLevelsTest_restInput;
   @FXML
   public TextField dmsLevelsTest_numberOfAisles;
   @FXML
   public TextField dmsLevelsTest_levelsPerAisle;
   @FXML
   public TextField dmsLevelsTest_delay;
   @FXML
   public TextField dmsLevelsTest_loadUnitsPerLevel;
   @FXML
   public TextField dmsLevelsTest_workstation;
   @FXML
   public TextField dmsLevelsTest_facility;
   @FXML
   public TextArea dmsLevelsTest_logoutput;
   @FXML
   public TextField dmsLevelsTest_dbusername;
   @FXML
   public PasswordField dmsLevelsTest_dbpassword;
   @FXML
   public TextField dmsLevelsTest_dbhostname;
   @FXML
   public TextField dmsLevelsTest_dbport;
   @FXML
   public TextField dmsLevelsTest_dbsid;
   @FXML
   public GridPane dmsLevelsTest_aislesGrid;
   @FXML
   private TextField dmsLevelsTest_aislePrefix;
   @FXML
   private ComboBox<String> dmsLevelsTest_configsList;

   //Shuffle DMS program items
   @FXML
   private TextField shuffleDMS_loadUnitsCount;
   @FXML
   public TextArea shuffleDMS_outputArea;

   //Improved Messages Trace items
   @FXML
   private ListView<String> improvedTrace_loadUnitsList;
   @FXML
   private ListView<String> improvedTrace_messagesList;
   @FXML
   private Button improvedTrace_addMessageToExceptionsButton;
   @FXML
   private TextField improvedTrace_timestamp;
   @FXML
   private CheckBox improvedTrace_onlyInvalidLUCheckbox;
   @FXML
   private TextField improvedTrace_locationPassed;
   @FXML
   private TextField improvedTrace_barcode;
   @FXML
   private TextArea improvedTrace_messageNote;
   @FXML
   private TextArea improvedTrace_comment;
   @FXML
   public TextField improvedTrace_dbusername;
   @FXML
   public PasswordField improvedTrace_dbpassword;
   @FXML
   public Label improvedTrace_retrievalStatus;
   @FXML
   public TextField improvedTrace_dbhostname;
   @FXML
   public TextField improvedTrace_dbport;
   @FXML
   public TextField improvedTrace_dbsid;
   @FXML
   public TextField improvedTrace_pathToExcel;
   @FXML
   private ComboBox<String> improvedTrace_configsList;
   @FXML
   public TextField improvedTrace_numberOfAisles;
   @FXML
   public TextField improvedTrace_MaximumX;
   @FXML
   public TextField improvedTrace_MaximumY;
   @FXML
   public TextField improvedTrace_firstAisleNumber;
   @FXML
   public TextField improvedTrace_binDepth;
   @FXML
   public ProgressBar improvedTrace_progressBar;
   @FXML
   public TextArea improvedTrace_logoutput;
   @FXML
   public Pane improvedTrace_dmsPane;
   @FXML
   public Pane improvedTrace_hbwPane;
   @FXML
   public RadioButton improvedTrace_dmsRadioButton;
   @FXML
   public RadioButton improvedTrace_hbwRadioButton;

   //Performance Analyzer items
   @FXML
   private AreaChart performanceAnalyzer_throughputAreaChart;
   @FXML
   private NumberAxis performanceAnalyzer_yAxis;
   @FXML
   private CategoryAxis performanceAnalyzer_xAxis;
   @FXML
   public DatePicker performanceAnalyzer_date;
   @FXML
   public TextField performanceAnalyzer_startTime;
   @FXML
   public TextField performanceAnalyzer_endTime;
   @FXML
   public TextField performanceAnalyzer_location;
   @FXML
   public ComboBox performanceAnalyzer_telegramsList;
   @FXML
   public ComboBox performanceAnalyzer_locationsList;
   @FXML
   public TextArea performanceAnalyzer_outputTextArea;


   public Controller() {

   }

   //------------------------------------------------------------------------------------------------------------------

   /**
    * Main initialize method which is launched at the start of the app
    */
   @FXML
   private void initialize() {
      logger.info("Starting to initialize components and programs");
      LoadUnitExtractor = new LoadUnitExtractor();
      ButtonPaneMap = new HashMap<>();
      DMSLevelsTest = new DMSLevelsTest();
      FilesManager = new FilesManager();
      Validations = new Validations(FilesManager);
      initializeConfigurationsMenu();
      initializeInitialConfiguration();
      initializePerformanceAnalyzer();
      logger.info("All programs initialized");
      setInitialPanes();
      logger.info("Initial panes set successfully");
      setDescriptions();
      logger.info("Descriptions set");
      initializeMenuMap();
      logger.info("Menu map created");
   }

   //------------------------------------------------------------------------------------------------------------------
   private void initializeMenuMap() {
      ButtonPaneMap.put(loadUnitExtractorButton, loadUnitExtractorPane);
      ButtonPaneMap.put(loadUnitSenderButton, loadUnitSenderPane);
      ButtonPaneMap.put(loadUnitRetrieverButton, loadUnitRetrieverPane);
      ButtonPaneMap.put(storageOperationsButton, storageOperationsPane);
      ButtonPaneMap.put(improvedTraceButton, improvedTracePane);
      ButtonPaneMap.put(hostSimulatorButton, hostSimulatorPane);
      ButtonPaneMap.put(performanceAnalyzerButton, performanceAnalyzerPane);
      ButtonPaneMap.put(descriptionsButton, descriptionsPane);
   }

   private void initializeConfigurationsMenu() {
      //get all .properties files from "/configurations" directory`
      ObservableList<String> availableConfigs = FilesManager.getAvailableConfigurations();
      logger.trace("All configurations found - {}", availableConfigs);
      availableConfigs.forEach(config -> {
         //map each file as sub menu item
         String configName = config.substring(0, config.indexOf(".properties"));
         MenuItem menuItem = new MenuItem(configName);
         configurationsMenu.getItems().add(menuItem);
      });
   }

   private void initializeInitialConfiguration() {
      //find first available configuration
      MenuItem configuration = configurationsMenu.getItems()
              .stream()
              .filter(config -> !config.isDisable())
              .findFirst()
              .orElse(null);

      if (configuration != null) {
         String configurationName = configuration.getText();
         logger.trace("First available configuration selected - {}", configurationName);
         updateConfigValues(ResourceLoader.CONFIGURATIONS_PATH, configurationName);
         currentConfigLabel.setText("Current configuration - " + configurationName);
      } else {
         logger.trace("No available configurations could be found, using default one");
         updateConfigValues(ResourceLoader.DEFAULT_CONFIGURATION, null);
         currentConfigLabel.setText("Current configuration - default");
      }
   }

   @FXML
   @SuppressWarnings("unchecked")
   private void loadGeneralConfiguration(ActionEvent event) {
      Menu selectedItem = (Menu) event.getSource();
      selectedItem.getItems().forEach(selectedConfig -> {
         selectedConfig.setOnAction(event1 -> {
            //applies configuration depending on menu item selected
            updateConfigValues(ResourceLoader.CONFIGURATIONS_PATH, selectedConfig.getText());
            currentConfigLabel.setText("Current configuration - " + selectedConfig.getText());
            logger.trace("Configuration loaded - {}", selectedConfig.getText());
         });
      });
   }

   @FXML
   private void openConfigurationEditor() throws Exception {
      Stage newStage = new Stage();
      BorderPane borderPanePopup = new FXMLLoader(getClass().getClassLoader().getResource(ResourceLoader.CONFIGURATION_FXML_LAYOUT)).load();
      newStage.initStyle(StageStyle.UNDECORATED);
      Scene scene = new Scene(borderPanePopup);
      newStage.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream(ResourceLoader.DEMATIC_ICON)));
      newStage.setScene(scene);
      newStage.setMinWidth(600);
      newStage.setMinHeight(400);
      WindowHelper.addWindowHandler(newStage);
      scene.getRoot().requestFocus();
      newStage.showAndWait();
   }

   private void setGeneralConfigurationValues(String pathToConfigs, String value) {
      ConfigurationManager.setCurrentConfiguration(FilesManager.getGeneralConfigurationModel(pathToConfigs, value));

      loadUnitExtractor_LUprefix.setText(ConfigurationManager.getCurrentConfiguration().getLoadUnitExtractor_Prefix());
      loadUnitExtractor_LUbarcodeLength.setText(ConfigurationManager.getCurrentConfiguration().getLoadUnitExtractor_PrefixLength());

      loadUnitSender_restInput.setText(ConfigurationManager.getCurrentConfiguration().getCreateTransportRestCall());
      loadUnitSender_dbusername.setText(ConfigurationManager.getCurrentConfiguration().getUsernameDB());
      loadUnitSender_dbpassword.setText(ConfigurationManager.getCurrentConfiguration().getPasswordDB());
      loadUnitSender_dbhostname.setText(ConfigurationManager.getCurrentConfiguration().getHostnameDB());
      loadUnitSender_dbport.setText(ConfigurationManager.getCurrentConfiguration().getPortDB());
      loadUnitSender_dbsid.setText(ConfigurationManager.getCurrentConfiguration().getSidDB());
      loadUnitSender_sourcelocation.setText(ConfigurationManager.getCurrentConfiguration().getLoadUnitSender_SourceLocation());
      loadUnitSender_destinationlocation.setText(ConfigurationManager.getCurrentConfiguration().getLoadUnitSender_DestinationLocation());
      loadUnitSender_delay.setText(ConfigurationManager.getCurrentConfiguration().getLoadUnitSender_Delay());
      loadUnitSender_clarifyCheckbox.setSelected(ConfigurationManager.getCurrentConfiguration().isLoadUnitSender_Clarify());
      loadUnitSender_disregardClientCheckbox.setSelected(ConfigurationManager.getCurrentConfiguration().isLoadUnitSender_DisregardClient());

      loadUnitRetriever_restInput.setText(ConfigurationManager.getCurrentConfiguration().getCreateTransportRestCall());
      loadUnitRetriever_dbusername.setText(ConfigurationManager.getCurrentConfiguration().getUsernameDB());
      loadUnitRetriever_dbpassword.setText(ConfigurationManager.getCurrentConfiguration().getPasswordDB());
      loadUnitRetriever_dbhostname.setText(ConfigurationManager.getCurrentConfiguration().getHostnameDB());
      loadUnitRetriever_dbport.setText(ConfigurationManager.getCurrentConfiguration().getPortDB());
      loadUnitRetriever_dbsid.setText(ConfigurationManager.getCurrentConfiguration().getSidDB());
      loadUnitRetriever_retrievalDestination.setText(ConfigurationManager.getCurrentConfiguration().getLoadUnitRetriever_RetrievalDestination());
      loadUnitRetriever_loopDelay.setText(ConfigurationManager.getCurrentConfiguration().getLoadUnitRetriever_LoopDelay());
      loadUnitRetriever_loopCheckbox.setSelected(ConfigurationManager.getCurrentConfiguration().isLoadUnitRetriever_LoopEnabled());
      loadUnitRetriever_storedCheckbox.setSelected(ConfigurationManager.getCurrentConfiguration().isLoadUnitRetriever_StoredCheckEnabled());

      logger.debug("Configuration values set: {}", ConfigurationManager.getCurrentConfiguration());
   }

   public void updateConfigValues(String pathToConfigs, String value) {
      setGeneralConfigurationValues(pathToConfigs, value);
   }

   @FXML
   private void switchPanel(MouseEvent event) {
      Button clickedButton = (Button) event.getSource();

      for (Button button : ButtonPaneMap.keySet()) {
         if (clickedButton.equals(button)) {
            Pane openedPane = ButtonPaneMap.get(button);
            openedPane.setVisible(true);
            button.setStyle("-fx-background-color: #595959");
            logger.trace("Button clicked - {}", clickedButton);
         } else {
            ButtonPaneMap.get(button).setVisible(false);
            button.setStyle("-fx-background-color: #808080");
         }
      }
   }

   public void setFullscreen(Stage stage) {
      if (stage.isMaximized()) {
         maximizedImageView.setImage(maximizedIcon);
         stage.setMaximized(false);
      } else {
         maximizedImageView.setImage(notMaximizedIcon);
         stage.setMaximized(true);
      }
      logger.trace("Fullscreen - {}", stage.isMaximized());
   }

   //--------------------------------System UI BUTTONS START---------------------------------------
   @FXML
   private void closeApplication(ActionEvent event) {
      Node node = ((Node) event.getSource());
      Stage stage = ((Stage) node.getScene().getWindow());
      logger.info("Request to exit {} initiated", stage.getTitle());
      Platform.exit();
      logger.info("{} closed", stage.getTitle());
      System.exit(0);
   }

   @FXML
   private void minimizeApplication(ActionEvent event) {
      Node node = ((Node) event.getSource());
      Stage stage = ((Stage) node.getScene().getWindow());
      stage.setIconified(true);
      logger.trace("Application minimized");
   }

   @FXML
   private void setFullscreen(ActionEvent event) {
      Node node = ((Node) event.getSource());
      Stage stage = ((Stage) node.getScene().getWindow());
      setFullscreen(stage);
   }

   //--------------------------------System UI BUTTONS END---------------------------------------

   @FXML
   private void switchStorageTypePanes(ActionEvent event) {
      RadioButton radioButton = (RadioButton) event.getSource();
      if (radioButton.equals(improvedTrace_dmsRadioButton)) {
         improvedTrace_dmsPane.setVisible(true);
         improvedTrace_hbwPane.setVisible(false);
      } else {
         improvedTrace_hbwPane.setVisible(true);
         improvedTrace_dmsPane.setVisible(false);
      }
   }

   private void setDescriptions() {
      description_generalInfoDesc.setText(FilesManager.getDescription("GeneralInformation"));
      description_loadUnitExtractorDesc.setText(FilesManager.getDescription(InformationalCodes.PROGRAM_NAME_LOAD_UNIT_EXTRACTOR));
      description_loadUnitSenderDesc.setText(FilesManager.getDescription(InformationalCodes.PROGRAM_NAME_LOAD_UNIT_SENDER));
      description_loadUnitRetrieverDesc.setText(FilesManager.getDescription(InformationalCodes.PROGRAM_NAME_LOAD_UNIT_RETRIEVER));
      description_storageTestsDesc.setText(FilesManager.getDescription(InformationalCodes.PROGRAM_NAME_STORAGE_TESTS));
      description_improvedTraceDesc.setText(FilesManager.getDescription(InformationalCodes.PROGRAM_NAME_IMPROVED_MESSAGE_TRACE));
      description_hostSimulatorDesc.setText(FilesManager.getDescription(InformationalCodes.PROGRAM_NAME_HOST_SIMULATOR));
      description_performanceAnalyzerDesc.setText(FilesManager.getDescription(InformationalCodes.PROGRAM_NAME_PERFORMANCE_ANALYZER));
   }

   private void setInitialPanes() {
      loadUnitExtractorPane.setVisible(false);
      loadUnitSenderPane.setVisible(false);
      loadUnitRetrieverPane.setVisible(false);
      storageOperationsPane.setVisible(false);
      improvedTracePane.setVisible(false);
      hostSimulatorPane.setVisible(false);
      performanceAnalyzerPane.setVisible(false);

      //on startup load only descriptions
      descriptionsPane.setVisible(true);
      descriptionsButton.setStyle("-fx-background-color: \n" + "#595959");
   }

   //------------------------------------------------------------------------------------------------------------------

   @FXML
   private void loadUnitExtractor_ParseLoadUnits() {
      LoadUnitExtractor = new LoadUnitExtractor();
      LoadUnitExtractor.setOutputTextArea(loadUnitExtractor_outputField);
      LoadUnitExtractor.setCopiedToClipboardLabel(loadUnitExtractor_copiedLabel);
      LoadUnitExtractor.setInputText(loadUnitExtractor_inputField.getText());
      LoadUnitExtractor.setLoadUnitIdLength(loadUnitExtractor_LUbarcodeLength.getText());
      LoadUnitExtractor.setLoadUnitPrefix(loadUnitExtractor_LUprefix.getText());

      LoadUnitExtractor.startBackgroundThread();
   }

   @FXML
   private void loadUnitExtractor_ResetData() {
      loadUnitExtractor_inputField.clear();
      loadUnitExtractor_outputField.clear();
      loadUnitExtractor_copiedLabel.setVisible(false);
   }

   //------------------------------------------------------------------------------------------------------------------

   @FXML
   private void loadUnitSender_Start() {
      SqlManager databaseParameters = ConfigurationManager.getCurrentConfiguration().getDatabaseParameters();
      SqlQueries databaseConnector = new SqlQueries(databaseParameters);

      LoadUnitSender = new LoadUnitSender(databaseConnector);
      LoadUnitSender.setCreateTransportByCoordEndpoint(loadUnitSender_restInput.getText());
      LoadUnitSender.setSourceLocation(loadUnitSender_sourcelocation.getText());
      LoadUnitSender.setDestinationLocation(loadUnitSender_destinationlocation.getText());
      LoadUnitSender.setDelay(loadUnitSender_delay.getText());
      LoadUnitSender.setClient(loadUnitSender_clientList.getValue());
      LoadUnitSender.setOutputTextArea(loadUnitSender_logoutput);
      LoadUnitSender.setClarify(loadUnitSender_clarifyCheckbox.isSelected());
      LoadUnitSender.startBackgroundThread();
   }

   @FXML
   private void loadUnitSender_Stop() {

   }

   @FXML
   private void loadUnitSender_StopAll() {
      LoadUnitSender.stopBackgroundThread();
   }

   private boolean threadAlreadyRunning(ObservableMap<String, String> LoadUnitSenderTransportsMap, String value) {
      for (String sourceLocation : LoadUnitSenderTransportsMap.keySet()) {
         if (value.equals(sourceLocation)) {
            return true;
         }
      }
      return false;
   }

   //------------------------------------------------------------------------------------------------------------------

   @FXML
   private void loadUnitRetriever_Start() {
      loadUnitRetriever_loopOutput.clear();

      SqlManager databaseParameters = ConfigurationManager.getCurrentConfiguration().getDatabaseParameters();
      SqlQueries databaseConnector = new SqlQueries(databaseParameters);

      LoadUnitRetriever = new LoadUnitRetriever(databaseConnector);
      LoadUnitRetriever.setCreateTransportByCoordEndpoint(loadUnitRetriever_restInput.getText());
      LoadUnitRetriever.setDestinationLocation(loadUnitRetriever_retrievalDestination.getText());
      LoadUnitRetriever.setLoopDelay(loadUnitRetriever_loopDelay.getText());
      LoadUnitRetriever.setOutputTextArea(loadUnitRetriever_loopOutput);
      LoadUnitRetriever.setLoopEnabled(loadUnitRetriever_loopCheckbox.isSelected());
      LoadUnitRetriever.setStoredCheckEnabled(loadUnitRetriever_storedCheckbox.isSelected());
      LoadUnitRetriever.setInputTextArea(loadUnitRetriever_loadUnits);

      LoadUnitRetriever.startBackgroundThread();
   }

   @FXML
   private void loadUnitRetriever_Stop() {
      LoadUnitRetriever.stopBackgroundThread();
   }
   //------------------------------------------------------------------------------------------------------------------

   private void dmsLevelsTest_generateValuesForAisles(String initialValue) {
      if (initialValue.isEmpty()) {
         initialValue = "0";
      }
      int value = Integer.parseInt(initialValue);
      ObservableList<Node> checkboxList = dmsLevelsTest_aislesGrid.getChildren();
      for (Node node : checkboxList) {
         if (node instanceof CheckBox) {
            ((CheckBox) node).setText(String.valueOf(value));
            value++;
         }
      }
   }

   @FXML
   private void dmsLevelsTest_Start() {
      dmsLevelsTest_logoutput.clear();
      if (threadAlreadyRunning(DMSLevelsTestTransportsMap, dmsLevelsTest_workstation.getText())) {
         dmsLevelsTest_logoutput.appendText("DMS Levels Test is already active on " + dmsLevelsTest_workstation.getText() + '\n');
         return;
      }
      if (!Validations.dmsLevelsTestValidateParameters(
              dmsLevelsTest_logoutput,
              dmsLevelsTest_restInput,
              dmsLevelsTest_numberOfAisles,
              dmsLevelsTest_levelsPerAisle,
              dmsLevelsTest_loadUnitsPerLevel,
              dmsLevelsTest_aislePrefix,
              dmsLevelsTest_workstation,
              dmsLevelsTest_facility,
              dmsLevelsTest_delay,
              dmsLevelsTest_aislesGrid)) return;
      if (!Validations.validateDatabaseConnection(
              dmsLevelsTest_logoutput,
              dmsLevelsTest_dbusername,
              dmsLevelsTest_dbpassword,
              dmsLevelsTest_dbhostname,
              dmsLevelsTest_dbport,
              dmsLevelsTest_dbsid)) return;
      DMSLevelsTestRunningThreadsMap.put(dmsLevelsTest_workstation.getText(), new AtomicBoolean(false));
      DMSLevelsTestStoppedThreadsMap.put(dmsLevelsTest_workstation.getText(), new AtomicBoolean(true));
      Thread dmsLevelsTestThread = initializeDMSLevelsTestThread();
      DMSLevelsTestThreadsMap.put(dmsLevelsTest_workstation.getText(), dmsLevelsTestThread);
      dmsLevelsTestThread.start();
      DMSLevelsTestTransportsMap.put(dmsLevelsTest_workstation.getText(), String.format("%s", dmsLevelsTest_workstation.getText()));
   }

   @FXML
   private void dmsLevelsTest_StopAll() {
      for (String thread : DMSLevelsTestRunningThreadsMap.keySet()) {
         DMSLevelsTestRunningThreadsMap.get(thread).set(false);
         DMSLevelsTestTransportsMap.remove(thread);
      }
   }

   private Thread initializeDMSLevelsTestThread() {
      return new Thread(() -> {
         String sourceLocation = dmsLevelsTest_workstation.getText();

         SqlManager = new SqlManager(
                 dmsLevelsTest_dbhostname.getText(),
                 dmsLevelsTest_dbport.getText(),
                 dmsLevelsTest_dbsid.getText(),
                 dmsLevelsTest_dbusername.getText(),
                 dmsLevelsTest_dbpassword.getText()
         );
         DMSLevelsTest = new DMSLevelsTest(
                 SqlManager,
                 dmsLevelsTest_restInput.getText(),
                 dmsLevelsTest_numberOfAisles.getText(),
                 dmsLevelsTest_levelsPerAisle.getText(),
                 dmsLevelsTest_delay.getText(),
                 dmsLevelsTest_loadUnitsPerLevel.getText(),
                 sourceLocation,
                 dmsLevelsTest_facility.getText()
         );
         ArrayList<String> Aisles = DMSLevelsTest.retrieveSelectedAisles(dmsLevelsTest_aislesGrid);
         ArrayList<String> Transports = DMSLevelsTest.generateTransports(Aisles);

         DMSLevelsTestRunningThreadsMap.put(sourceLocation, new AtomicBoolean(true));
         DMSLevelsTestStoppedThreadsMap.put(sourceLocation, new AtomicBoolean(false));
         int locationIndex = 0;
         while (DMSLevelsTestRunningThreadsMap.get(sourceLocation).get()) {
            try {
               if (locationIndex < Transports.size()) {
                  for (int i = 0; i < Integer.parseInt(dmsLevelsTest_loadUnitsPerLevel.getText()); i++) {
                     DMSLevelsTest.sendLoadUnits(locationIndex, Transports, dmsLevelsTest_logoutput);
                  }
               } else {
                  DMSLevelsTestRunningThreadsMap.get(sourceLocation).set(false);
                  break;
               }
            } catch (InterruptedException e) {
               DMSLevelsTestThreadsMap.get(sourceLocation).interrupt();
            }
            locationIndex++;
         }
         DMSLevelsTestStoppedThreadsMap.get(sourceLocation).set(true);
         DMSLevelsTestTransportsMap.remove(sourceLocation);
      });
   }

   public static AtomicBoolean getDMSLevelsTestThreadRunning(String sourceLocation) {
      return DMSLevelsTestRunningThreadsMap.get(sourceLocation);
   }

   //------------------------------------------------------------------------------------------------------------------

   @FXML
   private void shuffleDMS_Start() {
      if (shuffleDMSThread.isAlive()) {
         shuffleDMS_outputArea.appendText("Shuffle DMS program is already active\n");
         return;
      }
      if (!Validations.shuffleDMS_ValidateParameters(
              shuffleDMS_outputArea,
              shuffleDMS_loadUnitsCount,
              dmsLevelsTest_restInput,
              dmsLevelsTest_facility,
              dmsLevelsTest_numberOfAisles,
              dmsLevelsTest_levelsPerAisle,
              dmsLevelsTest_aislePrefix)) return;
      if (!Validations.validateDatabaseConnection(
              shuffleDMS_outputArea,
              dmsLevelsTest_dbusername,
              dmsLevelsTest_dbpassword,
              dmsLevelsTest_dbhostname,
              dmsLevelsTest_dbport,
              dmsLevelsTest_dbsid)) return;
      shuffleDMS_outputArea.clear();
      shuffleDMSThread = initializeShuffleDMSThread();
      shuffleDMSThread.start();
   }

   @FXML
   private void shuffleDMS_Abort() {
      shuffleDMSRunning.set(false);
   }

   private Thread initializeShuffleDMSThread() {
      return new Thread(() -> {
         SqlQueries = new SqlQueries(
                 dmsLevelsTest_dbhostname.getText(),
                 dmsLevelsTest_dbport.getText(),
                 dmsLevelsTest_dbsid.getText(),
                 dmsLevelsTest_dbusername.getText(),
                 dmsLevelsTest_dbpassword.getText()
         );
         ShuffleDMS = new ShuffleDMS(SqlQueries,
                 dmsLevelsTest_restInput.getText(),
                 Integer.parseInt(shuffleDMS_loadUnitsCount.getText()),
                 dmsLevelsTest_facility.getText(),
                 Integer.parseInt(dmsLevelsTest_numberOfAisles.getText()),
                 Integer.parseInt(dmsLevelsTest_aislePrefix.getText()),
                 Integer.parseInt(dmsLevelsTest_levelsPerAisle.getText())
         );
         shuffleDMSRunning.set(true);
         FilesManager.printToOutputTextAreaAndFile("Shuffling started", shuffleDMS_outputArea, ShuffleDMS.getClass().getSimpleName(), ShuffleDMS.getLogFileName());
         for (String loadUnit : ShuffleDMS.getLoadUnits()) {
            if (shuffleDMSRunning.get()) {
               ShuffleDMS.sendLoadUnitToDifferentLocation(loadUnit, ShuffleDMS.returnRandomDMSVirtualLocation());
            } else {
               FilesManager.printToOutputTextAreaAndFile("Shuffling aborted", shuffleDMS_outputArea, ShuffleDMS.getClass().getSimpleName(), ShuffleDMS.getLogFileName());
               break;
            }
         }
         FilesManager.printToOutputTextAreaAndFile("Shuffling finished", shuffleDMS_outputArea, ShuffleDMS.getClass().getSimpleName(), ShuffleDMS.getLogFileName());
      });
   }
   //------------------------------------------------------------------------------------------------------------------

   @FXML
   private void improvedTrace_addMessageToExceptions() {
      String selectedLoadUnit = improvedTrace_loadUnitsList.getSelectionModel().getSelectedItem();
      int selectedMessageIndex = improvedTrace_messagesList.getSelectionModel().getSelectedIndex();
      FilesManager.addLocationException(
              improvedTraceConfigName, MessageChecker.retrieveMessageByIndex(
                      selectedLoadUnit, selectedMessageIndex));
      improvedTrace_refreshLoadUnits();
   }

   @FXML
   private void improvedTrace_retrieveLoadUnits() {
      improvedTrace_logoutput.clear();
      if (!improvedTraceThread.isAlive()) {
         if (!Validations.improvedTrace_ValidateParameters(
                 improvedTrace_logoutput,
                 improvedTrace_pathToExcel,
                 improvedTrace_retrievalStatus,
                 improvedTrace_numberOfAisles,
                 improvedTrace_firstAisleNumber,
                 improvedTrace_MaximumY,
                 improvedTrace_MaximumX,
                 improvedTrace_binDepth,
                 improvedTrace_timestamp)) return;
         if (!Validations.validateDatabaseConnection(
                 improvedTrace_logoutput,
                 improvedTrace_dbusername,
                 improvedTrace_dbpassword,
                 improvedTrace_dbhostname,
                 improvedTrace_dbport,
                 improvedTrace_dbsid)) return;
         improvedTrace_reset();
         improvedTrace_progressBar.setProgress(0);
         improvedTraceThread = improvedTrace_initializeThread();
         improvedTrace_retrievalStatus.setText("Retrieving messages");
         improvedTraceThread.start();
      } else {
         System.out.println("thread is still running");
      }
   }

   @FXML
   private void improvedTrace_refreshLoadUnits() {
      if (!improvedTraceThread.isAlive()) {
         if (MessageChecker != null) {
            improvedTrace_loadAndDisplayLoadUnits();
         } else {
            improvedTrace_retrievalStatus.setText("No messages retrieved yet");
         }
      } else {
         System.out.println("thread is still running");
      }
   }

   private void improvedTrace_loadAndDisplayLoadUnits() {
      improvedTrace_reset();
      ObservableList<LoadUnit> loadUnits = FXCollections.observableArrayList(
              MessageChecker.retrieveLoadUnitsWithAppliedFilters(
                      totalLoadUnits,
                      improvedTrace_onlyInvalidLUCheckbox.isSelected(),
                      improvedTrace_locationPassed.getText(),
                      improvedTrace_barcode.getText()
              )
      );
      System.out.println("Total load units:" + totalLoadUnits.size());
      System.out.println("Retrieved: " + loadUnits.size());
      Platform.runLater(() -> {
         improvedTrace_loadUnitsList.setItems(MessageChecker.getLoadUnitsAsStrings(loadUnits));
         improvedTrace_retrievalStatus.setText("Found " + loadUnits.size() + " load units");
      });
      improvedTrace_loadUnitsList.setCellFactory(param -> new ListCell<>() {

         @Override
         protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty || item == null) {
               setText(null);
               setStyle(null);
            } else {
               //TODO fix IndexOutOfBoundsException
               setText(improvedTrace_loadUnitsList.getItems().get(getIndex()));
               if (!loadUnits.get(getIndex()).getContainsOnlyValidMessages()) {
                  setStyle("-fx-text-fill: red;");
               } else {
                  setStyle("-fx-text-fill: green;");
               }
            }
         }
      });

      improvedTrace_messagesList.setCellFactory(param -> new ListCell<>() {
         @Override
         protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty || item == null) {
               setText(null);
               setStyle(null);
            } else {
               setText(improvedTrace_messagesList.getItems().get(getIndex()));
               int index = improvedTrace_loadUnitsList.getSelectionModel().getSelectedIndex();
               if (loadUnits.get(index).getMessages().getValue(getIndex())) {
                  setStyle("-fx-text-fill: green;");
               } else {
                  setStyle("-fx-text-fill: red;");
               }
            }
         }
      });
   }

   private void improvedTrace_reset() {
      improvedTrace_loadUnitsList.getItems().clear();
      improvedTrace_messagesList.getItems().clear();
      improvedTrace_messageNote.clear();
      improvedTrace_comment.clear();
   }

   private Thread improvedTrace_initializeThread() {
      return new Thread(() -> {
         improvedTraceThreadRunning.set(true);
         improvedTracelogFileName = FilesManager.generateFileName("improvedTrace");
         System.out.println("Improved message trace thread started");
         venckus.gytis.programs.improvedmessagetrace.control.MessageChecker.setTimestamp(Integer.parseInt(improvedTrace_timestamp.getText()));
         DMSManager.setDMSValues(
                 Integer.parseInt(improvedTrace_numberOfAisles.getText()),
                 Integer.parseInt(improvedTrace_MaximumX.getText()),
                 Integer.parseInt(improvedTrace_MaximumY.getText()),
                 Integer.parseInt(improvedTrace_binDepth.getText()),
                 Integer.parseInt(improvedTrace_firstAisleNumber.getText())
         );
         improvedTraceMessagesManagerSql = new MessagesManagerSql(
                 improvedTrace_dbhostname.getText(),
                 improvedTrace_dbport.getText(),
                 improvedTrace_dbsid.getText(),
                 improvedTrace_dbusername.getText(),
                 improvedTrace_dbpassword.getText()
         );
         improvedTracePathToExcel = improvedTrace_pathToExcel.getText();
         MessageChecker = new MessageChecker(improvedTraceConfigName);
         totalLoadUnits = MessageChecker.getRetrievedLoadUnits();
         improvedTrace_loadAndDisplayLoadUnits();
         improvedTraceThreadRunning.set(false);
         System.out.println("Improved message trace thread stopped");
         improvedTrace_listenerThread().start();
      });
   }

   private Thread improvedTrace_listenerThread() {
      return new Thread(() -> {
         improvedTrace_loadUnitsList.getSelectionModel().selectedItemProperty().addListener(
                 (observable, oldValue, newValue) ->
                         improvedTrace_messagesList.setItems(
                                 FXCollections.observableArrayList(
                                         MessageChecker.getDisplayMessagesByLoadUnit(
                                                 newValue).keySet()))
         );
         improvedTrace_messagesList.getSelectionModel().selectedItemProperty().addListener(
                 (observable2, oldValue2, newValue2) -> {
                    if (!improvedTrace_messagesList.getSelectionModel().isEmpty()) {
                       improvedTrace_addMessageToExceptionsButton.setDisable(false);
                       int index = improvedTrace_messagesList.getSelectionModel().getSelectedIndex();
                       if (index >= 0) {
                          improvedTrace_messageNote.setText(
                                  MessageChecker.retrieveMessageNote(
                                          improvedTrace_loadUnitsList.getSelectionModel().getSelectedItem(), index));
                          improvedTrace_comment.setText(
                                  MessageChecker.retrieveComment(
                                          improvedTrace_loadUnitsList.getSelectionModel().getSelectedItem(), index));
                       }
                    } else {
                       improvedTrace_addMessageToExceptionsButton.setDisable(true);
                    }
                 });
      });
   }

   @FXML
   private void performanceAnalyzer_addNewLocation() {
      if (performanceAnalyzer_location.getText().isEmpty()) {
         return;
      }
      String location = String.format("%s - %s", performanceAnalyzer_location.getText(),
              performanceAnalyzer_telegramsList.getSelectionModel().getSelectedItem());

      if (performanceAnalyzerLocations.contains(location)) {
         logger.trace("Location - {} already exists in list - {}", location, performanceAnalyzerLocations);
         return;
      }
      logger.trace("Adding new location - {}", location);
      performanceAnalyzerLocations.add(location);

      //update list
      performanceAnalyzerUpdateLocations();
   }

   @FXML
   private void performanceAnalyzer_removeLocation() {
      if (performanceAnalyzer_locationsList.getSelectionModel().getSelectedItem() == null) {
         logger.trace("Item does not exist");
         return;
      }

      String location = performanceAnalyzer_locationsList.getSelectionModel().getSelectedItem().toString();
      logger.trace("Removing location - {}", location);
      performanceAnalyzerLocations.remove(location);
      //update list
      performanceAnalyzerUpdateLocations();
   }

   @SuppressWarnings("unchecked")
   private void performanceAnalyzerUpdateLocations() {
      logger.debug("Updating locations list - {}", performanceAnalyzerLocations);
      performanceAnalyzer_locationsList.setItems(performanceAnalyzerLocations);
      //to avoid null pointers
      if (!performanceAnalyzerLocations.isEmpty()) {
         performanceAnalyzer_locationsList.setValue(performanceAnalyzerLocations.get(0));
      }
   }

   @SuppressWarnings("unchecked")
   private void initializePerformanceAnalyzer() {
      //Setting initial parameters
      performanceAnalyzer_yAxis.setLabel("Load units per hour");
      performanceAnalyzer_xAxis.setLabel("Time");
      performanceAnalyzer_throughputAreaChart.setTitle("THP Monitor");

      //Setting telegram list
      ObservableList<Telegram> telegramObservableList = FXCollections.observableArrayList(Telegram.getValuesAsArraylist());
      performanceAnalyzer_telegramsList.setItems(telegramObservableList);
      performanceAnalyzer_telegramsList.setValue(telegramObservableList.get(0));

      //Setting locations
      performanceAnalyzerLocations = FXCollections.observableArrayList();
   }

   @FXML
   private void performanceAnalyzer_displayThroughputData() {
      //create throughput monitor object and set values
      ThroughputMonitor = new ThroughputMonitor();
      ThroughputMonitor.setDate(performanceAnalyzer_date.getValue());
      ThroughputMonitor.setStartTime(performanceAnalyzer_startTime.getText());
      ThroughputMonitor.setEndTime(performanceAnalyzer_endTime.getText());
      ThroughputMonitor.setLocationsTelegramsList(performanceAnalyzerLocations);
      ThroughputMonitor.setAreaChart(performanceAnalyzer_throughputAreaChart);
      ThroughputMonitor.setOutputTextArea(performanceAnalyzer_outputTextArea);
      ThroughputMonitor.setDatabaseParameters(ConfigurationManager.getCurrentConfiguration().getDatabaseParameters());

      //create data for chart
      ThroughputMonitor.startBackgroundThread();
   }

   @FXML
   private void performanceAnalyzer_stop() {
      ThroughputMonitor.completelyStop();
   }
}
