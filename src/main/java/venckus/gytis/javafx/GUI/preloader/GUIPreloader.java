package venckus.gytis.javafx.GUI.preloader;

import javafx.application.Preloader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.javafx.UI.ResourceLoader;

public class GUIPreloader extends Preloader {

   private Logger logger = LoggerFactory.getLogger(GUIPreloader.class);

   private Stage preloaderStage;
   private Scene scene;

   public GUIPreloader() {

   }

   @Override
   public void init() throws Exception {
      FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(ResourceLoader.PRELOADER_FXML_LAYOUT));
      Parent root = loader.load();
      scene = new Scene(root);
      logger.info("Preloader initialization done");

   }

   @Override
   public void start(Stage primaryStage) throws Exception {
      this.preloaderStage = primaryStage;
      primaryStage.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream(ResourceLoader.DEMATIC_ICON)));
      preloaderStage.setScene(scene);
      preloaderStage.initStyle(StageStyle.UNDECORATED);
      preloaderStage.show();
      logger.info("Preloader started");
   }

   @Override
   public void handleApplicationNotification(Preloader.PreloaderNotification info) {
      if (info instanceof ProgressNotification) {
         PreloaderController.statProgressBar.setProgress(((ProgressNotification) info).getProgress());
      }
   }

   @Override
   public void handleStateChangeNotification(Preloader.StateChangeNotification info) {
      StateChangeNotification.Type type = info.getType();
      switch (type) {
         case BEFORE_START:
            logger.info("Preloader finished");
            preloaderStage.hide();
            break;
      }
   }
}
