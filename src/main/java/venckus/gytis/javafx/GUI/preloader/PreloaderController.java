package venckus.gytis.javafx.GUI.preloader;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;

import java.net.URL;
import java.util.ResourceBundle;

public class PreloaderController implements Initializable {

   @FXML
   private ProgressBar loadingProgressBar;

   public static ProgressBar statProgressBar;

   @Override
   public void initialize(URL url, ResourceBundle rb) {
      statProgressBar = loadingProgressBar;
   }
}
