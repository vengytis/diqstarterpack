package venckus.gytis.javafx.GUI.configuration;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.general.ConfigurationManager;
import venckus.gytis.general.FilesManager;
import venckus.gytis.javafx.Main;
import venckus.gytis.javafx.UI.ResourceLoader;
import venckus.gytis.model.GeneralConfigurationModel;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ConfigurationController {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   @FXML
   private Label ConfigurationName;

   @FXML
   private VBox ParametersContainerVBox;

   private LinkedHashMap<String, String> ConfigurationMap;

   @FXML
   private void initialize() {
      generateConfiguration();
   }

   @FXML
   private void closeWindow(ActionEvent event) {
      ((Node)(event.getSource())).getScene().getWindow().hide();
   }

   @SuppressWarnings("unchecked")
   private void generateConfiguration() {
      logger.trace("Generating configuration for config editor");
      GeneralConfigurationModel CurrentConfiguration = ConfigurationManager.getCurrentConfiguration();
      logger.debug("Current configuration for configuration editor generation - {}", CurrentConfiguration);
      FilesManager filesManager = new FilesManager();
      ConfigurationName.setText(CurrentConfiguration.getConfigurationName());
      ConfigurationMap = filesManager.getPropertiesMapFromPropertiesFile(CurrentConfiguration.getConfigurationName());
      for (HashMap.Entry<String, String> entry : ConfigurationMap.entrySet()) {
         GridPane gridPane = new GridPane();
         Label label = new Label();
         TextField textField = new TextField();
         label.setText(entry.getKey());
         label.setPadding(new Insets(0,0,0,10));
         textField.setText(entry.getValue());
         textField.setAlignment(Pos.CENTER);
         gridPane.add(label, 0, 0);
         gridPane.add(textField, 1, 0);
         ColumnConstraints col1 = new ColumnConstraints();
         col1.setPercentWidth(40);
         ColumnConstraints col2 = new ColumnConstraints();
         col2.setPercentWidth(60);
         gridPane.getColumnConstraints().addAll(col1, col2);
         gridPane.setVgap(10);
         gridPane.setGridLinesVisible(true);
         gridPane.setPrefWidth(ParametersContainerVBox.getPrefWidth());
         gridPane.setMaxWidth(ParametersContainerVBox.getMaxWidth());
         ParametersContainerVBox.getChildren().add(gridPane);
      }
      System.out.println();
   }

   @FXML
   private void saveConfiguration() {
      logger.trace("Saving configuration");
      LinkedHashMap<String, String> updatedConfiguration = deconstructToMap();
      ConfigurationManager.updateCurrentConfiguration(ConfigurationName.getText(), updatedConfiguration);
      ConfigurationMap = updatedConfiguration;

      //update GUI by accessing Controller
      logger.trace("Updating GUI with new values");
      Main.controllerHandler.updateConfigValues(ResourceLoader.CONFIGURATIONS_PATH, ConfigurationName.getText());
   }

   private LinkedHashMap<String, String> deconstructToMap() {
      logger.trace("Deconstructing GUI elements to map");
      LinkedHashMap<String, String> configuration = new LinkedHashMap<>();
      ParametersContainerVBox.getChildren().forEach(gridPane -> {
         String configKey = null;
         String configValue = null;
         if (gridPane instanceof GridPane) {
            Node firstNode = ((GridPane) gridPane).getChildren().get(0);
            Node secondNode = ((GridPane) gridPane).getChildren().get(1);
            if (firstNode instanceof Label) {
               configKey = ((Label) firstNode).getText();
            }
            if (secondNode instanceof TextField) {
               configValue = ((TextField) secondNode).getText();
            }
            configuration.put(configKey, configValue);
         }
      });
      logger.trace("Map from deconstructed GUI elements - {}", configuration);
      return configuration;
   }
}
