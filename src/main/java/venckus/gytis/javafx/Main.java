package venckus.gytis.javafx;

import javafx.application.Application;
import javafx.application.Preloader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.javafx.GUI.mainGUI.Controller;
import venckus.gytis.javafx.GUI.mainGUI.WindowHelper;
import venckus.gytis.javafx.GUI.preloader.GUIPreloader;
import venckus.gytis.javafx.UI.ResourceLoader;


public class Main extends Application {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private static boolean LoadPreloader = false;

   private String Title = "DIQ Starter Pack";

   public static Controller controllerHandler;

   @Override
   public void start(Stage primaryStage) throws Exception {
      logger.info("{} started", Title);
      FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(ResourceLoader.MAIN_STAGE_FXML_LAYOUT));
      Parent root = loader.load();
      controllerHandler = loader.getController();
      primaryStage.initStyle(StageStyle.UNDECORATED);
      primaryStage.setTitle(Title);
      primaryStage.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream(ResourceLoader.DEMATIC_ICON)));
      Scene scene = new Scene(root, 1152, 720);
      primaryStage.setScene(scene);
      primaryStage.setMinWidth(880);
      primaryStage.setMinHeight(590);
      primaryStage.setResizable(true);
      primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
      primaryStage.setFullScreen(false);
      primaryStage.show();
      WindowHelper.addWindowHandler(primaryStage);
      logger.info("{} loaded", Title);
   }

   @Override
   public void init() throws Exception {
      if (!LoadPreloader) {
         return;
      }
      // Perform some heavy lifting (i.e. database start, check for application updates, etc. )
      for (int i = 1; i <= 100; i++) {
         double progress =(double) i/100;
         notifyPreloader(new Preloader.ProgressNotification(progress));
         Thread.sleep(20);
      }
      Thread.sleep(400);
      logger.info("Loading initialization done");
   }

   public static void main(String[] args) {
      if (LoadPreloader) {
         System.setProperty("javafx.preloader", GUIPreloader.class.getCanonicalName());
      }
      launch(args);
   }
}
