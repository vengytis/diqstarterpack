package venckus.gytis.javafx.UI;

public class ResourceLoader {

   //System UI resources
   public static final String SYSTEM_UI_MAXIMIZE_ICON = "javafx/icons/maximize.png";
   public static final String SYSTEM_UI_NOT_MAXIMIZE_ICON = "javafx/icons/maximize2.png";

   //Preloader resources
   public static final String PRELOADER_FXML_LAYOUT = "javafx/LoadingScreen.fxml";

   //Main stage resources
   public static final String MAIN_STAGE_FXML_LAYOUT = "javafx/DIQStarterPackReloaded.fxml";
   public static final String CONFIGURATIONS_PATH = "configurations/";
   public static final String DEFAULT_CONFIGURATION = "javafx/configurations/default.properties";

   //Configuration stage resources
   public static final String CONFIGURATION_FXML_LAYOUT = "javafx/DIQConfigurations.fxml";

   //Shared resources
   public static final String DEMATIC_ICON = "javafx/dematicLogo.png";

}
