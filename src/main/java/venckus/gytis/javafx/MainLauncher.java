package venckus.gytis.javafx;

/**
 * Workaround to fix packaging problem with maven for javafx application provided by user "chowmein"
 * https://stackoverflow.com/questions/57019143/build-executable-jar-with-javafx11-from-maven
 */
public class MainLauncher {
   public static void main(String[] args) {
      Main.main(args);
   }
}
