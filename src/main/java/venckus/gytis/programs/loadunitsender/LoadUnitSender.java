package venckus.gytis.programs.loadunitsender;

import javafx.scene.control.TextArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.database.SqlQueries;
import venckus.gytis.general.FilesManager;
import venckus.gytis.programs.Program;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.concurrent.TimeUnit;

public class LoadUnitSender implements Program {

   protected Logger logger = LoggerFactory.getLogger(this.getClass());

   protected FilesManager filesManager;

   protected TextArea OutputTextArea;

   private Thread LoadUnitSenderThread;

   private boolean Clarify;

   protected SqlQueries SQLQueries;

   private long Delay;

   protected String CreateTransportRestCall;

   private String SourceLocation;

   protected String DestinationLocation;

   private String Client;

   protected boolean SignalToStopThreadReceived;

   protected boolean ThreadRunning;

   public LoadUnitSender(SqlQueries sqlQueries) {
      //Set initial values for thread status
      this.SQLQueries = sqlQueries;
      filesManager = new FilesManager();
      SignalToStopThreadReceived = false;
      ThreadRunning = false;
   }

   protected boolean sendToDestination() {
      //get load unit on location
      String loadUnit = getLoadUnitAtLocation();
      if (loadUnit == null) {
         return false;
      }
      logger.debug("Load unit - {} received on location - {}", loadUnit, SourceLocation);
      //since rest call asks for specific destination format - create it
      String detailedDestination = getDetailedDestination(DestinationLocation);
      if (detailedDestination == null) {
         //null is expected when detailed destination is not found
         return false;
      }
      logger.debug("Destination found - {}", detailedDestination);
      //clarify
      logger.debug("Clarify load unit - {}", Clarify);
      if (Clarify) {
         if (clarify(loadUnit)) {
            logger.trace("Load unit - {} clarified", loadUnit);
            filesManager.printToOutputTextArea(String.format("Load unit - %s clarified", loadUnit), OutputTextArea);
         } else {
            //TODO might be smart to put some kind of "maximum attempts logic" here
            logger.trace("Load unit - {} could not be clarified", loadUnit);
            filesManager.printToOutputTextArea(String.format("Load unit - %s could not be clarified", loadUnit), OutputTextArea);
         }
      }
      //false is returned here only if interruption was received (stop signal etc.)
      return sendLoadUnit(loadUnit, detailedDestination);
   }

   /**
    * @return loadunit id or null
    */
   private String getLoadUnitAtLocation() {
      String loadUnit;
      try {
         //try to retrieve load unit on location
         loadUnit = SQLQueries.getFirstLoadUnitInLocation(SourceLocation);
      } catch (SQLException e) {
         logger.warn("SQL message - {}, SQL state - {}", e.getMessage(), e.getSQLState(), e);
         filesManager.printToOutputTextArea("Database connection interrupted", OutputTextArea);
         return null;
      }

      if (loadUnit == null) {
         //load unit was not found
         logger.debug("Load unit with status - {} not found at location - {}", "NO_TO", SourceLocation);
         filesManager.printToOutputTextArea(String.format("Load unit with status - %s not found at location - %s",
                 "NO_TO", SourceLocation), OutputTextArea);
      } else {
         //load unit found
         logger.debug("Load unit - {} found at location - {}", loadUnit, SourceLocation);
         filesManager.printToOutputTextArea(String.format("Load unit - %s found at location - %s", loadUnit, SourceLocation), OutputTextArea);
      }
      return loadUnit;
   }

   protected boolean sendLoadUnit(String loadUnit, String detailedDestination) {
      HttpURLConnection connection;
      try {
         URL restApiEndPoint = new URL(CreateTransportRestCall);
         connection = (HttpURLConnection) restApiEndPoint.openConnection();
         connection.setDoOutput(true);
         connection.setInstanceFollowRedirects(false);
         connection.setRequestMethod("POST");
         connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
         connection.setRequestProperty("charset", "utf-8");
         connection.setUseCaches(false);
         StringBuilder restCallBody = new StringBuilder()
                 .append("loadUnitId=").append(loadUnit).append("&")
                 .append("destinationLocationId=").append(detailedDestination).append("&")
                 .append("type=RELOCATION");
         byte[] postData = restCallBody.toString().getBytes(StandardCharsets.UTF_8);
         try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
            wr.write(postData);
         }
         if (String.valueOf(connection.getResponseCode()).startsWith("2")) {
            String outputText = String.format("Load unit - %s sent to - %s", loadUnit, detailedDestination);
            filesManager.printToOutputTextArea(outputText, OutputTextArea);
            logger.trace(outputText);
            connection.disconnect();
            return true;
         } else {
            String outputText = String.format("Unable to send load unit - %s to - %s", loadUnit, detailedDestination);
            filesManager.printToOutputTextArea(outputText, OutputTextArea);
            logger.trace(outputText);
         }
      } catch (Exception e) {
         //in case of any exception
         filesManager.printToOutputTextArea("Rest api connection failed", OutputTextArea);
         logger.error(e.getMessage(), e);
         return false;
      }
      return false;
   }

   protected String getDetailedDestination(String destinationLocation) {
      String detailedDestination = null;
      if (destinationLocation.matches("(.*?)\\.(.*?)\\.\\w+\\.\\w+\\.\\w+")) {
         //detailed destination was passed already, no need to lookup in DB
         logger.trace("Detailed destination was passed as parameter, no need to look up - {}", destinationLocation);
         return destinationLocation;
      }
      try {
         logger.trace("Destination is not detailed - {}, need to look up in DB", destinationLocation);
         detailedDestination = SQLQueries.getDetailedLocation(destinationLocation);
         logger.trace("Detailed destination found - {}", detailedDestination);
      } catch (SQLException e) {
         logger.trace("Destination does not exist - {}", destinationLocation);
         filesManager.printToOutputTextArea(String.format("Destination - %s does not exist", destinationLocation), OutputTextArea);
      }
      return detailedDestination;
   }

   private boolean clarify(String loadUnit) {
      try {
         SQLQueries.clarifyLoadUnit(loadUnit);
      } catch (SQLException e) {
         logger.error("SQL message - {}, SQL state - {}", e.getMessage(), e.getSQLState(), e);
         return false;
      } finally {
         //regardless if clarification failed or not - close connection with DB
         SQLQueries.disconnect();
      }
      return true;
   }

   @Override
   public boolean validateFields() {
      logger.debug("Validating database connection with provided parameters - {}", SQLQueries);
      boolean dbValidation = ValidationObj.validateDatabaseConnection(OutputTextArea, SQLQueries.getUsername(),
              SQLQueries.getPassword(), SQLQueries.getHostname(), SQLQueries.getPort(), SQLQueries.getSID());
      if (!dbValidation) {
         logger.trace("Database connection failed");
         filesManager.printToOutputTextArea("Database connection failed", OutputTextArea);
         return false;
      }
      logger.trace("Database connection successful");
      logger.debug("Validating provided program parameters - {}", this);
      boolean parametersValidation = ValidationObj.loadUnitSenderValidateParameters(OutputTextArea,
              CreateTransportRestCall, SourceLocation, DestinationLocation, String.valueOf(Delay));
      if (!parametersValidation) {
         logger.trace("Program parameters validation failed");
         filesManager.printToOutputTextArea("Program parameters validation failed", OutputTextArea);
         return false;
      }
      logger.trace("Program parameters validated successfully");
      return true;
   }

   @Override
   public void initializeBackgroundThread() {
      logger.trace("Initializing new thread");
      LoadUnitSenderThread = new Thread(() -> {
         if (!validateFields()) {
            stopBackgroundThread();
            return;
         }
         logger.trace("Validation successful");
         ThreadRunning = true;
         while (ThreadRunning) {
            sendToDestination();
            try {
               TimeUnit.MILLISECONDS.sleep(Delay);
            } catch (InterruptedException e) {
               //if stop signal was received from user
               stopBackgroundThread();
            }
         }
         stopBackgroundThread();
      });
      logger.debug("Thread initialized - {}", LoadUnitSenderThread);
   }

   @Override
   public void startBackgroundThread() {
      OutputTextArea.clear();
      filesManager.printToOutputTextArea("Starting Load Unit Sender", OutputTextArea);
      logger.info("Starting Load Unit Sender");
      initializeBackgroundThread();
      LoadUnitSenderThread.start();
   }

   @Override
   public void pauseBackgroundThread() {
      //TODO implement pause logic
   }

   @Override
   public void stopBackgroundThread() {
      if (!SignalToStopThreadReceived) {
         logger.debug("Stop signal received for thread - {}", LoadUnitSenderThread);
         filesManager.printToOutputTextArea("Stopping Load Unit Sender on location " + SourceLocation, OutputTextArea);
         SQLQueries.disconnect();
         SignalToStopThreadReceived = true;
         LoadUnitSenderThread.interrupt();
         ThreadRunning = false;
      }
   }

   @Override
   public String toString() {
      return "LoadUnitSender{" +
              "delay=" + Delay +
              ", createTransportByCoordEndpoint='" + CreateTransportRestCall + '\'' +
              ", sourceLocation='" + SourceLocation + '\'' +
              ", destinationLocation='" + DestinationLocation + '\'' +
              ", client='" + Client + '\'' +
              '}';
   }

   public void setOutputTextArea(TextArea outputTextArea) {
      OutputTextArea = outputTextArea;
   }

   public void setClarify(boolean clarify) {
      Clarify = clarify;
   }

   public void setDelay(String delay) {
      try {
         Delay = (long) (Double.parseDouble(delay) * 1000);
      } catch (NumberFormatException e) {
         //will be taken care of in validations
         logger.trace("Incorrect format passed");
      }
   }

   public void setCreateTransportByCoordEndpoint(String createTransportByCoordEndpoint) {
      CreateTransportRestCall = createTransportByCoordEndpoint;
   }

   public void setSourceLocation(String sourceLocation) {
      SourceLocation = sourceLocation;
   }

   public void setDestinationLocation(String destinationLocation) {
      DestinationLocation = destinationLocation;
   }

   public void setClient(String client) {
      Client = client;
   }
}
