package venckus.gytis.programs.improvedmessagetrace.control.database;

import org.joda.time.DateTime;
import venckus.gytis.database.SqlManager;
import venckus.gytis.general.FilesManager;
import venckus.gytis.javafx.GUI.mainGUI.Controller;
import venckus.gytis.javafx.Main;
import venckus.gytis.programs.improvedmessagetrace.control.MessageChecker;
import venckus.gytis.programs.improvedmessagetrace.entity.Message;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;

public class MessagesManagerSql extends SqlManager {

   private static double Progress = 0;

   public MessagesManagerSql(String hostname, String port, String SID, String dbusername, String dbpassword) {
      super(hostname, port, SID, dbusername, dbpassword);
   }

   public MessagesManagerSql(String username, String password, String url) {
      super(username, password, url);
   }

   private ArrayList<Message> messages;
   private FilesManager filesManager = new FilesManager();

   private String[] standartOperationTypes = {
           "TUDR", "TUMI", "TULL", "TUNO", "TURP",
           "TUEX", "TUCA"
   };

   private void fetchData() {
      try {
         int rownum = 1;
         DateTime earliestMessageTime = DateTime.now().minusMinutes(MessageChecker.getTimestamp());
         Timestamp ts = new Timestamp(earliestMessageTime.getMillis());
         messages = new ArrayList<>();
         Connection connection = connect();
         PreparedStatement countRows = connection.prepareStatement(
                 "SELECT COUNT(*) AS rowcount FROM APP_MM_TRACE " +
                         "WHERE cdate >= to_timestamp('" + ts + "', 'YYYY-MM-DD HH24:MI:SS.FF')");
         ResultSet countRs = countRows.executeQuery();
         countRs.next();
         int count = countRs.getInt("rowcount");
         countRs.close();

         PreparedStatement preparedStatement = connection.prepareStatement(
                 "SELECT * FROM APP_MM_TRACE " +
                         "WHERE cdate >= to_timestamp('" + ts + "', 'YYYY-MM-DD HH24:MI:SS.FF') " +
                         "ORDER BY cdate ASC");
         ResultSet rs = preparedStatement.executeQuery();
         System.out.println("total rows = " + count);
         long start = System.nanoTime();
         while (rs.next()) {
            Progress = (double)rownum/count;
            System.out.println(rownum + "/" + count);
            Main.controllerHandler.improvedTrace_progressBar.setProgress(Progress);
            if (rownum < count) rownum++;
            if (!isStandartOperationType(rs.getString("OPERATION_ID"))) {
               continue;
            }
            Message message = new Message(rs);
            messages.add(message);
         }
         System.out.println("Elapsed: " + (double) (System.nanoTime() - start) / 1000000000);
      } catch (Throwable e) {
         filesManager.writeToFile(getErrorMessage(), "ImprovedMessageTrace", Controller.improvedTracelogFileName);
         e.printStackTrace();
      } finally {
         System.out.println("Finished retrieving from SQL");
         disconnect();
      }
   }

   public ArrayList<Message> getMessages() {
      fetchData();
      Collections.sort(messages);
      Collections.reverse(messages);
      return messages;
   }

   private boolean isStandartOperationType(String operationType) {
      for (String operation : standartOperationTypes) {
         if (operationType.equals(operation)) {
            return true;
         }
      }
      return false;
   }
}
