package venckus.gytis.programs.improvedmessagetrace.control.subsystem;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import venckus.gytis.general.FilesManager;
import venckus.gytis.javafx.GUI.mainGUI.Controller;
import venckus.gytis.programs.improvedmessagetrace.control.parser.LocationsParser;
import venckus.gytis.programs.improvedmessagetrace.entity.LogicalLocation;
import venckus.gytis.programs.improvedmessagetrace.entity.Operation;

import java.util.*;

public class ExcelManager {

   private String pathToXLS = Controller.improvedTracePathToExcel;

   private ArrayList<XSSFSheet> informationalSheets;

   private Map<String, ArrayList<String>> allPossibleLocations;
   private Map<String, Integer> PLC_and_LogicalLocation_Rows;
   private Map<String, Integer> PLC_Columns;
   private Map<String, Integer> LogicalLocations_Columns;
   private Map<String, Integer> CommentsColumns;

   private LocationsParser locationsParser;
   private FilesManager filesManager = new FilesManager();

   private static String[] OperationTypes = {
           "TUDR", "TUMI", "TULL", "TUNO", "TURP",
           "TUEX", "TUCA", "STFI", "SETD",
           "DEMI", "DELL", "DENO", "DERP"
   };

   public ExcelManager() {
      locationsParser = new LocationsParser();
      allPossibleLocations = new HashMap<>();
      PLC_and_LogicalLocation_Rows = new HashMap<>();
      PLC_Columns = new HashMap<>();
      LogicalLocations_Columns = new HashMap<>();
      CommentsColumns = new HashMap<>();
      informationalSheets = new ArrayList<>();
   }


   public ArrayList<LogicalLocation> createMap() {
      XSSFWorkbook PLC_DIQ_InterfaceXLS = openXLSFile();
      XSSFSheet[] allSheets = getSheets(PLC_DIQ_InterfaceXLS);
      filterInformationalSheets(allSheets);
      return mapAllInformationalSheets();
   }

   /**
    * Method attempts to open .xls or .xlsx file
    *
    * @return opened file
    */
   private XSSFWorkbook openXLSFile() {
      XSSFWorkbook file = new XSSFWorkbook();
      try {
         file = new XSSFWorkbook(pathToXLS);
      } catch (Throwable e) {
         filesManager.writeToFile(ExceptionUtils.getStackTrace(e), "ImprovedMessageTrace", Controller.improvedTracelogFileName);
         e.printStackTrace();
      }
      return file;
   }

   /**
    * Loops through all sheets in given excel workbook
    *
    * @param PLC_DIQ_InterfaceXLS Excel workbook
    * @return array of visible/unhidden XSSFSheet objects
    */
   private XSSFSheet[] getSheets(XSSFWorkbook PLC_DIQ_InterfaceXLS) {
      int size = 0;
      XSSFSheet[] sheets = new XSSFSheet[PLC_DIQ_InterfaceXLS.getNumberOfSheets()];
      for (int i = 0; i < PLC_DIQ_InterfaceXLS.getNumberOfSheets(); i++) {
         if (!PLC_DIQ_InterfaceXLS.isSheetHidden(i)) {
            size++;
            sheets[i] = PLC_DIQ_InterfaceXLS.getSheetAt(i);
         }
      }
      sheets = Arrays.copyOf(sheets, size);
      return sheets;
   }

   /**
    * The method loops through all cells in a sheet and looks for pattern
    * (if cells with "PLC" and "Logical location" are side by side)
    *
    * @param sheet Sheet which is processed
    * @return true - if pattern is found, then this sheet is useful
    */
   private boolean isSheetInformational(XSSFSheet sheet) {
      boolean sheetInformational = false;
      ArrayList<String> tempCellLocations = new ArrayList<>();
      Iterator rowIterator = sheet.rowIterator();
      while (rowIterator.hasNext()) {
         Row row = (Row) rowIterator.next();
         Iterator cellIterator = row.cellIterator();
         while (cellIterator.hasNext()) {
            Cell cell = (Cell) cellIterator.next();
            try {
               String cellValue = cell.getStringCellValue();

               if (cellValue.contains("Logical")) {
                  if (row.getCell(cell.getColumnIndex() - 1).getStringCellValue().contains("PLC")) {
                     PLC_and_LogicalLocation_Rows.put(sheet.getSheetName(), row.getRowNum());
                     PLC_Columns.put(sheet.getSheetName(), cell.getColumnIndex() - 1);
                     LogicalLocations_Columns.put(sheet.getSheetName(), cell.getColumnIndex());
                     sheetInformational = true;
                  }

               }
               if (sheetInformational && cellValue.contains("Comments")) {
                  CommentsColumns.put(sheet.getSheetName(), cell.getColumnIndex());
               }
               if (sheetInformational && cell.getColumnIndex() == LogicalLocations_Columns.get(sheet.getSheetName()) && row.getRowNum() > PLC_and_LogicalLocation_Rows.get(sheet.getSheetName())) {

                  if (!cellValue.isEmpty()) {
                     ArrayList<String> cellLocations = parseLogicalLocations(cellValue);
                     tempCellLocations.addAll(cellLocations);
                     allPossibleLocations.put(sheet.getSheetName(), tempCellLocations);
                  }
               }
            } catch (IllegalStateException e) {
               //we don't care about non String valued cells
            }
         }
      }

      return sheetInformational;
   }


   /**
    * The method loops through all cells in a sheet and maps all values
    *
    * @param sheet Sheet which is processed
    * @return mapped LogicalLocation type ArrayList
    */
   private ArrayList<LogicalLocation> mapLogicalLocations(XSSFSheet sheet) {
      ArrayList<LogicalLocation> logicalLocations = new ArrayList<>();

      Iterator rowIterator = sheet.rowIterator();
      while (rowIterator.hasNext()) {
         LogicalLocation logicalLocation = new LogicalLocation();
         Row row = (Row) rowIterator.next();
         if (row.getRowNum() <= PLC_and_LogicalLocation_Rows.get(sheet.getSheetName())) {
            continue;
         }
         try {
            if (row.getCell(PLC_Columns.get(sheet.getSheetName())) == null) {
               continue;
            }
         } catch (NullPointerException e) {
            filesManager.writeToFile(ExceptionUtils.getStackTrace(e), "ImprovedMessageTrace", Controller.improvedTracelogFileName);
         }
         Iterator cellIterator = row.cellIterator();
         while (cellIterator.hasNext()) {
            Cell cell = (Cell) cellIterator.next();
            try {
               String cellValue = cell.getStringCellValue();
               if (row.getRowNum() > PLC_and_LogicalLocation_Rows.get(sheet.getSheetName())) {
                  if (cell.getColumnIndex() == PLC_Columns.get(sheet.getSheetName())) {
                     logicalLocation.setPLC(cellValue);
                  } else if (cell.getColumnIndex() == LogicalLocations_Columns.get(sheet.getSheetName())) {
                     logicalLocation.setName(cellValue);
                  } else if (cell.getColumnIndex() == CommentsColumns.get(sheet.getSheetName())) {
                     logicalLocation.setComment(cellValue);
                  } else {
                     if (cellValue.contains("X")) {
                        Operation operation = new Operation();
                        operation.setOperationType(parseOperationType(sheet.getRow(PLC_and_LogicalLocation_Rows.get(sheet.getSheetName()))
                                .getCell(cell.getColumnIndex()).getStringCellValue()));
                        if (operation.getOperationType().equals("TUMI")) {
                           cell = (Cell) cellIterator.next();
                           cellValue = cell.getStringCellValue();
                           ArrayList<String> parsedLogicalLocations = parseLogicalLocations(cellValue);
                           cleanLogicalLocations(parsedLogicalLocations);
                           logicalLocation.setConnections(parsedLogicalLocations);
                           //TODO add note for TUMI messages
                        } else {
                           String note = cellValue.replaceFirst("X", "").replaceAll("\n", "");
                           operation.setOperationNote(note);
                        }
                        logicalLocation.addOperation(operation);
                     }
                  }

               }

            } catch (IllegalStateException e) {
               filesManager.writeToFile(ExceptionUtils.getStackTrace(e), "ImprovedMessageTrace", Controller.improvedTracelogFileName);
            }
         }
         if (logicalLocation.getName() == null) {
            continue;
         }
         ArrayList<String> relatedLogicalLocations = parseLogicalLocations(logicalLocation.getName());
         if (relatedLogicalLocations.size() > 1) {
            cleanLogicalLocations(relatedLogicalLocations);
            for (String tempLoc : relatedLogicalLocations) {
               LogicalLocation tempLocation = new LogicalLocation(tempLoc, logicalLocation.getPLC(), logicalLocation.getOperations(), logicalLocation.getConnections(), logicalLocation.getComment());
               logicalLocations.add(tempLocation);
            }
         } else {
            logicalLocations.add(logicalLocation);
         }
      }
      return logicalLocations;
   }

   private void filterInformationalSheets(XSSFSheet[] sheets) {
      for (XSSFSheet sheet : sheets) {
         if (isSheetInformational(sheet)) {
            informationalSheets.add(sheet);
         }
      }
   }

   private ArrayList<LogicalLocation> mapAllInformationalSheets() {
      ArrayList<LogicalLocation> mappedLocations = new ArrayList<>();
      for (XSSFSheet sheet : informationalSheets) {
         mappedLocations.addAll(mapLogicalLocations(sheet));
      }
      return mappedLocations;
   }

   /**
    * The method loops through all defined available operation types
    * and checks if given value of cell matches one of the types
    *
    * @param cellValue cell value which is processed
    * @return operation String if match is found, otherwise return null
    */
   private String parseOperationType(String cellValue) {
      for (String operation : OperationTypes) {
         if (cellValue.contains(operation)) {
            return operation;
         }
      }
      return null;
   }

   /**
    * The method splits cell value to locations by new line and then
    * extracts location to multiple locations if it is required
    * i.e. result of given cell value "PCRE[01-02]CS0[1-2]\nPCRE01CS03"
    * would be PCRE01CS01 PCRE02CS01 PCRE01CS02 PCRE02CS02 PCRE01CS03
    *
    * @param cellValue cell value which is processed
    * @return ArrayList of locations
    */
   private ArrayList<String> parseLogicalLocations(String cellValue) {
      String[] splittedLocations = cellValue.split("\n");
      ArrayList<String> allLocations = new ArrayList<>();
      for (String location : splittedLocations) {
         allLocations.addAll(locationsParser.processLocation(location));
      }
      return allLocations;
   }

   /**
    * The method "cleans" all logical locations, so that their size consists
    * of exactly 10 characters and would not contain any unnecessary info
    *
    * @param allLocations passed locations which may need cleaning
    */
   private void cleanLogicalLocations(ArrayList<String> allLocations) {
      for (String location : allLocations) {
         if (location.contains(" ")) {
            allLocations.set(allLocations.indexOf(location), location.split(" ")[0]);
         }
      }
   }

//    private Map<String, String> cleanLogicalLocations(ArrayList<String> allLocations) {
//        Map<String, String> cleanedLogicalLocations = new HashMap<>();
//        for (String location : allLocations) {
//            if (location.contains(" ")) {
//                String [] parts = location.split(" ");
//                cleanedLogicalLocations.put(parts[0], location.substring(location.lastIndexOf(parts[0])));
//            } else {
//                cleanedLogicalLocations.put(location, "");
//            }
//        }
//        return cleanedLogicalLocations;
//    }
}
