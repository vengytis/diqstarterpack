package venckus.gytis.programs.improvedmessagetrace.control.subsystem;

public class SubsystemsManager {

   private ExcelManager excelManager;
   private MessageManager messageManager;
   private DMSManager dmsManager;

   public SubsystemsManager() {
      setup();
   }

   private void setup() {
      excelManager = new ExcelManager();
      messageManager = new MessageManager();
      dmsManager = new DMSManager();
   }

   public ExcelManager getExcelManager() {
      return excelManager;
   }

   public MessageManager getMessageManager() {
      return messageManager;
   }

   public DMSManager getDmsManager() { return dmsManager; }
}
