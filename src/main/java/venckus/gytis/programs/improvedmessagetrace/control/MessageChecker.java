package venckus.gytis.programs.improvedmessagetrace.control;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.ListOrderedMap;
import venckus.gytis.general.FilesManager;
import venckus.gytis.general.InformationalCodes;
import venckus.gytis.javafx.Main;
import venckus.gytis.programs.improvedmessagetrace.control.subsystem.DMSManager;
import venckus.gytis.programs.improvedmessagetrace.control.subsystem.SubsystemsManager;
import venckus.gytis.programs.improvedmessagetrace.entity.*;

import java.util.ArrayList;
import java.util.Map;

public class MessageChecker {

   private SubsystemsManager subsystemsManager;

   private ArrayList<LogicalLocation> mappedLogicalLocations;
   private Map<String, ArrayList<Message>> messages;
   private ArrayList<LocationException> locationExceptions;
   private static int Timestamp;
   private Message previousMessage = null;
   private FilesManager filesManager;

   private ArrayList<LoadUnit> retrievedLoadUnits;

   public MessageChecker(String configName) {
      subsystemsManager = new SubsystemsManager();
      //Adding locations from excel
      mappedLogicalLocations = subsystemsManager.getExcelManager().createMap();
      //Adding generated DMS or HBW locations
      if (Main.controllerHandler.improvedTrace_dmsRadioButton.isSelected()) {
         DMSManager dmsManager = subsystemsManager.getDmsManager();
         dmsManager.generateDMSLocations();
         mappedLogicalLocations.addAll(dmsManager.getDMSLogicalLocations());
      } else if (Main.controllerHandler.improvedTrace_hbwRadioButton.isSelected()) {
         //TODO: add logic for hbw locations generation
      }
      //Retrieved messages
      messages = subsystemsManager.getMessageManager().getLoadUnitsMessages();
      filesManager = new FilesManager();
      locationExceptions = filesManager.mapLocationsExceptionsFromJSON(configName);
      retrievedLoadUnits = retrieveLuMessages();
   }

   public static int getTimestamp() {
      return Timestamp;
   }

   public static void setTimestamp(int timestamp) {
      Timestamp = timestamp;
   }

   private ArrayList<LoadUnit> retrieveLuMessages() {
      Platform.runLater(() -> Main.controllerHandler.improvedTrace_retrievalStatus.setText(InformationalCodes.IMT_MAPPING_MESSAGES));
      ArrayList<LoadUnit> mappedLoadUnitsData = new ArrayList<>();
      Map<String, ArrayList<Message>> loadUnitsMessages = messages;
      for (String loadUnit : loadUnitsMessages.keySet()) {
         mappedLoadUnitsData.add(new LoadUnit(loadUnit, true, new ListOrderedMap<>() {
            {
               ArrayList<Message> LUmessages = loadUnitsMessages.get(loadUnit);
               for (Message msg : LUmessages) {
                  put(msg, false);
               }
            }
         }));
      }
      for (LoadUnit loadUnit : mappedLoadUnitsData) {
         for (Message message : loadUnit.getMessages().keySet()) {
            if (isCurrentLocationException(message)) {
               message.setMessageNote(InformationalCodes.IMT_EXCEPTION_MESSAGE);
               loadUnit.getMessages().put(message, true);
            } else {
               if (isCurrentLocationValid(message)) {
                  if (!containsDupplicateMessages(message, loadUnit.getId())) {
                     loadUnit.getMessages().put(message, true);
                  } else {
                     message.setMessageNote(InformationalCodes.IMT_DUPLICATE_MESSAGE);
                     loadUnit.getMessages().put(message, false);
                     loadUnit.setContainsOnlyValidMessages(false);
                  }
               } else {
                  message.setMessageNote(InformationalCodes.IMT_INVALID_MAPING);
                  loadUnit.getMessages().put(message, false);
                  loadUnit.setContainsOnlyValidMessages(false);
               }
            }
            if (message.getOperationId().equals("TUMI")) {
               message.setDisplayMessage(
                       message.getCreateDate().toLocalDateTime()
                               + " " + message.getOperationId()
                               + " " + message.getDestinationPLC()
                               + " " + message.getCurrentLocation()
                               + " -> " + message.getDestinationLocation()
               );
            } else {
               message.setDisplayMessage(
                       message.getCreateDate().toLocalDateTime()
                               + " " + message.getOperationId()
                               + " " + message.getSourcePLC()
                               + " " + message.getCurrentLocation()
               );
            }
            previousMessage = message;
         }
      }
      return mappedLoadUnitsData;
   }

   private boolean isCurrentLocationValid(Message message) {

      //TODO delete this after implementing interaisles handling
      if (message.getCurrentLocation().contains("IA01") || message.getDestinationLocation().contains("IA01")) {
         return true;
      }
      //TODO delete this after figuring out how to solve DS PLC mapping from excel
      if (message.getCurrentLocation().contains("DS")) {
         return true;
      }

      boolean isMessageTumi = message.getOperationId().equals("TUMI");
      for (LogicalLocation location : mappedLogicalLocations) {
         if (message.getCurrentLocation().equals(location.getName()) &&
                 (isMessageTumi ? message.getDestinationPLC().equals(location.getPLC()) :
                         message.getSourcePLC().equals(location.getPLC()))) {
            for (Operation operation : location.getOperations()) {
               message.setMessageNote(operation.getOperationNote());
               message.setComment(location.getComment());
               if (message.getOperationId().equals(operation.getOperationType())) {
                  if (message.getOperationId().equals("TULL")) {
                     if (!previousMessage.getCurrentLocation().equals(message.getCurrentLocation()) && previousMessage != null && previousMessage.getLU().equals(message.getLU())) {
                        message.setMessageNote(InformationalCodes.IMT_TULL_MESSAGE_NOTE);
                        return false;
                     }
                     return true;
                  } else if (message.getOperationId().equals("TUMI")) {
                     for (String connection : location.getConnections()) {
                        if (message.getDestinationLocation().equals(connection)) {
                           return true;
                        }
                     }
                     return false;
                  } else {
                     return true;
                  }
               }
            }
            return false;
         }
      }
      return false;
   }

   private boolean isCurrentLocationException(Message message) {
      for (LocationException locationException : locationExceptions) {
         if (message.equals(locationException))
            return true;
      }
      return false;
   }

   @SuppressWarnings("unchecked")
   public ArrayList<LoadUnit> retrieveLoadUnitsWithAppliedFilters(ArrayList<LoadUnit> initialLoadUnits, boolean onlyInvalidLoadUnits, String locationPassed, String barcode) {
      ArrayList<LoadUnit> tempLoadUnits = (ArrayList<LoadUnit>) initialLoadUnits.clone();
      if (onlyInvalidLoadUnits) {
         filterInvalidLoadUnits(tempLoadUnits);
      }
      if (!locationPassed.isEmpty()) {
         tempLoadUnits = getLoadUnitsWhichPassedLocation(locationPassed, tempLoadUnits);
      }
      if (!barcode.isEmpty()) {
         filterByBarcode(tempLoadUnits, barcode);
      }
      return tempLoadUnits;
   }

   private ArrayList<LoadUnit> getLoadUnitsWhichPassedLocation(String passedLocation, ArrayList<LoadUnit> loadUnits) {
      boolean notPassedLocationEnabled = passedLocation.startsWith("!");
      passedLocation = passedLocation.replaceAll("!", "");
      ArrayList<LoadUnit> tempLoadUnits = new ArrayList<>();
      for (LoadUnit loadUnit : loadUnits) {
         for (Message message : loadUnit.getMessages().keySet()) {
            if (message.getCurrentLocation().contains(passedLocation)) {
               if (!tempLoadUnits.contains(loadUnit)) {
                  tempLoadUnits.add(loadUnit);
               }
            }
         }
      }
      if (notPassedLocationEnabled) {
         loadUnits.removeAll(tempLoadUnits);
         return loadUnits;
      }
      return tempLoadUnits;
   }

   /**
    * Checks if neighbour messages (1 message before or 1 message after)
    * are the same as current message
    * @param message message to be checked now
    * @param loadUnit load unit to which this message belongs
    * @return
    */
   private boolean containsDupplicateMessages(Message message, String loadUnit) {
      int count = 0;
      ArrayList<Message> luMessages = messages.get(loadUnit);
      int messageIndex = luMessages.indexOf(message);
      ArrayList<Message> neighbourMessages = new ArrayList<>() {
         {
            try {
               if (luMessages.size() > 1) {
                  if (messageIndex == 0) {
                     add(luMessages.get(messageIndex + 1));
                  } else if (messageIndex == luMessages.size() - 1) {
                     add(luMessages.get(messageIndex - 1));
                  } else {
                     add(luMessages.get(messageIndex - 1));
                     add(luMessages.get(messageIndex + 1));
                  }
               }
            } catch (Exception e) {
               e.printStackTrace();
            }
         }
      };
      for (Message msg : neighbourMessages) {
         if (msg.equals(message)) {
            count++;
         }
      }
      return count > 0;
   }

   public ListOrderedMap<String, Boolean> getDisplayMessagesByLoadUnit(String loadUnit) {
      ListOrderedMap<String, Boolean> messages = new ListOrderedMap<>();
      for (LoadUnit lu : retrievedLoadUnits) {
         if (lu.getId().equals(loadUnit)) {
            for (Message message : lu.getMessages().keySet()) {
               messages.put(message.getDisplayMessage(), lu.getMessages().get(message));
            }
         }
      }
      return messages;
   }

   public ObservableList<String> getLoadUnitsAsStrings(ObservableList<LoadUnit> loadUnits) {
      ObservableList<String> loadUnitsIds = FXCollections.observableArrayList();
      for (LoadUnit loadUnit : loadUnits) {
         loadUnitsIds.add(loadUnit.getId());
      }
      return loadUnitsIds;
   }

   private LoadUnit findLoadUnitByString(String loadUnitId) {
      LoadUnit loadUnit = new LoadUnit();
      for (LoadUnit lu : retrievedLoadUnits) {
         if (lu.getId().equals(loadUnitId))
            loadUnit = lu;
      }
      return loadUnit;
   }

   private void filterInvalidLoadUnits(ArrayList<LoadUnit> loadUnits) {
      loadUnits.removeIf(LoadUnit::getContainsOnlyValidMessages);
   }

   private void filterByBarcode(ArrayList<LoadUnit> loadUnits, String barcode) {
      if (barcode.startsWith("!")) {
         loadUnits.removeIf(loadUnit ->
                 loadUnit.getId().startsWith(barcode.replaceAll("!", "")));
      } else {
         loadUnits.removeIf(loadUnit -> !loadUnit.getId().startsWith(barcode));
      }
   }

   public String retrieveMessageNote(String loadUnit, int index) {
      return retrieveMessageByIndex(loadUnit, index).getMessageNote();
   }

   public Message retrieveMessageByIndex(String loadUnit, int index) {
      return findLoadUnitByString(loadUnit).getMessages().get(index);
   }

   public String retrieveComment(String loadUnit, int index) {
      return findLoadUnitByString(loadUnit).getMessages().get(index).getComment();
   }

   public ArrayList<LoadUnit> getRetrievedLoadUnits() {
      return retrievedLoadUnits;
   }
}
