package venckus.gytis.programs.improvedmessagetrace.control.subsystem;

import org.joda.time.DateTime;
import venckus.gytis.javafx.GUI.mainGUI.Controller;
import venckus.gytis.programs.improvedmessagetrace.control.MessageChecker;
import venckus.gytis.programs.improvedmessagetrace.control.database.MessagesManagerSql;
import venckus.gytis.programs.improvedmessagetrace.entity.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MessageManager {

   private ArrayList<Message> messages;
   private MessagesManagerSql managerSql;
   private DateTime dateNow = DateTime.now();

   public MessageManager() {
      managerSql = new MessagesManagerSql(
              Controller.improvedTraceMessagesManagerSql.getUsername(),
              Controller.improvedTraceMessagesManagerSql.getPassword(),
              Controller.improvedTraceMessagesManagerSql.getURL()
      );
      messages = managerSql.getMessages();
   }

   private ArrayList<String> getLUWithinTimestamp() {
      ArrayList<String> LUByTimestamp = new ArrayList<>();
      for (Message msg : messages) {
         if (msg.getCreateDate().plusMinutes(MessageChecker.getTimestamp()).isAfter(dateNow)) {
            if (!LUByTimestamp.contains(msg.getLU()) && msg.getLU() != null) {
               LUByTimestamp.add(msg.getLU());
            }
         }
      }
      return LUByTimestamp;
   }

   private ArrayList<Message> getMessagesByLU(String loadUnit) {
      ArrayList<Message> messagesByLU = new ArrayList<>();
      for (Message msg : messages) {
         if (msg.getLU() != null) {
            if (msg.getLU().equals(loadUnit)) {
               messagesByLU.add(msg);
            }
         }
      }
      return messagesByLU;
   }

   public Map<String, ArrayList<Message>> getLoadUnitsMessages() {
      Map<String, ArrayList<Message>> loadUnitsMessages = new HashMap<>();
      for (String loadUnit : getLUWithinTimestamp()) {
         loadUnitsMessages.put(loadUnit, getMessagesByLU(loadUnit));
      }
      return loadUnitsMessages;
   }
}
