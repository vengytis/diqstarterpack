package venckus.gytis.programs.improvedmessagetrace.control.subsystem;

import venckus.gytis.programs.improvedmessagetrace.entity.LogicalLocation;
import venckus.gytis.programs.improvedmessagetrace.entity.Operation;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class DMSManager {
   private static int MaxNumberOfAisles;
   private static int MaxXLocations;
   private static int MaxYLocations;
   private static int MaxDepth;
   private static int FirstAisle;

   private ArrayList<LogicalLocation> LogicalLocations;

   private NumberFormat twoDigitsFormatter = new DecimalFormat("00");
   private NumberFormat threeDigitsFormatter = new DecimalFormat("000");

   public DMSManager() {
      LogicalLocations = new ArrayList<>();
   }

   public void generateDMSLocations() {
      generateBins();
      generateShuttles();
      generateRackIns();
      generateRackOuts();
      generateLifts();
      generatePSandDS();
   }

   public static void setDMSValues(int maxNumberOfAisles, int maxXLocations, int maxYLocations, int maxDepth, int firstAisle) {
      MaxNumberOfAisles = maxNumberOfAisles;
      MaxXLocations = maxXLocations;
      MaxYLocations = maxYLocations;
      MaxDepth = maxDepth;
      FirstAisle = firstAisle;
   }

   private void generateBins() {
      for (int i = FirstAisle; i < FirstAisle + MaxNumberOfAisles; i++) {
         String aisleNumber = twoDigitsFormatter.format(i);
         for (int l = 1; l <= 2; l++) {
            String side = String.valueOf(l);
            for (int j = 1; j <= MaxXLocations; j++) {
               String xLocation = threeDigitsFormatter.format(i);
               if (j < 100)
                  for (int k = 1; k <= MaxYLocations; k++) {
                     String yLocation = twoDigitsFormatter.format(k);
                     for (int m = 1; m <= MaxDepth; m++) {
                        String depth = "0" + m;
                        String binLocation = "MS" + aisleNumber + side + xLocation + yLocation + depth + "11";
                        LogicalLocation binLogicalLocation = new LogicalLocation();
                        binLogicalLocation.setName(binLocation);
                        binLogicalLocation.setPLC("MS" + binLocation.substring(2, 4));
                        ArrayList<Operation> binOperations = new ArrayList<Operation>() {
                           {
                              add(new Operation("TURP", "when load unit gets stored inside DMS"));
                              add(new Operation("TUMI"));
                           }
                        };
                        binLogicalLocation.setOperations(binOperations);
                        ArrayList<String> binConnections = new ArrayList<String>() {
                           {
                              add(String.format("MSAI%s%s%sRO10", aisleNumber, "LL", yLocation));
                              add(String.format("MSAI%s%s%sRO10", aisleNumber, "LR", yLocation));
                              //TODO add interaisles logic
                           }
                        };
                        binLogicalLocation.setConnections(binConnections);
                        LogicalLocations.add(binLogicalLocation);
                     }
                  }
            }
         }
      }
   }

   private void generateShuttles() {
      for (int i = FirstAisle; i < FirstAisle + MaxNumberOfAisles; i++) {
         String aisleNumber = twoDigitsFormatter.format(i);
         for (int j = 1; j <= MaxYLocations; j++) {
            String yLocation = twoDigitsFormatter.format(j);
            String shuttle = "MSAI" + aisleNumber + "LV" + yLocation + "SH01";
            LogicalLocation shuttleLogicalLocation = new LogicalLocation();
            shuttleLogicalLocation.setName(shuttle);
            shuttleLogicalLocation.setPLC("MS" + shuttle.substring(4, 6));
            ArrayList<Operation> shuttleOperations = new ArrayList<Operation>() {
               {
                  add(new Operation("TUNO", "load unit is on the shuttle"));
                  add(new Operation("TUEX", "load unit rejected from bin"));
               }
            };
            shuttleLogicalLocation.setOperations(shuttleOperations);
            LogicalLocations.add(shuttleLogicalLocation);
         }
      }
   }

   private void generateRackIns() {
      for (int i = FirstAisle; i < FirstAisle + MaxNumberOfAisles; i++) {
         String aisleNumber = twoDigitsFormatter.format(i);
         for (int j = 1; j <= 2; j++) {
            String side = (j == 1) ? "LL" : "LR";
            for (int k = 1; k <= MaxYLocations; k++) {
               String yLocation = twoDigitsFormatter.format(k);
               String RackIn = "MSAI" + aisleNumber + side + yLocation + "RI" + "10";
               LogicalLocation RackInLogicalLocation = new LogicalLocation();
               RackInLogicalLocation.setName(RackIn);
               RackInLogicalLocation.setPLC("MS" + RackIn.substring(4, 6));
               ArrayList<Operation> RIOperations = new ArrayList<Operation>() {
                  {
                     add(new Operation("TURP", "load unit is put on rack in"));
                     add(new Operation("TUMI", "load unit has transport mission to bin"));
                  }
               };
               RackInLogicalLocation.setOperations(RIOperations);
               ArrayList<String> RIandROConnections = new ArrayList<String>() {
                  {
                     for (int x = 1; x <= MaxXLocations; x++) {
                        String xLocation = threeDigitsFormatter.format(x);
                        for (int side = 1; side <= 2; side++) {
                           for (int depth = 1; depth <= 2; depth++) {
                              add(String.format("MS%s%s%s%s%s11", aisleNumber, String.valueOf(side), xLocation, yLocation, "0" + depth));
                           }
                        }
                     }
                  }
               };
               RackInLogicalLocation.setConnections(RIandROConnections);
               LogicalLocations.add(RackInLogicalLocation);
            }
         }
      }
   }

   private void generateRackOuts() {
      for (int i = FirstAisle; i < FirstAisle + MaxNumberOfAisles; i++) {
         String aisleNumber = twoDigitsFormatter.format(i);
         for (int j = 1; j <= 2; j++) {
            String side = (j == 1) ? "LL" : "LR";
            for (int k = 1; k <= MaxYLocations; k++) {
               String yLocation = twoDigitsFormatter.format(k);
               String RackOut = "MSAI" + aisleNumber + side + yLocation + "RO" + "10";
               LogicalLocation RackOutLogicalLocation = new LogicalLocation();
               RackOutLogicalLocation.setName(RackOut);
               RackOutLogicalLocation.setPLC("MS" + RackOut.substring(4, 6));
               ArrayList<Operation> RackOutOperations = new ArrayList<Operation>() {
                  {
                     add(new Operation("TURP", "load unit is on the rack out"));
                     add(new Operation("TUMI"));
                  }
               };
               RackOutLogicalLocation.setOperations(RackOutOperations);
               ArrayList<String> RackOutConnections = new ArrayList<String>() {
                  {
                     add(String.format("MSAI%s%s01DS10", aisleNumber, ((side == "LL") ? "CL" : "CR")));
                     for (int i = 1; i <= MaxYLocations; i++) {
                        add(String.format("MSAI%s%s%sRI10", aisleNumber, side, twoDigitsFormatter.format(i)));
                     }
                  }
               };
               RackOutLogicalLocation.setConnections(RackOutConnections);
               LogicalLocations.add(RackOutLogicalLocation);
            }
         }
      }
   }

   private void generateLifts() {
      for (int i = FirstAisle; i < FirstAisle + MaxNumberOfAisles; i++) {
         String aisleNumber = twoDigitsFormatter.format(i);
         for (int j = 1; j <= 2; j++) {
            String side = (j == 1) ? "EL" : "ER";
            String lift = "MSAI" + aisleNumber + side + "01LO00";
            LogicalLocation liftLogicalLocation = new LogicalLocation();
            liftLogicalLocation.setName(lift);
            liftLogicalLocation.setPLC("MS" + lift.substring(4, 6));
            ArrayList<Operation> liftOperations = new ArrayList<Operation>() {
               {
                  add(new Operation("TUEX", "load unit rejected from the lift"));
                  add(new Operation("TUNO", "load unit is on the lift"));
               }
            };
            liftLogicalLocation.setOperations(liftOperations);
            LogicalLocations.add(liftLogicalLocation);
         }
      }
   }

   private void generatePSandDS() {
      for (int i = FirstAisle; i < FirstAisle + MaxNumberOfAisles; i++) {
         String aisleNumber = twoDigitsFormatter.format(i);
         for (int j = 1; j <= 2; j++) {
            String side = (j == 1) ? "CL" : "CR";
            for (int k = 1; k <= 2; k++) {
               String PSorDS = (k == 1) ? "PS" : "DS";
               String station = "MSAI" + aisleNumber + side + "01" + PSorDS + "10";
               //DS messages are written in excel, so we only need to handle PS
               if (PSorDS.equals("PS")) {
                  LogicalLocation PSLogicalLocation = new LogicalLocation();
                  PSLogicalLocation.setName(station);
                  PSLogicalLocation.setPLC("MS" + station.substring(4, 6));
                  ArrayList<Operation> PSOperations = new ArrayList<Operation>() {
                     {
                        add(new Operation("TUEX"));
                        add(new Operation("TUMI"));
                        add(new Operation("TUDR"));
                     }
                  };
                  PSLogicalLocation.setOperations(PSOperations);
                  ArrayList<String> PSConnections = new ArrayList<>() {
                     {
                        for (int y = 1; y <= MaxYLocations; y++) {
                           if (side.equals("CL")) {
                              add(String.format("MSAI%s%s%sRI10", aisleNumber, "LL", twoDigitsFormatter.format(y)));
                           } else {
                              add(String.format("MSAI%s%s%sRI10", aisleNumber, "LR", twoDigitsFormatter.format(y)));
                           }
                           add(String.format("MSAI%s%s01DS10", aisleNumber, side));
                        }
                     }
                  };
                  PSLogicalLocation.setConnections(PSConnections);
                  LogicalLocations.add(PSLogicalLocation);
               }
            }
         }
      }
   }

   public ArrayList<LogicalLocation> getDMSLogicalLocations() {
      return LogicalLocations;
   }
}
