package venckus.gytis.programs.improvedmessagetrace.control.parser;

import org.apache.commons.lang3.exception.ExceptionUtils;
import venckus.gytis.general.FilesManager;
import venckus.gytis.javafx.GUI.mainGUI.Controller;

import java.util.ArrayList;

public class LocationsParser {

   private final String pattern = "\\[.*?\\]";
   private FilesManager filesManager = new FilesManager();

   public LocationsParser() {

   }

   public ArrayList<String> processLocation(String location) {
      ArrayList<String> locations = new ArrayList<>();
      ArrayList<String> tempLocations = new ArrayList<>();
      int[] indexes = new int[10];
      int count = 0;

      int index = location.indexOf('[');
      while (index >= 0) {
         indexes[count] = index;
         index = location.indexOf('[', index + 1);
         count++;
      }

      if (count > 0) {
         locations = parseLocations(location);

         if (count >= 2) {
            for (int i = 0; i < count - 1; i++) {
               tempLocations.removeAll(tempLocations);
               for (String tempLoc : locations) {
                  tempLocations.addAll(parseLocations(tempLoc));
               }
               locations.removeAll(locations);
               locations.addAll(tempLocations);
            }
         }
      } else {
         locations.add(location);
      }
      return locations;
   }

   private ArrayList<String> parseLocations(String location) {
      ArrayList<String> locations = new ArrayList<>();
      String temp1 = location.substring(location.indexOf('[') + 1, location.indexOf('-'));
      String temp2 = location.substring(location.indexOf('-') + 1, location.indexOf(']'));
      try {
         int temp1_int = Integer.parseInt(temp1);
         int temp2_int = Integer.parseInt(temp2);
         for (int i = temp1_int; i <= temp2_int; i++) {
            if (temp1.startsWith("0")) {
               if (i < 10) {
                  locations.add(location.replaceFirst(pattern, "0" + i));
               } else {
                  locations.add(location.replaceFirst(pattern, String.valueOf(i)));
               }
            } else {
               locations.add(location.replaceFirst(pattern, String.valueOf(i)));
            }
         }
      } catch (Throwable e) {
         e.printStackTrace();
         filesManager.writeToFile(ExceptionUtils.getStackTrace(e), "ImprovedMessageTrace", Controller.improvedTracelogFileName);
      }
      return locations;
   }
}
