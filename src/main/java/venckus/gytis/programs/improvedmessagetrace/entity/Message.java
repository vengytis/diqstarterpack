package venckus.gytis.programs.improvedmessagetrace.entity;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Message implements Comparable<Message> {
   private final int standartLocationLength = 14;
   private final int standartLULength = 22;
   private String OperationId;
   private String UserData;
   private String SourceLocation;
   private String CurrentLocation;
   private String DestinationLocation;
   private String LU;
   private String SourcePLC;
   private String DestinationPLC;
   private String MessageNote;
   private DateTime CreateDate;
   private String DisplayMessage;
   private String Comment;

   public Message(ResultSet resultSet) throws SQLException {
      OperationId = resultSet.getString("OPERATION_ID");
      UserData = resultSet.getString("USER_DATA");
      SourcePLC = resultSet.getString("SOURCE_NODE");
      DestinationPLC = resultSet.getString("DESTINATION_NODE");
      CreateDate = convertStringToDate(resultSet.getString("CDATE"));
      MessageNote = "";
      parseUserData();
   }

   private void parseUserData() {
      if (UserData.length() >= standartLocationLength * 3 + standartLULength) {
         SourceLocation = UserData.substring(0, standartLocationLength).replaceAll("\\.", "");
         CurrentLocation = UserData.substring(standartLocationLength, standartLocationLength * 2).replaceAll("\\.", "");
         DestinationLocation = UserData.substring(standartLocationLength * 2, standartLocationLength * 3).replaceAll("\\.", "");
         LU = UserData.substring(standartLocationLength * 3, standartLocationLength * 3 + standartLULength).replaceAll("\\.", "");
      }
   }

   @Override
   public String toString() {
      return "Message{" +
              "OperationId='" + OperationId + '\'' +
              ", SourceLocation='" + SourceLocation + '\'' +
              ", CurrentLocation='" + CurrentLocation + '\'' +
              ", DestinationLocation='" + DestinationLocation + '\'' +
              ", LU='" + LU + '\'' +
              ", SourcePLC='" + SourcePLC + '\'' +
              ", DestinationPLC='" + DestinationPLC + '\'' +
              ", Date='" + CreateDate + '\'' +
              '}';
   }

   private DateTime convertStringToDate(String sqlInput) {
      DateTime date;
      DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
      DateTimeFormatter formatter2 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
      try {
         date = formatter.parseDateTime(sqlInput);
      } catch (IllegalArgumentException e) {
         date = formatter2.parseDateTime(sqlInput);
      }
      return date;
   }

   /**
    * Sorts by create date in descending order
    *
    * @param otherMessage
    * @return
    */
   @Override
   public int compareTo(Message otherMessage) {
      return otherMessage.CreateDate.compareTo(this.CreateDate);
   }

   public boolean equals(Message otherMessage) {
      return this.getCurrentLocation().equals(otherMessage.getCurrentLocation()) &&
              this.getSourcePLC().equals(otherMessage.getSourcePLC()) &&
              this.getOperationId().equals(otherMessage.getOperationId()) &&
              this.getDestinationLocation().equals(otherMessage.getDestinationLocation());
   }

   public boolean equals(LocationException locationException) {
      return this.getCurrentLocation().equals(locationException.getLocation()) &&
              this.getOperationId().equals(locationException.getOperation()) &&
              this.getDestinationLocation().equals(locationException.getDestination());
   }

   public String getLU() {
      return LU;
   }

   public DateTime getCreateDate() {
      return CreateDate;
   }

   public String getOperationId() {
      return OperationId;
   }

   public String getSourceLocation() {
      return SourceLocation;
   }

   public String getCurrentLocation() {
      return CurrentLocation;
   }

   public String getDestinationLocation() {
      return DestinationLocation;
   }

   public String getSourcePLC() {
      return SourcePLC;
   }

   public String getDestinationPLC() {
      return DestinationPLC;
   }

   public String getMessageNote() {
      return MessageNote;
   }

   public void setMessageNote(String MessageNote) {
      if (!MessageNote.isEmpty()) {
         if (this.MessageNote.isBlank()) {
            this.MessageNote = MessageNote;
         } else {
            this.MessageNote += "\n" + MessageNote;
         }
      }
   }

   public String getComment() {
      return Comment;
   }

   public void setComment(String comment) {
      Comment = comment;
   }

   public String getDisplayMessage() {
      return DisplayMessage;
   }

   public void setDisplayMessage(String displayMessage) {
      DisplayMessage = displayMessage;
   }
}