package venckus.gytis.programs.improvedmessagetrace.entity;

import org.apache.commons.collections4.map.ListOrderedMap;

public class LoadUnit {
   private String id;
   private boolean containsOnlyValidMessages;
   private ListOrderedMap<Message, Boolean> messages;

   public LoadUnit() {

   }

   public LoadUnit(String id, boolean containsOnlyValidMessages, ListOrderedMap<Message, Boolean> messages) {
      this.id = id;
      this.containsOnlyValidMessages = containsOnlyValidMessages;
      this.messages = messages;
   }

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public boolean getContainsOnlyValidMessages() {
      return containsOnlyValidMessages;
   }

   public void setContainsOnlyValidMessages(boolean containsOnlyValidMessages) {
      this.containsOnlyValidMessages = containsOnlyValidMessages;
   }

   public ListOrderedMap<Message, Boolean> getMessages() {
      return messages;
   }

   public void setMessages(ListOrderedMap<Message, Boolean> messages) {
      this.messages = messages;
   }
}
