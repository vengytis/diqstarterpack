package venckus.gytis.programs.improvedmessagetrace.entity;

public class Operation {
   private String OperationType;
   private String OperationNote;

   public Operation() {
      OperationNote = "";
   }

   public Operation(String operationType) {
      this.OperationType = operationType;
      this.OperationNote = "";
   }

   public Operation(String operationType, String operationNote) {
      this.OperationType = operationType;
      this.OperationNote = operationNote;
   }

   public String getOperationType() {
      return OperationType;
   }

   public void setOperationType(String operationType) {
      OperationType = operationType;
   }

   public String getOperationNote() {
      return OperationNote;
   }

   public void setOperationNote(String operationNote) {
      OperationNote = operationNote;
   }
}
