package venckus.gytis.programs.improvedmessagetrace.entity;

import java.util.ArrayList;

public class LogicalLocation {
   private String Name;
   private String PLC;
   private ArrayList<Operation> Operations;
   private ArrayList<String> Connections;
   private String Comment;

   public LogicalLocation(String name, String PLC, ArrayList<Operation> operations, ArrayList<String> connections, String comment) {
      this.Name = name;
      this.PLC = PLC;
      this.Operations = operations;
      this.Connections = connections;
      this.Comment = comment;
   }

   public ArrayList<String> getConnections() {
      return Connections;
   }

   public void setConnections(ArrayList<String> connections) {
      Connections = connections;
   }

   public LogicalLocation() {
      Operations = new ArrayList<>();
   }

   public String getName() {
      return Name;
   }

   public void setName(String name) {
      Name = name;
   }

   public String getPLC() {
      return PLC;
   }

   public void setPLC(String PLC) {
      this.PLC = PLC;
   }

   public ArrayList<Operation> getOperations() {
      return Operations;
   }

   public void setOperations(ArrayList<Operation> operations) {
      this.Operations = operations;
   }

   public void addOperation(Operation operation) {
      this.Operations.add(operation);
   }

   public String getComment() {
      return Comment;
   }

   public void setComment(String Comment) {
      this.Comment = Comment;
   }
}
