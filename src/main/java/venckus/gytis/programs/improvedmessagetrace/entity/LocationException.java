package venckus.gytis.programs.improvedmessagetrace.entity;

public class LocationException {
   private String Operation;
   private String Location;
   private String Destination;

   public LocationException(String operation, String location, String destination) {
      Operation = operation;
      Location = location;
      Destination = destination;
   }

   public String getOperation() {
      return Operation;
   }

   public void setOperation(String operation) {
      Operation = operation;
   }

   public String getLocation() {
      return Location;
   }

   public void setLocation(String location) {
      Location = location;
   }

   public String getDestination() {
      return Destination;
   }

   public void setDestination(String destination) {
      Destination = destination;
   }
}
