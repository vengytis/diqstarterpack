package venckus.gytis.programs.loadunitretriever;

import javafx.scene.control.TextArea;
import venckus.gytis.database.SqlQueries;
import venckus.gytis.programs.Program;
import venckus.gytis.programs.loadunitsender.LoadUnitSender;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class LoadUnitRetriever extends LoadUnitSender implements Program {

   private long LoopDelay;

   private boolean LoopEnabled;

   private boolean StoredCheckEnabled;

   private TextArea InputTextArea;

   private ArrayList<String> LoadUnitsList;

   private Thread LoadUnitRetrieverThread;

   public LoadUnitRetriever(SqlQueries sqlQueries) {
      super(sqlQueries);
   }

   @Override
   protected boolean sendLoadUnit(String loadUnit, String detailedDestination) {
      if (StoredCheckEnabled && !loadUnitStored(loadUnit)) {
         logger.trace("Load unit - {} is not stored", loadUnit);
         filesManager.printToOutputTextArea("Load unit - " + loadUnit + " is not stored", OutputTextArea);
         return false;
      }
      return super.sendLoadUnit(loadUnit, detailedDestination);
   }

   @Override
   protected String getDetailedDestination(String destinationLocation) {
      return super.getDetailedDestination(destinationLocation);
   }

   @Override
   protected boolean sendToDestination() {
      for (String loadUnit : LoadUnitsList) {
         if (ThreadRunning) {
            sendLoadUnit(loadUnit, getDetailedDestination(DestinationLocation));
         } else {
            //stop the iteration
            break;
         }
      }
      if (LoopEnabled) {
         if (ThreadRunning) {
            try {
               filesManager.printToOutputTextArea(String.format("Next cycle will start in - %s seconds",
                       (double) LoopDelay / 1000), OutputTextArea);
               TimeUnit.MILLISECONDS.sleep(LoopDelay);
            } catch (InterruptedException e) {
               //most likely stop was initiated by user
               logger.info("Stop initiated by user");
               stopBackgroundThread();
            }
         }
      } else {
         //loop option is not selected
         stopBackgroundThread();
      }
      return true;
   }

   private boolean parseLoadUnitsFromInput() {
      String stringInputRepresentation = InputTextArea.getText();
      LoadUnitsList = new ArrayList<>(Arrays.asList(stringInputRepresentation.split(" ")));
      logger.trace("Parsed load units - {}", LoadUnitsList);
      return !LoadUnitsList.isEmpty();
   }

   private boolean loadUnitStored(String loadUnit) {
      try {
         return SQLQueries.loadUnitStored(loadUnit);
      } catch (SQLException e) {
         logger.warn("SQL message - {}, SQL state - {}", e.getMessage(), e.getSQLState(), e);
         return false;
      }
   }

   @Override
   public boolean validateFields() {
      logger.debug("Validating database connection with provided parameters - {}", SQLQueries);
      boolean dbValidation = ValidationObj.validateDatabaseConnection(OutputTextArea, SQLQueries.getUsername(),
              SQLQueries.getPassword(), SQLQueries.getHostname(), SQLQueries.getPort(), SQLQueries.getSID());
      if (!dbValidation) {
         logger.trace("Database connection failed");
         filesManager.printToOutputTextArea("Database connection failed", OutputTextArea);
         return false;
      }
      logger.trace("Database connection successful");
      logger.debug("Validating provided program parameters - {}", this);
      boolean parametersValidation = ValidationObj.loadUnitRetrieverValidateParameters(OutputTextArea, LoopEnabled,
              CreateTransportRestCall, InputTextArea.getText(), DestinationLocation, String.valueOf(LoopDelay));
      if (!parametersValidation) {
         logger.trace("Program parameters validation failed");
         filesManager.printToOutputTextArea("Program parameters validation failed", OutputTextArea);
         return false;
      }
      logger.trace("Program parameters validated successfully");
      return true;
   }

   @Override
   public void initializeBackgroundThread() {
      logger.trace("Initializing new thread");
      LoadUnitRetrieverThread = new Thread(() -> {
         if (!validateFields()) {
            stopBackgroundThread();
            return;
         }
         logger.trace("Validation successful");
         ThreadRunning = true;
         while (ThreadRunning) {
            sendToDestination();
         }
      });
      logger.debug("Thread initialized - {}", LoadUnitRetrieverThread);
   }

   @Override
   public void startBackgroundThread() {
      OutputTextArea.clear();
      filesManager.printToOutputTextArea("Starting Load Unit Retriever", OutputTextArea);
      logger.info("Starting Load Unit Retriever");
      parseLoadUnitsFromInput();
      initializeBackgroundThread();
      LoadUnitRetrieverThread.start();
   }

   @Override
   public void pauseBackgroundThread() {

   }

   @Override
   public void stopBackgroundThread() {
      if (!SignalToStopThreadReceived) {
         logger.debug("Stop signal received for thread - {}", LoadUnitRetrieverThread);
         filesManager.printToOutputTextArea("Stopping Load Unit Retriever", OutputTextArea);
         SignalToStopThreadReceived = true;
         LoadUnitRetrieverThread.interrupt();
         ThreadRunning = false;
      }
   }

   public long getLoopDelay() {
      return LoopDelay;
   }

   public void setLoopDelay(String loopDelay) {
      try {
         LoopDelay = (long) (Double.parseDouble(loopDelay) * 1000);
      } catch (NumberFormatException e) {
         //will be taken care of in validations
         logger.trace("Incorrect format passed");
      }
   }

   public TextArea getInputTextArea() {
      return InputTextArea;
   }

   public void setInputTextArea(TextArea inputTextArea) {
      InputTextArea = inputTextArea;
   }

   public void setLoopDelay(long loopDelay) {
      LoopDelay = loopDelay;
   }

   public boolean isLoopEnabled() {
      return LoopEnabled;
   }

   public void setLoopEnabled(boolean loopEnabled) {
      LoopEnabled = loopEnabled;
   }

   public boolean isStoredCheckEnabled() {
      return StoredCheckEnabled;
   }

   public void setStoredCheckEnabled(boolean storedCheckEnabled) {
      StoredCheckEnabled = storedCheckEnabled;
   }

   @Override
   public String toString() {
      return "LoadUnitRetriever{" +
              "LoopDelay=" + LoopDelay +
              ", LoopEnabled=" + LoopEnabled +
              ", StoredCheckEnabled=" + StoredCheckEnabled +
              ", InputTextArea=" + InputTextArea +
              ", LoadUnitsList=" + LoadUnitsList +
              ", LoadUnitRetrieverThread=" + LoadUnitRetrieverThread +
              ", CreateTransportRestCall='" + CreateTransportRestCall + '\'' +
              ", DestinationLocation='" + DestinationLocation + '\'' +
              '}';
   }
}

