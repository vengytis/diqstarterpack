package venckus.gytis.programs.dmslevelstest;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import venckus.gytis.database.SqlManager;
import venckus.gytis.general.FilesManager;
import venckus.gytis.javafx.GUI.mainGUI.Controller;

import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class DMSLevelsTest {
   private int MaxNumberOfAisles;
   private int MaxNumberOfLevels;
   private long Delay;

   private String Workstation;
   private int LoadUnitsPerLevel;
   private String Facility;

   private ArrayList<String> Aisles;
   private ArrayList<String> TransportLocations;
   private String logFileName;
   private String loadUnit = null;
   private String previousLoadUnit = null;
   private String createTransportEndpoint;

   private SqlManager SQLManager;
   private FilesManager filesManager;

   public DMSLevelsTest() {

   }

   public DMSLevelsTest(SqlManager sqlManager, String createTransportEndpoint, String maxNumberOfAisles, String maxNumberOfLevels, String delay, String loadUnitsPerLevel, String workstation, String facility) {
      this.SQLManager = sqlManager;
      this.createTransportEndpoint = createTransportEndpoint;
      this.MaxNumberOfAisles = Integer.parseInt(maxNumberOfAisles);
      this.MaxNumberOfLevels = Integer.parseInt(maxNumberOfLevels);
      this.Delay = (long) (Double.parseDouble(delay) * 1000);
      this.LoadUnitsPerLevel = Integer.parseInt(loadUnitsPerLevel);
      this.Workstation = workstation;
      this.Facility = facility;
      filesManager = new FilesManager();
      logFileName = filesManager.generateFileName(workstation);
   }

   /* public void generateAislesGrid(GridPane aislesGrid) {
        ArrayList<CheckBox> checkBoxes = new ArrayList<>();
        int cellSize = 50;
        MaxNumberOfAisles = 10;
        int gridSize = calculateGridSize(MaxNumberOfAisles);
        for (int i = 1; i <= MaxNumberOfAisles; i++) {
            CheckBox checkBox = new CheckBox();
            checkBox.setText(String.valueOf(i));
            checkBoxes.add(checkBox);
        }
        aislesGrid.addColumn();
        aislesGrid.getColumnConstraints().add(new ColumnConstraints(cellSize));
        aislesGrid.getRowConstraints().add(new RowConstraints(cellSize));
        aislesGrid.getChildren().addAll(checkBoxes);
       /* int counter = 0;
        for (int row = 0; row < gridSize-1; row++) {
        aislesGrid.getColumnConstraints().add(new ColumnConstraints(cellSize));
        aislesGrid.getRowConstraints().add(new RowConstraints(cellSize));
            for (int column = 0; column < gridSize-1; column++) {
                if (counter <= MaxNumberOfAisles){
                aislesGrid.add(checkBoxes.get(counter), column, row);
                }
                counter++;
            }
        }
        aislesGrid.setLayoutX(aislesGrid.getLayoutX() - 2*cellSize);
    }*/

   private int calculateGridSize(int maxNumberOfAisles) {
      int value = 1;
      while (true) {
         if (value * value > maxNumberOfAisles) {
            return value;
         }
         value++;
      }
   }

   public ArrayList<String> retrieveSelectedAisles(GridPane aislesGrid) {
      Aisles = new ArrayList<>();
      ObservableList<Node> checkboxList = aislesGrid.getChildren();
      for (Node node : checkboxList) {
         if (node instanceof CheckBox) {
            if (((CheckBox) node).isSelected()) {
               Aisles.add(((CheckBox) node).getText());
            }
         }
      }
      return Aisles;
   }

   public ArrayList<String> generateTransports(ArrayList<String> Aisles) {
      TransportLocations = new ArrayList<>();
      for (String aisle : Aisles) {
         for (int i = 1; i <= MaxNumberOfLevels; i++) {
            TransportLocations.add(Facility + ".MS." + ((Integer.parseInt(aisle) < 10) ? "0" + aisle : aisle) + "." + ((i < 10) ? "0" + i : i) + ".0");
         }
      }
      return TransportLocations;
   }

   public void sendLoadUnits(int locationIndex, ArrayList<String> transportLocations, TextArea outputTextArea) throws InterruptedException {
      String loadUnit = getLoadUnitAtLocation(Workstation, outputTextArea);
      sendLoadUnit(loadUnit, transportLocations.get(locationIndex), outputTextArea);
      filesManager.printToOutputTextAreaAndFile("LoadUnit " + loadUnit + " sent from " + Workstation + " to " +
              transportLocations.get(locationIndex), outputTextArea, getClass().getSimpleName(), logFileName);
      TimeUnit.MILLISECONDS.sleep(Delay);
   }

   private String getLoadUnitAtLocation(String sourceLocation, TextArea outputTextArea) {
      try {
         if (!Controller.getDMSLevelsTestThreadRunning(sourceLocation).get()) {
            throw new InterruptedException();
         }
         Connection SQLConnection = SQLManager.connect();
         if (SQLConnection == null) {
            filesManager.printToOutputTextAreaAndFile("Connection with database could not be established for location " + sourceLocation + "\n", outputTextArea, getClass().getSimpleName(), logFileName);
            return null;
         }
         PreparedStatement preparedStatement = SQLConnection.prepareStatement(
                 "SELECT * from cor_lum_load_unit WHERE current_loc like '%" + sourceLocation +
                         "%' and rownum = 1 and id != '" + previousLoadUnit + "' and status != 'TRANSPORT' and status != 'SCHEDULED' order by mod_date desc");
         ResultSet rs = preparedStatement.executeQuery();
         if (rs.next()) {
            loadUnit = rs.getString("id");
            if (loadUnit.equals(previousLoadUnit)) {
               filesManager.printToOutputTextAreaAndFile("Unable to receive loadUnit at " + sourceLocation +
                       ". Retrying in " + (double) Delay / 1000 + " seconds", outputTextArea, getClass().getSimpleName(), logFileName);
               TimeUnit.MILLISECONDS.sleep(Delay);
               loadUnit = getLoadUnitAtLocation(sourceLocation, outputTextArea);
            } else {
               filesManager.printToOutputTextAreaAndFile("Previous loadUnit: " + previousLoadUnit, outputTextArea,
                       getClass().getSimpleName(), logFileName);
               filesManager.printToOutputTextAreaAndFile("LoadUnit " + loadUnit + " arrived at " +
                       sourceLocation, outputTextArea, getClass().getSimpleName(), logFileName);
               return loadUnit;
            }
         } else {
            filesManager.printToOutputTextAreaAndFile("Unable to receive loadUnit at " + sourceLocation +
                    ". Retrying in " + (double) Delay / 1000 + " seconds", outputTextArea, getClass().getSimpleName(), logFileName);
            TimeUnit.MILLISECONDS.sleep(Delay);
            loadUnit = getLoadUnitAtLocation(sourceLocation, outputTextArea);
         }
      } catch (InterruptedException e) {
         Thread.currentThread().interrupt();
         SQLManager.disconnect();
         return null;
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         SQLManager.disconnect();
      }
      return loadUnit;
   }

   private boolean sendLoadUnit(String loadUnit, String location, TextArea outputTextArea) {
      HttpURLConnection connection;
      try {
         if (!Controller.getDMSLevelsTestThreadRunning(Workstation).get()) {
            throw new InterruptedException();
         }
         URL url = new URL(createTransportEndpoint + loadUnit + "/" + location);
         connection = (HttpURLConnection) url.openConnection();
         connection.setRequestMethod("PUT");
         if (String.valueOf(connection.getResponseCode()).startsWith("2")) {
            previousLoadUnit = loadUnit;
            return true;
         } else {
            filesManager.printToOutputTextAreaAndFile("Unable to send loadUnit " + loadUnit + " from " + Workstation + " to " +
                    location + ". Retrying in " + (double) Delay / 1000 + " seconds", outputTextArea, getClass().getSimpleName(), logFileName);
            TimeUnit.MILLISECONDS.sleep(Delay);
            loadUnit = getLoadUnitAtLocation(Workstation, outputTextArea);
            sendLoadUnit(loadUnit, location, outputTextArea);
         }
      } catch (InterruptedException e) {
         Thread.currentThread().interrupt();
      } catch (Exception e) {
         e.printStackTrace();
      }
      return false;
   }
}
