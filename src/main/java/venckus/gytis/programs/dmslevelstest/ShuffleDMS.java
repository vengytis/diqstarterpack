package venckus.gytis.programs.dmslevelstest;

import venckus.gytis.boundary.storage.DematicMultiShuttle;
import venckus.gytis.database.SqlManager;
import venckus.gytis.database.SqlQueries;
import venckus.gytis.general.FilesManager;
import venckus.gytis.javafx.Main;

import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;

public class ShuffleDMS extends DematicMultiShuttle {
   private Random RandomGen;
   private ArrayList<String> Transports;
   private ArrayList<String> LoadUnits;
   private int LoadUnitCount;
   private SqlQueries SQLQueries;
   private FilesManager FilesManagerObj;
   private String LogFileName;
   private String CreateTransportEndpoint;

   public ShuffleDMS(SqlQueries sqlQueries, String createTransportEndpoint, int loadUnitCount, String facility, int maxNumberOfAisles, int firstAisleNumber, int maxNumberOfLevels) {
      FilesManagerObj = new FilesManager();
      LogFileName = FilesManagerObj.generateFileName("shuffleDMS");
      SQLQueries = sqlQueries;
      CreateTransportEndpoint = createTransportEndpoint;
      LoadUnitCount = loadUnitCount;
      Facility = facility;
      NumberOfAisles = maxNumberOfAisles;
      StartingAisleNumber = firstAisleNumber;
      NumberOfLevels = maxNumberOfLevels;
      Transports = generateTransports();
      LoadUnits = storedLoadUnits();
   }

   public ArrayList<String> getLoadUnits() {
      return LoadUnits;
   }

   public String getLogFileName() {
      return LogFileName;
   }

   private ArrayList<String> storedLoadUnits() {
      ArrayList<String> loadUnits = new ArrayList<>();
      try {
         loadUnits = SQLQueries.getNumberOfStoredLoadUnitsAsStrings(LoadUnitCount);
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         FilesManagerObj.printToOutputTextAreaAndFile(String.format("Load units retrieval from DB finished, retrieved %s load units", loadUnits.size()),
                 Main.controllerHandler.shuffleDMS_outputArea, ShuffleDMS.class.getSimpleName(), LogFileName);
         SQLQueries.disconnect();
      }
      return loadUnits;
   }

   private ArrayList<String> generateTransports() {
      ArrayList<String> TransportLocations = super.generateVirtualLocations();
      FilesManagerObj.printToOutputTextAreaAndFile(String.format("Possible %s transport destinations generated", TransportLocations.size()),
              Main.controllerHandler.shuffleDMS_outputArea, ShuffleDMS.class.getSimpleName(), LogFileName);
      return TransportLocations;
   }

   //TODO: change these rest calls to ones from core rather than ASPIAG specific
   public void sendLoadUnitToDifferentLocation(String loadUnit, String destination) {
      HttpURLConnection connection = null;
      try {
         URL url = new URL(CreateTransportEndpoint + loadUnit + "/" + destination);
         connection = (HttpURLConnection) url.openConnection();
         connection.setRequestMethod("PUT");
         if (String.valueOf(connection.getResponseCode()).startsWith("2")) {
            FilesManagerObj.printToOutputTextAreaAndFile("LoadUnit " + loadUnit + " sent to location " + destination,
                    Main.controllerHandler.shuffleDMS_outputArea, ShuffleDMS.class.getSimpleName(), LogFileName);
         } else {
            FilesManagerObj.printToOutputTextAreaAndFile("Unable to send loadUnit " + loadUnit + " to location " + destination,
                    Main.controllerHandler.shuffleDMS_outputArea, ShuffleDMS.class.getSimpleName(), LogFileName);
         }
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         assert connection != null;
         connection.disconnect();
      }
   }

   public String returnRandomDMSVirtualLocation() {
      RandomGen = new Random();
      int index = RandomGen.nextInt(Transports.size());
      return Transports.get(index);
   }
}
