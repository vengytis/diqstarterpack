package venckus.gytis.programs;

import venckus.gytis.general.Validations;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Every program implementing this interface should do internal handling without exposing logic to Controller (validation of
 * parameters, DB connection etc.). Separate thread is created for each program instead of using default JavaFX GUI thread.
 * Controller should only be able to send commands (start, stop, pause) and not handle any program logic itself
 */
public interface Program {

   Validations ValidationObj = new Validations();

   boolean validateFields();

   void initializeBackgroundThread();

   void startBackgroundThread();

   void pauseBackgroundThread();

   void stopBackgroundThread();
}
