package venckus.gytis.programs.loadunitextractor;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.general.FilesManager;
import venckus.gytis.programs.Program;


public class LoadUnitExtractor implements Program {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private TextArea OutputTextArea;

   private Label CopiedToClipboardLabel;

   private String InputText;

   private String LoadUnitPrefix;

   private int LoadUnitIdLength;

   private Thread LoadUnitExtractorThread;

   private FilesManager filesManager;

   /**
    * Constructor
    */
   public LoadUnitExtractor() {
      filesManager = new FilesManager();
   }

   /**
    * Extracts load units from given input by provided prefix
    */
   private void extractLoadUnits() {
      StringBuilder loadUnits = new StringBuilder();
      String initialTextInput = InputText;
      if (!InputText.contains(LoadUnitPrefix)) {
         //conditional check to validate if it's even worth to dive into 'while' loop
         logger.debug("Input text does not contain prefix at all");
         filesManager.printToOutputTextArea("No load units found", OutputTextArea);
         return;
      }

      while (InputText.contains(LoadUnitPrefix)) {
         if (initialTextInput.length() < LoadUnitIdLength) {
            //check for lower bound
            logger.debug("Lower bound check failed, input text length is - {} while min load unit length is - {}",
                    InputText.length(), LoadUnitIdLength);
            filesManager.printToOutputTextArea("No load units found", OutputTextArea);
            break;
         }
         if (InputText.indexOf(LoadUnitPrefix) + LoadUnitIdLength > InputText.length()) {
            //checks if upper bound constraint is not violated, if it is - break
            if (loadUnits.length() == 0) {
               filesManager.printToOutputTextArea("No load units found", OutputTextArea);
            }
            logger.debug("Upper bound violated, last prefix index position - {}, load unit length - {}, remaining text length - {}",
                    InputText.indexOf(LoadUnitPrefix), LoadUnitIdLength, InputText.length());
            break;
         }
         try {
            String loadUnit = InputText.substring(InputText.indexOf(LoadUnitPrefix), InputText.indexOf(LoadUnitPrefix) + LoadUnitIdLength);
            InputText = InputText.replace(loadUnit, "");
            loadUnits.append(loadUnit).append(" ");
         } catch (Exception e) {
            logger.error(e.getMessage(), e);
            //In case of exception - break the cycle and kill the thread
            stopBackgroundThread();
            break;
         }
      }
      logger.debug("Load units found - {}", loadUnits);
      //there is no need to use files manager printing method here, since it would provide inconvenience when copying load units
      OutputTextArea.setText(loadUnits.toString());
      copyLoadUnitIdsToClipboard(loadUnits.toString());
      CopiedToClipboardLabel.setVisible(true);
   }

   /**
    * Copies extracted load units to user's clipboard
    *
    * @param loadUnitIds extracted load units
    */
   private void copyLoadUnitIdsToClipboard(String loadUnitIds) {
      StringSelection selection = new StringSelection(loadUnitIds);
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.setContents(selection, selection);
   }

   @Override
   public boolean validateFields() {
      logger.debug("Validating provided program parameters - {}", this);
      boolean parametersValidation = ValidationObj.loadUnitExtractorValidateParameters(OutputTextArea,
              InputText, LoadUnitPrefix, String.valueOf(LoadUnitIdLength));
      if (!parametersValidation) {
         logger.trace("Program parameters validation failed");
         return false;
      }
      logger.trace("Program parameters validated successfully");
      return true;
   }

   @Override
   public void initializeBackgroundThread() {
      OutputTextArea.clear();
      logger.trace("Initializing new thread");
      LoadUnitExtractorThread = new Thread(() -> {
         if (!validateFields())
            return;
         logger.trace("Validation successful");
         extractLoadUnits();
         stopBackgroundThread();
      });
      logger.debug("Thread initialized - {}", LoadUnitExtractorThread);
   }

   @Override
   public void startBackgroundThread() {
      initializeBackgroundThread();
      logger.debug("Starting thread - {}", LoadUnitExtractorThread);
      LoadUnitExtractorThread.start();
   }

   @Override
   public void pauseBackgroundThread() {

   }

   @Override
   public void stopBackgroundThread() {
      logger.debug("Stopping thread - {}", LoadUnitExtractorThread);
      LoadUnitExtractorThread.interrupt();
   }

   public void setInputText(String inputText) {
      InputText = inputText;
   }

   public void setLoadUnitPrefix(String loadUnitPrefix) {
      LoadUnitPrefix = loadUnitPrefix;
   }

   public void setLoadUnitIdLength(String loadUnitIdLength) {
      try {
         LoadUnitIdLength = Integer.parseInt(loadUnitIdLength);
      } catch (NumberFormatException e) {
         //will be taken care of in validation
         logger.debug("Incorrect format passed - {}", loadUnitIdLength);
      }
   }

   public void setOutputTextArea(TextArea outputTextArea) {
      OutputTextArea = outputTextArea;
   }

   public void setCopiedToClipboardLabel(Label copiedToClipboardLabel) {
      CopiedToClipboardLabel = copiedToClipboardLabel;
   }

   @Override
   public String toString() {
      return "LoadUnitExtractor{" +
              "InputText='" + InputText + '\'' +
              ", LoadUnitPrefix='" + LoadUnitPrefix + '\'' +
              ", LoadUnitIdLength=" + LoadUnitIdLength +
              '}';
   }
}
