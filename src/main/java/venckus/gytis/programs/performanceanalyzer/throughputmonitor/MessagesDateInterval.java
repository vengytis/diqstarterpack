package venckus.gytis.programs.performanceanalyzer.throughputmonitor;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Entity class which represent a single one minute interval
 */
public class MessagesDateInterval {

   private DateTime StartTime;

   private DateTime EndTime;

   private long LoadUnitsPassedCount;

   private DateTimeFormatter minutesFormatter = DateTimeFormat.forPattern("HH:mm");


   /**
    * Constructor
    */
   public MessagesDateInterval (DateTime startTime, DateTime endTime) {
      StartTime = startTime;
      EndTime = endTime;
   }

   /**
    * Overridden toString() method which defines the format of interval and is used for displaying data on X axis of the chart
    * @return
    */
   @Override
   public String toString() {
      return String.format("%s", StartTime.toString(minutesFormatter));
   }

   public DateTime getStartTime() {
      return StartTime;
   }

   public DateTime getEndTime() {
      return EndTime;
   }

   public void setLoadUnitsPassedCount(long loadUnitsPassedCount) {
      LoadUnitsPassedCount = loadUnitsPassedCount;
   }

   public long getLoadUnitsPassedCount() {
      return LoadUnitsPassedCount;
   }
}
