package venckus.gytis.programs.performanceanalyzer.throughputmonitor;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Enumerator class which represents telegrams that may be used when calculating throughput
 */
public enum Telegram {
   TURP,
   TUDR,
   TULL,
   TUNO,
   TUEX,
   TUCA,
   TUMI;

   public static ArrayList<Telegram> getValuesAsArraylist() {
      return new ArrayList<>(Arrays.asList(Telegram.values()));
   }

}
