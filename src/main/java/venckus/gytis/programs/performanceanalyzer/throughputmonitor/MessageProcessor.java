package venckus.gytis.programs.performanceanalyzer.throughputmonitor;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.database.SqlManager;
import venckus.gytis.programs.improvedmessagetrace.entity.Message;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Class which fetches rows (messages) from database and then processes and filters them
 */
public class MessageProcessor {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private ArrayList<Message> MessagesList;

   private PerformanceQueriesDatabase performanceQueriesDatabase;

   private DateTime StartTime;

   private DateTime EndTime;

   private DateTime LatestUpdate;

   private DateTime LastMessageTime;

   private IntervalsProvider intervalsProvider;

   private HashMap<String, String> LocationsAndTelegrams;

   /**
    * Constructor
    */
   public MessageProcessor(DateTime startTime, DateTime endTime, HashMap<String, String> locationsAndTelegrams, SqlManager databaseParameters) {
      performanceQueriesDatabase = new PerformanceQueriesDatabase(databaseParameters);
      StartTime = startTime;
      EndTime = endTime;
      LocationsAndTelegrams = locationsAndTelegrams;
      intervalsProvider = new IntervalsProvider();
      //initial messages retrieval
      MessagesList = new ArrayList<>();
      processMessages(StartTime, EndTime);
   }

   /**
    * Fetches rows by provided timestamp and puts them to the message list
    */
   private void processMessages(DateTime startTime, DateTime endTime) {
      logger.debug("Start time - {}, End time - {}", startTime, endTime);
      Timestamp startDateTimestamp = new Timestamp(startTime.getMillis());
      Timestamp endDateTimestamp = new Timestamp(endTime.getMillis());

      //building custom query
      int internalCounter = 0;
      StringBuilder messageQuery = new StringBuilder();
      messageQuery.append("SELECT * FROM APP_MM_TRACE ");
      messageQuery.append("WHERE cdate >= to_timestamp('").append(startDateTimestamp).append("', 'YYYY-MM-DD HH24:MI:SS.FF') ");
      messageQuery.append("AND cdate <= to_timestamp('").append(endDateTimestamp).append("', 'YYYY-MM-DD HH24:MI:SS.FF') ");
      messageQuery.append("AND (");
      for (HashMap.Entry<String, String> entry : LocationsAndTelegrams.entrySet()) {
         messageQuery.append("(SUBSTR(USER_DATA, 15, 29) LIKE '%").append(entry.getKey()).append("%' ");
         messageQuery.append("AND OPERATION_ID = '").append(entry.getValue()).append("') ");
         if (++internalCounter < LocationsAndTelegrams.size()) {
            messageQuery.append("OR ");
         }
      }
      messageQuery.append(") ORDER BY cdate ASC");

      try {
         ResultSet messages = performanceQueriesDatabase.executeCustomQuery(messageQuery);
         LatestUpdate = endTime;
         logger.debug("Latest update from database was received at - {}", LatestUpdate);
         messages.setFetchSize(performanceQueriesDatabase.getOptimalFetchSize(messageQuery));
         //iterating through all messages retrieved from DB and adding them to the list
         while (messages.next()) {
            Message message = new Message(messages);
            MessagesList.add(message);
            LastMessageTime = message.getCreateDate();
         }
         messages.close();
      } catch (SQLException e) {
         //in case something breaks - wait a while and try to repeat the process from previous "save point"
         logger.warn(e.getMessage(), e);
         performanceQueriesDatabase.disconnect();
         try {
            //TODO this logic is not really working for some reason
            TimeUnit.SECONDS.sleep(5);
            logger.info("Attempting to reestablish connection");
            processMessages(LastMessageTime, endTime);
         } catch (InterruptedException interruptedException) {
            logger.info("Operation interrupted on thread - {}", Thread.currentThread());
         }
      } finally {
         performanceQueriesDatabase.disconnect();
      }
   }

   private void updateMessages() {
      processMessages(LatestUpdate, new DateTime(System.currentTimeMillis()));
   }

   /**
    * Filters fetched messages data from DB by location and operation
    *
    * @return filtered ArrayList by provided predicate
    */
   private ArrayList<Message> getMessagesFilteredByLocationAndTelegram(String location, String telegram) {
      Predicate<Message> messagePredicate = message ->
              message.getCurrentLocation() != null
                      && message.getOperationId() != null
                      && message.getCurrentLocation().contains(location)
                      && message.getOperationId().equals(telegram);

      logger.trace("Total messages list length - {}", MessagesList.size());

      return MessagesList.stream()
              .filter(messagePredicate)
              .collect(Collectors.toCollection(ArrayList::new));
   }

   /**
    * Calculates applicable messages in defined interval
    *
    * @param messages filtered list containing only useful locations and telegrams
    * @param interval defines start and end of singular timestamp
    * @return counted messages in interval after applying predicate
    */
   private int calculateLoadUnitsPassedPerInterval(ArrayList<Message> messages, MessagesDateInterval interval) {
      Predicate<Message> messagePredicate = message ->
              message.getCreateDate().isAfter(interval.getStartTime())
                      && message.getCreateDate().isBefore(interval.getEndTime());

      return ((int) messages.stream()
              .filter(messagePredicate)
              .count());
   }

   /**
    * Combines operations to create data for all intervals and populates them with count of messages in each small interval
    *
    * @return
    */
   public ArrayList<MessagesDateInterval> createIntervalsData(String location, String telegram) {
      intervalsProvider.setStartTime(StartTime);
      intervalsProvider.setEndTime(EndTime);
      ArrayList<MessagesDateInterval> messagesDateIntervals = intervalsProvider.createIntervalsByMinutes();
      ArrayList<Message> filteredMessagesList = getMessagesFilteredByLocationAndTelegram(location, telegram);
      messagesDateIntervals.forEach(interval -> {
         interval.setLoadUnitsPassedCount(calculateLoadUnitsPassedPerInterval(filteredMessagesList, interval));
      });
      return messagesDateIntervals;
   }

   public ArrayList<MessagesDateInterval> updateIntervalsData(String location, String telegram) {
      intervalsProvider.setEndTime(LatestUpdate);
      updateMessages();
      ArrayList<MessagesDateInterval> messagesDateIntervals = intervalsProvider.createIntervalsByMinutes();
      ArrayList<Message> filteredMessagesList = getMessagesFilteredByLocationAndTelegram(location, telegram);
      messagesDateIntervals.forEach(interval -> {
         interval.setLoadUnitsPassedCount(calculateLoadUnitsPassedPerInterval(filteredMessagesList, interval));
      });
      return messagesDateIntervals;
   }
}
