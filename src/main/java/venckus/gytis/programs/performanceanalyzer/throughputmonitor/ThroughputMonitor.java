package venckus.gytis.programs.performanceanalyzer.throughputmonitor;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextArea;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import venckus.gytis.database.SqlManager;
import venckus.gytis.general.FilesManager;
import venckus.gytis.programs.Program;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Main program class used by Controller. Communicates with other needed classes to return result in form of ArrayList of Series
 * later used for displaying data in chart
 */
public class ThroughputMonitor implements Program {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private final String MovingAverageSuffix = "_MA";

   private LocalDate Date;

   private String StartTime;

   private String EndTime;

   private boolean Initialized;

   private TextArea OutputTextArea;

   private FilesManager filesManager;

   private ArrayList<String> LocationsTelegramsList;

   private MessageProcessor messageProcessor;

   private Thread ThroughputMonitorThread;

   private ArrayList<XYChart.Series<String, Number>> ChartData;

   private HashMap<String, String> LocationsAndTelegrams;

   private AreaChart AreaChart;

   private ThroughputMonitorTimerTask ThroughputMonitorTimerTask;

   private SqlManager DatabaseParameters;

   private boolean SignalToStopThreadReceived;

   private boolean ThreadRunning;

   /**
    * Constructor
    */
   public ThroughputMonitor() {
      Initialized = false;
      filesManager = new FilesManager();
      ThreadRunning = false;
      SignalToStopThreadReceived = false;
   }

   /**
    * Forms list of Series to be displayed in GUI. Each location has two series -
    * one is for load unit count and another for moving average
    *
    * @return list of created series
    */
   @SuppressWarnings("unchecked")
   private void createInitialChartData() {
      ChartData = new ArrayList<>();
      DateTime startTime = constructJodaDateTime(Date, StartTime);
      DateTime endTime;
      if (isEndTimeNow(EndTime)) {
         endTime = new DateTime(System.currentTimeMillis());
      } else {
         endTime = constructJodaDateTime(Date, EndTime);
      }
      logger.debug("Constructed Joda DateTimes from fields: start - {}, end - {}", startTime, endTime);
      LocationsAndTelegrams = deconstructLocationsAndTelegrams();
      logger.debug("Deconstructed locations and telegrams map from list in GUI - {}", LocationsAndTelegrams);
      filesManager.printToOutputTextArea("Fetching data from database", OutputTextArea);
      messageProcessor = new MessageProcessor(startTime, endTime, LocationsAndTelegrams, DatabaseParameters);
      filesManager.printToOutputTextArea("Creating intervals", OutputTextArea);
      for (Map.Entry<String, String> entry : LocationsAndTelegrams.entrySet()) {
         String location = entry.getKey();
         String telegram = entry.getValue();
         ArrayList<MessagesDateInterval> intervalsData = messageProcessor.createIntervalsData(location, telegram);

         assignSeries(location, intervalsData);
      }
      Initialized = true;
      ThreadRunning = false;
   }

   @SuppressWarnings("unchecked")
   private void assignSeries(String location, ArrayList<MessagesDateInterval> intervalsData) {
      XYChart.Series<String, Number> locationLoadUnitsSeries = new XYChart.Series<>();
      locationLoadUnitsSeries.setName(location);
      XYChart.Series<String, Number> locationMovingAverageSeries = new XYChart.Series<>();
      locationMovingAverageSeries.setName(location + MovingAverageSuffix);
      logger.trace("Assigned series names: Load Units series - {}, Moving Average series - {}",
              locationLoadUnitsSeries.getName(), locationMovingAverageSeries.getName());

      int sumMovingAverage = 0;
      for (int i = 0; i < intervalsData.size(); i++) {
         locationLoadUnitsSeries.getData().add(new XYChart.Data(intervalsData.get(i).toString(), intervalsData.get(i).getLoadUnitsPassedCount()*60));
         sumMovingAverage += intervalsData.get(i).getLoadUnitsPassedCount()*60;
         locationMovingAverageSeries.getData().add(new XYChart.Data(intervalsData.get(i).toString(), (double) sumMovingAverage / (i + 1)));
      }
      ChartData.add(locationLoadUnitsSeries);
      ChartData.add(locationMovingAverageSeries);
      logger.trace("Series added - {}, {}", locationLoadUnitsSeries, locationMovingAverageSeries);
   }

   private void updateChartData() {
      //clear previous data
      ChartData.clear();
      for (Map.Entry<String, String> entry : LocationsAndTelegrams.entrySet()) {
         String location = entry.getKey();
         String telegram = entry.getValue();
         ArrayList<MessagesDateInterval> intervalsData = messageProcessor.updateIntervalsData(location, telegram);

         assignSeries(location, intervalsData);
      }
      ThreadRunning = false;
   }

   /**
    * Constructs joda DateTime object with provided values of LocalDate and String
    *
    * @param date retrieved from DatePicker object in GUI
    * @param time retrieved as String object from TextField object in GUI
    * @return formatted DateTime object
    */
   private DateTime constructJodaDateTime(LocalDate date, String time) {
      DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
      String combinedDateTime = String.format("%s %s", date, time);
      return formatter.parseDateTime(combinedDateTime);
   }

   /**
    * Deconstructs values retrieved from GUI in format of "location - telegram" and
    * maps them to appropriate (key,value) pairs of (location,telegram)
    *
    * @return
    */
   private HashMap<String, String> deconstructLocationsAndTelegrams() {
      HashMap<String, String> deconstructedList = new HashMap<>();
      LocationsTelegramsList.forEach(entry -> {
         String[] deconstructedEntry = entry.split(" - ");
         deconstructedList.put(deconstructedEntry[0], deconstructedEntry[1]);
      });
      return deconstructedList;
   }

   private boolean isEndTimeNow(String endTime) {
      return endTime.isEmpty();
   }

   public LocalDate getDate() {
      return Date;
   }

   public void setDate(LocalDate date) {
      Date = date;
   }

   public void setStartTime(String startTime) {
      StartTime = startTime;
   }

   public void setEndTime(String endTime) {
      EndTime = endTime;
   }

   @SuppressWarnings("unchecked")
   public void setLocationsTelegramsList(ObservableList<String> locationsTelegramsList) {
      LocationsTelegramsList = new ArrayList<>(locationsTelegramsList);
   }

   @Override
   public boolean validateFields() {
      logger.debug("Validating database connection with provided parameters - {}", DatabaseParameters);
      boolean dbValidation = ValidationObj.validateDatabaseConnection(OutputTextArea, DatabaseParameters.getUsername(),
              DatabaseParameters.getPassword(), DatabaseParameters.getHostname(), DatabaseParameters.getPort(), DatabaseParameters.getSID());
      if (!dbValidation) {
         logger.trace("Database connection failed");
         filesManager.printToOutputTextArea("Database connection failed", OutputTextArea);
         return false;
      }
      logger.trace("Database connection successful");
      logger.debug("Validating provided program parameters - {}", this);
      boolean parametersValidation = ValidationObj.throughputMonitorValidateParameters(OutputTextArea,
              Date, StartTime, EndTime, LocationsTelegramsList);
      if (!parametersValidation) {
         logger.trace("Program parameters validation failed");
         filesManager.printToOutputTextArea("Program parameters validation failed", OutputTextArea);
         return false;
      }
      logger.trace("Program parameters validated successfully");
      return true;
   }

   @SuppressWarnings("unchecked")
   @Override
   public void initializeBackgroundThread() {
      logger.trace("Initializing new thread");
      ThroughputMonitorThread = new Thread(() -> {
         if (!validateFields()) {
            completelyStop();
            return;
         }
         logger.trace("Validation successful");
         ThreadRunning = true;
         while (ThreadRunning) {
            try {
               if (Initialized) {
                  //Initialization is already done, so we only need to update
                  updateChartData();
               } else {
                  //Not initialized yet
                  createInitialChartData();
               }
            } catch (Exception e) {
               e.printStackTrace();
               stopBackgroundThread();
            }
         }
         //update GuI
         filesManager.printToOutputTextArea("Populating chart with data", OutputTextArea);
         Platform.runLater(() -> {
            AreaChart.getData().clear();
            ChartData.forEach(series -> {
               //adding series
               AreaChart.getData().add(series);
            });
         });
         stopBackgroundThread();
      });
      logger.debug("Thread initialized - {}", ThroughputMonitorThread);
   }

   @Override
   public void startBackgroundThread() {
      OutputTextArea.clear();
      filesManager.printToOutputTextArea("Starting Throughput Monitor", OutputTextArea);
      initializeBackgroundThread();
      ThroughputMonitorThread.start();
   }

   @Override
   public void pauseBackgroundThread() {

   }

   @Override
   public void stopBackgroundThread() {
      completelyStop();
   }

   public void completelyStop() {
      if (!SignalToStopThreadReceived) {
         filesManager.printToOutputTextArea("Stopping Throughput Monitor", OutputTextArea);
         logger.debug("Stop signal received for thread - {}", ThroughputMonitorThread);
         logger.trace("Stopping - {}", ThroughputMonitorThread);
         if (ThroughputMonitorTimerTask != null) {
            //if timer task is running - cancel it
            logger.trace("Cancelling running timer task - {}", ThroughputMonitorTimerTask);
            ThroughputMonitorTimerTask.cancel();
         }
         SignalToStopThreadReceived = true;
         ThroughputMonitorThread.interrupt();
         ThreadRunning = false;
         logger.trace("Stopped - {}", ThroughputMonitorThread);
      }
   }

   public ArrayList<XYChart.Series<String, Number>> getChartData() {
      return ChartData;
   }

   public void setAreaChart(AreaChart areaChart) {
      AreaChart = areaChart;
   }

   public boolean isInitialized() {
      return Initialized;
   }

   public Thread getThroughputMonitorThread() {
      return ThroughputMonitorThread;
   }

   public SqlManager getDatabaseParameters() {
      return DatabaseParameters;
   }

   public void setDatabaseParameters(SqlManager databaseParameters) {
      DatabaseParameters = databaseParameters;
   }

   public TextArea getOutputTextArea() {
      return OutputTextArea;
   }

   public void setOutputTextArea(TextArea outputTextArea) {
      OutputTextArea = outputTextArea;
   }
}
