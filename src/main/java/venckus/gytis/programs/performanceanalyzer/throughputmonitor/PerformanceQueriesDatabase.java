package venckus.gytis.programs.performanceanalyzer.throughputmonitor;

import venckus.gytis.database.SqlManager;
import venckus.gytis.database.SqlQueries;

import java.sql.*;

/**
 * Queries class containing sql queries required for operations
 */
public class PerformanceQueriesDatabase extends SqlQueries {

   /**
    * Constructor
    */
   public PerformanceQueriesDatabase(String hostname, String port, String SID, String dbusername, String dbpassword) {
      super(hostname, port, SID, dbusername, dbpassword);
      URL = URLBuilder(hostname, port, SID);
   }

   public PerformanceQueriesDatabase(SqlManager databaseConnector) {
      this.Username = databaseConnector.getUsername();
      this.Password = databaseConnector.getPassword();
      this.Hostname = databaseConnector.getHostname();
      this.Port = databaseConnector.getPort();
      this.SID = databaseConnector.getSID();
   }

   @Override
   public int getOptimalFetchSize(StringBuilder query) throws SQLException {
      return super.getOptimalFetchSize(query);
   }

   @Override
   public ResultSet executeCustomQuery(StringBuilder query) throws SQLException {
      logger.debug("Executing query - {}", query.toString());
      return super.executeCustomQuery(query);
   }

}
