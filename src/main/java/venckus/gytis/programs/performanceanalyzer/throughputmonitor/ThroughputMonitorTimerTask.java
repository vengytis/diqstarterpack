package venckus.gytis.programs.performanceanalyzer.throughputmonitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class ThroughputMonitorTimerTask extends TimerTask {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private ThroughputMonitor throughputMonitor;

   public ThroughputMonitorTimerTask(int timerInSeconds, ThroughputMonitor throughputMonitor) {
      timerInSeconds *= 1000;
      this.throughputMonitor = throughputMonitor;
      Timer timer = new Timer(true);
      timer.scheduleAtFixedRate(this, 0, timerInSeconds);
   }

   @Override
   public void run() {
      Set<Thread> threads = Thread.getAllStackTraces().keySet();

      for (Thread t : threads) {
         System.out.println(t.toString());
      }
      throughputMonitor.initializeBackgroundThread();
      throughputMonitor.getThroughputMonitorThread().start();
   }
}
