package venckus.gytis.programs.performanceanalyzer.throughputmonitor;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Splits one provided interval into many small 1 minute intervals
 */
public class IntervalsProvider {

   private Logger logger = LoggerFactory.getLogger(this.getClass());

   private DateTime StartTime;

   private DateTime EndTime;

   private int TimestampInMinutes;

   /**
    * Constructor. Timestamp does not need to be passed, since it is used only here and can be calculated locally
    */
   public IntervalsProvider() {

   }

   /**
    * Creates intervals by splitting whole timestamp by points into equal parts
    * @param points
    * @return ArrayList of intervals
    */
   public ArrayList<MessagesDateInterval> createIntervalsByPoints(int points) {
      ArrayList<MessagesDateInterval> intervals = new ArrayList<>();
      logger.debug("Points provided to create intervals - {}", points);
      int durationPerPoint = TimestampInMinutes / points;
      DateTime currentPointer = StartTime.minusMinutes(durationPerPoint);
      logger.trace("Initial datetime - {}, duration in minutes per interval point - {}", currentPointer, durationPerPoint);
      while (currentPointer.isBefore(EndTime)) {
         MessagesDateInterval interval = new MessagesDateInterval(currentPointer, currentPointer.plusMinutes(durationPerPoint));
         intervals.add(interval);
         currentPointer = currentPointer.plusMinutes(durationPerPoint);
      }
      //remove first element, since it is less than the lower bound
      intervals.remove(0);
      logger.trace("Intervals created - {}", intervals);
      return intervals;
   }

   /**
    * if no points are defined, this is the default option which creates point for each minute
    * @return ArrayList of intervals
    */
   public ArrayList<MessagesDateInterval> createIntervalsByMinutes() {
      TimestampInMinutes = (int) new Duration(StartTime, EndTime).getStandardMinutes();
      logger.debug("No points provided, creating intervals by duration minutes - {}", TimestampInMinutes);
      return createIntervalsByPoints(TimestampInMinutes);
   }

   public void setStartTime(DateTime startTime) {
      StartTime = startTime;
   }

   public void setEndTime(DateTime endTime) {
      EndTime = endTime;
   }
}
